<?php
/**
 * Sample implementation of the Custom Header feature
 *
 * You can add an optional custom header image to header.php like so ...
 
 *
 * @link https://developer.wordpress.org/themes/functionality/custom-headers/
 *
 * @package software
 */

/**
 * Set up the WordPress core custom header feature.
 *
 * @uses spice_software_header_style()
 */
function spice_software_custom_header_setup() {
	add_theme_support( 'custom-header', apply_filters( 'spice_software_custom_header_args', array(
		'default-image'          => SPICE_SOFTWAREP_PLUGIN_URL.'/inc/images/Breadcrumb/breadcrumb-6.jpg',
		
		'width'                  => 1903,
		'height'                 => 350,
		'flex-height'            => true,

	) ) );
}
add_action( 'after_setup_theme', 'spice_software_custom_header_setup' );

