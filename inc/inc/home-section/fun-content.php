<!--Funfact Section-->
<?php
$home_fun_title = get_theme_mod('home_fun_section_title', __('<span>Doing the right thing,</span><br> at the right time.', 'spice-software-plus'));
$funfact_data = get_theme_mod('spice_software_funfact_content');
if (empty($funfact_data)) {
    $funfact_data = starter_funfact_json();
}

$fun_callout_background = get_theme_mod('funfact_callout_background', '');
if ($fun_callout_background != '') {
    ?>
    <section class="section-space funfact bg-default"  style="background-image:url('<?php echo esc_url($fun_callout_background); ?>'); background-repeat: no-repeat; width: 100%; background-size: cover;">
        <?php
    } else {
        ?>
        <section class="section-space funfact">
            <?php
        }
        ?>

        <!--<div class="overlay"></div>-->
        <div class="spice-software-fun-container container">
            <div class="row">
                <?php
                $funfact_data = json_decode($funfact_data);
                if (!empty($funfact_data)) {


                    foreach ($funfact_data as $funfact_iteam) {
                        $funfact_title = !empty($funfact_iteam->title) ? apply_filters('spice_software_translate_single_string', $funfact_iteam->title, 'Funfact section') : '';
                        $funfact_text = !empty($funfact_iteam->text) ? apply_filters('spice_software_translate_single_string', $funfact_iteam->text, 'Funfact section') : '';
                        $funfact_icon = !empty($funfact_iteam->icon_value) ? apply_filters('spice_software_translate_single_string', $funfact_iteam->icon_value, 'Funfact section') : '';
                        ?>

                        <div class="col-md-3 col-sm-6 col-xs-12">			
                            <div class="funfact-inner text-center">
                            <?php if($funfact_icon):?>
                                <i class="fa <?php echo $funfact_icon; ?> funfact-icon"></i>
                            <?php endif;?>
                                <?php if ($funfact_text != '') { ?>
                                    <p class="description"><?php echo $funfact_text; ?></p>
                                <?php } ?>
                                <?php if ($funfact_title != '') { ?>
                                    <h2 class="funfact-title"><?php echo $funfact_title; ?></h2>
                                <?php } ?>
                            </div>  
                        </div>

                        <?php
                    }
                }
                ?>
            </div>	 
        </div>
    </section>
    <!--/Funfact Section-->