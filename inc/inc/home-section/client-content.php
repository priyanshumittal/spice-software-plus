<?php
$isRTL = (is_rtl()) ? (bool) true : (bool) false;
$sponsorssettings = array( 'rtl' => $isRTL);

wp_register_script('spice-software-sponsors', SPICE_SOFTWAREP_PLUGIN_URL . '/inc/js/front-page/sponsors.js', array('jquery'));
wp_localize_script('spice-software-sponsors', 'sponsorssettings', $sponsorssettings);
wp_enqueue_script('spice-software-sponsors');

$client_options = get_theme_mod('spice_software_clients_content');
$clients_title = get_theme_mod('home_client_section_title', __('We Work With The Best Clients', 'spice-software-plus'));
$client_subtitle = get_theme_mod('home_client_section_discription', __('It is a long established fact that a reader will be distracted by the readable content.', 'spice-software-plus'));

$clt_bg_color = get_theme_mod('clt_bg_color');
if ($clt_bg_color != '') {
    ?>
    <section class="section-space sponsors"  style="background-color: <?php echo $clt_bg_color; ?>">
        <?php
    } else {
        ?>
        <section class="section-space sponsors" >
            <?php
        }
        ?>
        <div class="spice-software-client-container container">
            <?php if (!empty($clients_title) || !empty($client_subtitle)): ?>
            <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">		
                        <div class="section-header">
                            <?php if (!empty($clients_title)): ?>
                                <h2 class="section-title"><?php echo get_theme_mod('home_client_section_title', __('We Work With The Best Clients', 'spice-software-plus')); ?></h2>
                                <?php
                            endif;
                            if (!empty($client_subtitle)):
                                ?>
                                <h5 class="section-subtitle"><?php echo get_theme_mod('home_client_section_discription', __('It is a long established fact that a reader will be distracted by the readable content.', 'spice-software-plus')); ?></h5>
                            <?php endif; ?>
                        </div>  
                    </div>
                    </div>
                    <?php
                endif;
                ?>
                <div class="row pt-4">
                    <div id="clients-carousel" class="owl-carousel owl-theme col-md-12">
                        <?php
                        $t = true;
                        $client_options = json_decode($client_options);
                        if ($client_options != '') {
                            foreach ($client_options as $client_iteam) {
                                $client_image = !empty($client_iteam->image_url) ? apply_filters('spice_software_translate_single_string', $client_iteam->image_url, 'Client section') : '';
                                $client_link = !empty($client_iteam->link) ? apply_filters('spice_software_translate_single_string', $client_iteam->link, 'Client section') : '';
                                $open_new_tab = $client_iteam->open_new_tab;
                                ?>		
                                <div class="item">		
                                    <figure <?php if ($client_image != '') { ?>class="logo-scroll"<?php } ?>>
                                        <?php
                                        if (empty($client_link)) {
                                            ?>
                                            <img src="<?php echo $client_image; ?>" class="img-fluid px-4" >
                                            <?php
                                        } else {
                                            ?>
                                            <a href="<?php echo $client_link; ?>" <?php
                                            if ($open_new_tab == 'yes') {
                                                echo 'target="_blank"';
                                            }
                                            ?>>
                                                <img src="<?php echo $client_image; ?>" class="img-fluid px-4" >
                                            </a>
                                            <?php
                                        }
                                        ?>
                                    </figure>
                                </div>		
                                <?php
                            }
                        } else {
                            for ($i = 1; $i <= 6; $i++) {
                                ?> 	
                                <div class="item">
                                    <figure class="logo-scroll">
                                        <a href="#"><img src="<?php
                                            echo SPICE_SOFTWAREP_PLUGIN_URL . '/inc/images/sponsors/client-' . $i . '.png';
                                            ?>" class="img-fluid px-4" alt="Sponsors <?php echo $i; ?>"></a>
                                    </figure>
                                </div>
                                <?php
                            }
                        }
                        ?>	
                    </div>
                </div>
            </div>
        <!--</div>-->
        <!--</div>-->
    </section>