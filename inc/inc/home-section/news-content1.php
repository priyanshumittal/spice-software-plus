<?php
$index_news_link = get_theme_mod('home_blog_more_btn_link', __('#', 'spice-software-plus'));
$index_more_btn = get_theme_mod('home_blog_more_btn', __('View More', 'spice-software-plus'));
if (empty($index_news_link)) {
    $index_news_link = '#';
}
$latest_news_section_enable = get_theme_mod('latest_news_section_enable', true);
if ($latest_news_section_enable != false) {
    ?>
    <!-- Latest News section -->
    <section class="section-space blog bg-default home-blog">
        <div class="spice-software-newz container">
            <?php
            $home_news_section_title = get_theme_mod('home_news_section_title', __('Our Latest News', 'spice-software-plus'));
            $home_news_section_discription = get_theme_mod('home_news_section_discription', __('From our blog', 'spice-software-plus'));
            $home_meta_section_settings = get_theme_mod('home_meta_section_settings', true);
            if (($home_news_section_title) || ($home_news_section_discription) != '') {
                ?>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <div class="section-header">
                            <?php if ($home_news_section_title) { ?>
                                <h2 class="section-title"><?php echo $home_news_section_title; ?></h2>
                            <?php } ?>
                            <?php if ($home_news_section_discription) { ?>
                                <h5 class="section-subtitle"><?php echo $home_news_section_discription; ?></h5>
                            <?php } ?>
                        </div>
                    </div>						
                </div>
                <!-- /Section Title -->
            <?php } ?>
            <div class="row">
                <?php
                $no_of_post = get_theme_mod('spice_software_homeblog_counts', 3);
                $args = array('post_type' => 'post', 'post__not_in' => get_option("sticky_posts"), 'posts_per_page' => $no_of_post);
                query_posts($args);
                if (query_posts($args)) {
                    while (have_posts()):the_post(); {
                            ?>
                            <div class="col-md-<?php echo get_theme_mod('spice_software_homeblog_layout', 4); ?>">
                                <article class="post">	
                                    <?php if (has_post_thumbnail()) { ?>
                                        <figure class="post-thumbnail">
                                            <?php $defalt_arg = array('class' => "img-fluid"); ?>
                                            <!--<a href="<?php the_permalink(); ?>">-->
                                            <?php the_post_thumbnail('', $defalt_arg); ?>
                                            <!--</a>-->
                                            <div class="click-view">
                                                <a href="<?php the_post_thumbnail_url();?>"  data-lightbox="image" title="<?php the_title(); ?>"><i class="fa fa-link"></i></a>
                                            </div>
                                        </figure>	
                                    <?php } ?>
                                    <div class="post-content">
                                        <?php if ($home_meta_section_settings == true) { ?>
                                                <?php if(has_post_thumbnail()) { echo '<div class="entry-date">'; }else{ echo '<div class="remove-image">'; } ?>
                                                    <a href="<?php echo esc_url(home_url()); ?>/<?php echo esc_html(date('Y/m', strtotime(get_the_date()))); ?>"><span class="date"><?php echo esc_html(get_the_date()); ?></span></a>                                
                                                </div>

                                            <div class="entry-meta">
                                                <a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>">
                                                    <i class="fa fa-user"></i><span class="author postauthor"><?php echo get_the_author(); ?></span>
                                                </a>

                                                <?php
                                                $cat_list = get_the_category_list();
                                                if (!empty($cat_list)) {
                                                    ?>
                                                    <i class="fa fa-folder-open"></i><span class="cat-links postcat"><?php the_category(', '); ?></span>
                                                <?php } ?>
                                                <?php
                                                $tag_list = get_the_tag_list();
                                                if (!empty($tag_list)) {
                                                    ?>
                                                    <i class="fa fa-tag"></i>
                                                    <span class="cat-links posttag"><?php the_tags('', ', ', ''); ?></span>
                                                <?php } ?>

                                            </div>	
                                        <?php } ?>

                                        <header class="entry-header">
                                            <h4 class="entry-title">
                                                <a class="home-blog-title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                            </h4>
                                        </header>	

                                        <div class="entry-content">
                                        <?php the_excerpt(); 
                                        $blog_button = get_theme_mod('home_news_button_title', __('Read More', 'spice-software-plus'));
                                            if (!empty($blog_button)) {?>
                                            <p>
                                                <a href="<?php the_permalink(); ?>" class="more-link"><?php echo $blog_button; ?>
                                                    <i class="fa fa-long-arrow-right"></i>
                                                </a>
                                            </p>
                                        <?php } ?>
                                        </div>	
                                    </div>			
                                </article>
                            </div>
                            <?php
                        }
                    endwhile;
                }
                ?>
            </div>

            <?php if (!empty($index_more_btn)): ?>
                <div class="row index_extend_class">
                    <div class="mx-auto">
                        <a href="<?php echo $index_news_link; ?>" class="btn-small btn-default-dark business-view-more-post" <?php
                if (get_theme_mod('home_blog_more_btn_link_target', false) == true) {
                    echo "target='_blank'";
                };
                ?>><?php echo get_theme_mod('home_blog_more_btn', __('View More', 'spice-software-plus')); ?></a>
                    </div>
                </div>
            <?php endif; ?>



        </div>
    </section>
<?php } ?>