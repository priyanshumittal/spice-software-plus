<?php
$theme = wp_get_theme();
if('Spice Software Dark' == $theme->name) {
    $ss_contact_bg_color='#000000';
}
else{
    $ss_contact_bg_color='#ffffff';
}
$contact_data = get_theme_mod('spice_software_plus_contact_detail_content');

$spice_software_plus_contact_section_title = get_theme_mod('home_contact_section_title', __('Stay in touch with us', 'spice-software-plus'));
$contact_cta1_bg_enable = get_theme_mod('contact_bg_color_enable',false);
$contact_cta1_background = get_theme_mod('contact_bg_color', $ss_contact_bg_color);

if(get_theme_mod('spice_software_color_skin','light')=='dark')
{
?>
<section class="section-space contact-detail" style="background-color: <?php if ($contact_cta1_bg_enable==true):echo $contact_cta1_background; endif; ?>">
    <?php
}else{?>
<!--Contact info Section-->
<section class="section-space contact-detail"  style="background-color: <?php echo $contact_cta1_background; ?>">
    <?php
} 
?>
        <div class="spice-software-contact-container container">
            <?php if(!empty($spice_software_plus_contact_section_title)){ ?>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="section-header">
                        <h2 class="section-title"><?php echo $spice_software_plus_contact_section_title; ?></h2>
                    </div>
                </div>
            </div>
            <?php } ?>
            <div class="row">
                <?php
                $contact_data = json_decode($contact_data);
                if (!empty($contact_data)) {
                    foreach ($contact_data as $contact_item) {
                        $contact_icon = !empty($contact_item->icon_value) ? apply_filters('spice_software_plus_translate_single_string', $contact_item->icon_value, 'contact section') : '';
                       $contact_image = !empty($contact_item->image_url) ? apply_filters('spice_software_plus_translate_single_string', $contact_item->image_url, 'contact section') : '';
                        $contact_title = !empty($contact_item->title) ? apply_filters('spice_software_plus_translate_single_string', $contact_item->title, 'contact section') : '';
                        $contact_desc = !empty($contact_item->text) ? apply_filters('spice_software_plus_translate_single_string', $contact_item->text, 'contact section') : '';
                        ?>
                        <div class="col-md-4">
                            <div class="contact-area">
                                <div class="media">
                                    <?php
                                    if ($contact_item->choice == 'customizer_repeater_icon') {
                                        if ($contact_icon != '') {
                                            ?>
                                            <div class="contact-icon">
                                                <i class="fa <?php echo $contact_icon; ?>"></i>
                                            </div>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <div class="contact-icon">
                                            <img class="img-fluid" src="<?php echo $contact_image; ?>">
                                        </div>
                                    <?php } ?>
                                    <?php if ($contact_desc != '' || $contact_title != '') { ?>
                                        <div class="media-body">
                                            <?php
                                            if (!empty($contact_title)) {
                                                echo "<h6>$contact_title</h6>";
                                            }; 
                                            if (!empty($contact_desc)) {
                                                echo "<h4>$contact_desc</h4>";
                                            }?>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    <?php
                    }
                } else {?>          
                <div class="col-md-4">
                    <div class="contact-area">
                        <div class="media">
                            <div class="contact-icon">
                                <i class="fa fa fa-phone"></i>
                            </div>
                            <div class="media-body">
                                <h6>Have a question? Call us now</h6>
                                <h4>+82 334 843 52</h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="contact-area">
                        <div class="media">
                            <div class="contact-icon">
                                <i class="fa-regular fa-clock"></i>
                            </div>
                            <div class="media-body">
                                <h6>We are open Mon-Fri</h6>
                                <h4>Mon - Fri 08.00 - 18.00</h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="contact-area">
                        <div class="media">
                            <div class="contact-icon">
                                <i class="fa-solid fa-envelope"></i>
                            </div>
                            <div class="media-body">
                                <h6>Drop us an mail</h6>
                                <h4>info@yoursupport.com</h4>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
            </div>
        </div>
    </section> 
    <!--/Contact info Section-->