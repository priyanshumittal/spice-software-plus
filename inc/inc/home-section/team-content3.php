<?php
$team_options = get_theme_mod('spice_software_team_content');
if (empty($team_options)) {
    $team_options = starter_team_json();
}
$team_animation_speed = get_theme_mod('team_animation_speed', 3000);
$team_smooth_speed = get_theme_mod('team_smooth_speed', 1000);
$team_nav_style = get_theme_mod('team_nav_style', 'bullets');
$isRTL = (is_rtl()) ? (bool) true : (bool) false;
$teamsettings = array('team_animation_speed' => $team_animation_speed, 'team_smooth_speed' => $team_smooth_speed, 'team_nav_style' => $team_nav_style, 'rtl' => $isRTL);
wp_register_script('spice-software-team', SPICE_SOFTWAREP_PLUGIN_URL . 'inc/js/front-page/team.js', array('jquery'));
wp_localize_script('spice-software-team', 'team_settings', $teamsettings);
wp_enqueue_script('spice-software-team');
?>
<section class="section-space team team3">
    <div class="spice-software-team-container container">
        <?php
        $home_team_section_title = get_theme_mod('home_team_section_title', __('The Team', 'spice-software-plus'));
        $home_team_section_discription = get_theme_mod('home_team_section_discription', __('Meet Our Experts', 'spice-software-plus'));
        if (($home_team_section_title) || ($home_team_section_discription) != '') {
            ?>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="section-header">
                        <?php if (!empty($home_team_section_title)): ?>
                            <h2 class="section-title"><?php echo $home_team_section_title; ?></h2>
                            <?php
                        endif;

                        if (!empty($home_team_section_discription)):
                            ?>
                            <h5 class="section-subtitle"><?php echo $home_team_section_discription; ?></h5>
                        <?php endif; ?>
                    </div>
                </div>						
            </div>
        <?php } ?>
        <div class="row">
             <?php if(get_page_template_slug()!="template-team-content-7.php"):?>
            <div id="team-carousel" class="owl-carousel owl-theme col-lg-12">
                <?php endif;
                $team_options = json_decode($team_options);
                if ($team_options != '') {
                    foreach ($team_options as $team_item) {
                        $image = !empty($team_item->image_url) ? apply_filters('spice_software_translate_single_string', $team_item->image_url, 'Team section') : '';
                        $title = !empty($team_item->membername) ? apply_filters('spice_software_translate_single_string', $team_item->membername, 'Team section') : '';
                        $subtitle = !empty($team_item->designation) ? apply_filters('spice_software_translate_single_string', $team_item->designation, 'Team section') : '';
                       // $link = !empty($team_item->link) ? apply_filters('spice_software_translate_single_string', $team_item->link, 'Team section') : '';
                        $open_new_tab = $team_item->open_new_tab;
                        ?>
                        <div <?php if(get_page_template_slug()=="template-team-content-7.php") { ?>class="col-lg-4 col-md-6 col-sm-12"<?php } else { ?> class="item" <?php } ?>>
                            <div class="team-grid text-center">
                                <?php if(!empty($image)){ ?>
                                <div class="img-holder">
                                    <img src="<?php echo esc_url($image); ?>" class="img-fluid"alt="<?php echo $title; ?>">
                                </div>
                                <?php } ?>

                                <div class="card-body">
                                    <?php if (!empty($title)) : ?>
                                    <h4 class="font-weight-bold mt-1 mb-3"><?php echo esc_html($title); ?></h4>
                                    <?php endif;
                                    if (!empty($subtitle)) : ?>
                                        <p class="font-weight-bold dark-grey-text"><?php echo esc_html($subtitle); ?></p>
                                    <?php endif; ?>
                                    <!-- Social Icons -->

                                    <?php
                                    $icons = html_entity_decode($team_item->social_repeater);
                                    $icons_decoded = json_decode($icons, true);
                                    $socails_counts = $icons_decoded;
                                    if (!empty($socails_counts)) :
                                        if (!empty($icons_decoded)) : ?>
                                            <ul class="list-inline list-unstyled ml-0 mt-1 mb-1">
                                                <?php
                                                foreach ($icons_decoded as $value) {
                                                    $social_icon = !empty($value['icon']) ? apply_filters('spice_software_translate_single_string', $value['icon'], 'Team section') : '';
                                                    $social_link = !empty($value['link']) ? apply_filters('spice_software_translate_single_string', $value['link'], 'Team section') : '';
                                                    if (!empty($social_icon)) {
                                                        ?>							
                                                        <li class="list-inline-item"><a class="p-2 fa-lg fb-ic" <?php
                                if ($open_new_tab == 'yes') {
                                    echo 'target="_blank"';
                                }
                                                        ?> href="<?php echo esc_url($social_link); ?>"><i class="fa <?php echo esc_attr($social_icon); ?> " aria-hidden="true"></i></a></li>
                                                                                        <?php
                                                            }
                                                        }
                                                        ?>
                                            </ul>
                                            <?php
                                        endif;
                                    endif;
                                    ?>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                }
                ?>        
           <?php if(get_page_template_slug()!="template-team-content-7.php"):?>
            </div>
        <?php endif;?>
        </div>
    </div>
</section>	