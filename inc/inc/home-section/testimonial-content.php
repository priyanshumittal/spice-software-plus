<?php
$theme = wp_get_theme();
if('Spice Software Dark' == $theme->name) {
    $ss_testimonial_design=4;
}
else{
    $ss_testimonial_design=1;
}
$templ_slug=get_page_template_slug();
$testi_layout=get_theme_mod('home_testimonial_design_layout', $ss_testimonial_design);
$about_testi_layout=get_theme_mod('about_testimonial_design_layout',1);

switch ($templ_slug) {

            case 'template-aboutus.php':
                testimonial_design('testimonial-carousel'.$about_testi_layout, 'carousel');               
                break;    

            case 'template-service.php':
                testimonial_design('testimonial-carousel1', 'carousel');               
                break;

            case 'template-service-2.php':
                testimonial_design('testimonial-carousel2', 'carousel');
                break;

            case 'template-service-3.php':
                testimonial_design('testimonial-carousel3', 'carousel');
                break;

            case 'template-service-4.php':
                testimonial_design('testimonial-carousel4', 'carousel');
                break;

            case 'template-testimonial-1.php':
                testimonial_design('testimonial-carousel1', 'carousel');               
                break;

            case 'template-testimonial-2.php':
                testimonial_design('testimonial-carousel2', 'carousel');
                break;

            case 'template-testimonial-3.php':
                testimonial_design('testimonial-carousel3', 'carousel');
                break;

            case 'template-testimonial-4.php':
                testimonial_design('testimonial-carousel4', 'carousel');
                break;

            case 'template-testimonial-5.php':
                testimonial_design('testimonial-carousel1', 'grid');
                break;          

            case 'template-testimonial-6.php':
                testimonial_design('testimonial-carousel2', 'grid');
                break;

            case 'template-testimonial-7.php':
                testimonial_design('testimonial-carousel3', 'grid');
                break;
                
            case 'template-testimonial-8.php':
                testimonial_design('testimonial-carousel4', 'grid');
                break;

            default:
                testimonial_design('testimonial-carousel'.$testi_layout, 'carousel');   
            break;
        }