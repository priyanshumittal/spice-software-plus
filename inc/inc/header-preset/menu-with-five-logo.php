<div class="<?php if(get_theme_mod('sticky_header_clr_enable',false)==true):?>clr <?php endif;?><?php if(get_theme_mod('sticky_header_enable',false)===true):?>header-sticky<?php endif;?> <?php if(get_theme_mod('sticky_header_animation','')=='shrink'): echo 'shrink'; endif;?> <?php echo get_theme_mod('header_logo_placing','left');?>">
	<div class="header-logo index2">
	  	<div class="container">
	  		<div class="row">
	  			<div class="col-md-7">
	  				<?php the_custom_logo(); do_action('spice_software_plus_sticky_header_logo');
						if((get_option('blogname')!='') || (get_option('blogdescription')!='')):?>
						<div class="custom-logo-link-url"> 
							<?php if(get_option('blogname')!=''):?>
				    			<h2 class="site-title"><a class="site-title-name" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
				    			</h2>
				    		<?php endif;

							$description = get_bloginfo( 'description', 'display' );
							if(get_option('blogdescription')!='')
							{
							if ( $description || is_customize_preview() ) : ?>
								<p class="site-description"><?php echo esc_html($description); ?></p>
							<?php endif; 
							}?>
						</div>
					<?php endif;?>
	  			</div>
	  			<div class="col-md-5">
	  				<?php include SPICE_SOFTWAREP_PLUGIN_DIR.'/inc/inc/seprate-search-cart.php';?>
	  			</div>
			</div>
		</div>
	</div>

	<!-- Navbar -->
	<nav class="navbar navbar-expand-lg navbar-light navbar2">
		<div class="container">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			  <span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarNavDropdown">
				<!-- Right Nav -->
				<?php 
					$spice_software_menu_button = '<ul class="nav navbar-nav mr-auto">%3$s';
	          if(get_theme_mod('after_menu_btn_new_tabl',false)==true) { $spice_software_menu_target="_blank";}
	          else { $spice_software_menu_target="_self"; }
	          if(!empty(get_theme_mod('after_menu_btn_txt')) && (get_theme_mod('after_menu_multiple_option')=='menu_btn')):
	              $spice_software_menu_button .= '<li class="nav-item radix-btn menu-item"><a target='.$spice_software_menu_target.' class="spice_software_header_btn" href='.get_theme_mod('after_menu_btn_link','#').'>'.get_theme_mod('after_menu_btn_txt').'</a>';
	          endif;
	          if(!empty(get_theme_mod('after_menu_html')) && (get_theme_mod('after_menu_multiple_option')=='html')):
	             $spice_software_menu_button .= '<li class="nav-item radix-btn menu-item radix-html">'.get_theme_mod('after_menu_html'); endif;  
	          if(get_theme_mod('after_menu_multiple_option')=='top_menu_widget'):
	        	ob_start();
	        	$sidebar = spice_software_plus_footer_widget_area( 'menu-widget-area' );
	        	$sidebar = ob_get_contents();
	      		$spice_software_menu_button .= '<li class="nav-item">'.$sidebar.'</li>';
	      		ob_end_clean();
	      		endif;
	                        
	          $spice_software_menu_button .= '</ul>';
	          $menu_class='';
	         wp_nav_menu( array
	             (
	             'theme_location'=> 'spice-software-primary',
	             'container'  => '',
	             'menu_class'    => 'nav navbar-nav mr-auto '.$menu_class.'',
	             'items_wrap'  => $spice_software_menu_button,
	             'fallback_cb'   => 'spice_software_fallback_page_menu',
	             'walker'        => new Spice_Software_Nav_Walker()
	             ));

				if ( class_exists( 'Mega_Menu' ) ) {
				  include SPICE_SOFTWAREP_PLUGIN_DIR.'/inc/inc/mega-menu.php';
				}?>
		  </div>
		</div>
	</nav>
</div>
<!--/End of Navbar -->