<div class="col-lg-6 col-md-6 col-sm-6 right-info">
<?php
$foot_section_2 = get_theme_mod('footer_bar_sec2','none');
switch ( $foot_section_2 )
      {
        case 'none':
        break;

        case 'footer_menu':
        echo spice_software_plus_footer_bar_menu();
        break;

        case 'custom_text':
        echo get_theme_mod('footer_copyright_2','<span class="copyright">'.__( 'Proudly powered by <a href="https://wordpress.org"> WordPress</a> | Theme: <a href="https://spicethemes.com/spice-software-wordpress-theme" rel="nofollow">Spice Software</a> by <a href="https://spicethemes.com" rel="nofollow">Spicethemes</a>', 'spice-software-plus').'</span>');
        break;

        case 'widget':
        spice_software_plus_footer_widget_area('footer-bar-2');
        break;
      }
?>
</div>	