<div class="site-info text-center layout-1">
    <div class="container">
        <?php $foot_section_1 = get_theme_mod('footer_bar_sec1','custom_text');
              switch ( $foot_section_1 )
                {
                    case 'none':
                    break;

                    case 'footer_menu':
                    echo spice_software_plus_footer_bar_menu();
                    break;

                    case 'custom_text':
                    echo get_theme_mod('footer_copyright','<span class="copyright">'.__( 'Proudly powered by <a href="https://wordpress.org"> WordPress</a> | Theme: <a href="https://spicethemes.com/spice-software-wordpress-theme" rel="nofollow">Spice Software</a> by <a href="https://spicethemes.com" rel="nofollow">Spicethemes</a>', 'spice-software-plus').'</span>');
                    break;

                    case 'widget':
                    spice_software_plus_footer_widget_area('footer-bar-1');
                    break;
                }
        ?> 
    </div>
<!--</div>-->
