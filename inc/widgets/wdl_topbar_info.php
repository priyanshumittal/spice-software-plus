<?php
//error_reporting(0);

// Register and load the widget
function spice_software_header_topbar_info_widget() {
    register_widget('spice_software_header_topbar_info_widget');
}

add_action('widgets_init', 'spice_software_header_topbar_info_widget');

// Creating the widget
class spice_software_header_topbar_info_widget extends WP_Widget {

    function __construct() {
        parent::__construct(
                'spice_software_header_topbar_info_widget', // Base ID
                esc_html__('Spice Software: Header info widget', 'spice-software-plus' ), // Widget Name
                array(
                    'classname' => 'spice_software_header_topbar_info_widget',
                    'phone_number' => esc_html__('Topbar header info widget.', 'spice-software-plus' ),
                ),
                array(
                    'width' => 600,
                )
        );
    }

    public function widget($args, $instance) {

        //echo $args['before_widget']; 

        echo $args['before_widget'];
        ?>
        <ul class="head-contact-info">
            <li class="phone">
                <?php if (!empty($instance['spice_software_phone_icon'])) { ?>
                    <i class="fa <?php echo esc_attr($instance['spice_software_phone_icon']); ?>"></i>
                <?php } else { ?> 
                    <i class="fa fa-solid fa-phone"></i>
                <?php } ?>	
                <?php
                if (!empty($instance['phone_number'])) {
                    echo esc_html($instance['phone_number']);
                } else {
                    echo esc_html($instance['phone_number']);
                }
                ?>
            </li>
            <li class="envelope">
                <?php if (!empty($instance['spice_software_email_icon'])) { ?>
                    <i class="fa <?php echo esc_attr($instance['spice_software_email_icon']); ?>"></i>
                <?php } else { ?> 
                    <i class="fa fa-solid fa-envelope"></i>
                <?php } ?>	
                <a href="mailto:abc@example.com"> <?php
                    if (!empty($instance['spice_software_email_id'])) {
                        echo esc_html($instance['spice_software_email_id']);
                    }
                    ?></a>
            </li>
            <li class="address-info">
                <?php if (!empty($instance['spice_software_location_icon'])) { ?>
                    <i class="fa <?php echo esc_attr($instance['spice_software_location_icon']); ?>"></i>
                <?php } else { ?> 
                    <i class="fa fa-solid fa-location-dot"></i>
                <?php } ?>	
                <?php
                if (!empty($instance['spice_software_location_text'])) {
                    echo esc_html($instance['spice_software_location_text']);
                }
                ?>
            </li>
        </ul>
        <?php
        echo $args['after_widget'];
    }

    // Widget Backend
    public function form($instance) {

        if (isset($instance['spice_software_phone_icon'])) {
            $spice_software_phone_icon = $instance['spice_software_phone_icon'];
        } else {
            $spice_software_phone_icon = esc_html__('fa fa-phone', 'spice-software-plus' );
        }

        if (isset($instance['phone_number'])) {
            $phone_number = $instance['phone_number'];
        } else {
            $phone_number = esc_html__('+99 999-999-9999', 'spice-software-plus' );
        }

        if (isset($instance['spice_software_email_icon'])) {
            $spice_software_email_icon = $instance['spice_software_email_icon'];
        } else {
            $spice_software_email_icon = esc_html__('fa fa-envelope-o', 'spice-software-plus' );
        }

        if (isset($instance['spice_software_email_id'])) {
            $spice_software_email_id = $instance['spice_software_email_id'];
        } else {
            $spice_software_email_id = esc_html__('abc@example.com', 'spice-software-plus' );
        }

        if (isset($instance['spice_software_location_icon'])) {
            $spice_software_location_icon = $instance['spice_software_location_icon'];
        } else {
            $spice_software_location_icon = esc_html__('fa fa-map-marker', 'spice-software-plus' );
        }

        if (isset($instance['spice_software_location_text'])) {
            $spice_software_location_text = $instance['spice_software_location_text'];
        } else {
            $spice_software_location_text = esc_html__('9999 Nemo Enim Ipsam, Voluptatem', 'spice-software-plus' );
        }

        // Widget admin form
        ?>

        <label for="<?php echo esc_attr($this->get_field_id('spice_software_phone_icon')); ?>"><?php esc_html_e('Font Awesome icon', 'spice-software-plus' ); ?></label>
        <input class="widefat" id="<?php echo esc_attr($this->get_field_id('spice_software_phone_icon')); ?>" name="<?php echo esc_attr($this->get_field_name('spice_software_phone_icon')); ?>" type="text" value="<?php
        if ($spice_software_phone_icon)
            echo esc_attr($spice_software_phone_icon);
        else
            echo esc_attr('fa fa-solid fa-phone', 'spice-software-plus' );
        ?>" />
        <span><?php esc_html_e('Link to get Font Awesome icons', 'spice-software-plus' ); ?><a href="<?php echo esc_url('https://fontawesome.com/v4/icons/'); ?>" target="_blank" ><?php esc_html_e('fa-icon', 'spice-software-plus' ); ?></a></span>
        <br><br>

        <label for="<?php echo esc_attr($this->get_field_id('phone_number')); ?>"><?php esc_html_e('Phone', 'spice-software-plus' ); ?></label>
        <input class="widefat" id="<?php echo esc_attr($this->get_field_id('phone_number')); ?>" name="<?php echo esc_attr($this->get_field_name('phone_number')); ?>" type="text" value="<?php
        if ($phone_number)
            echo esc_attr($phone_number);
        else
            esc_html_e('+99 999-999-9999', 'spice-software-plus' );
        ?>" /><br><br>

        <label for="<?php echo esc_attr($this->get_field_id('spice_software_email_icon')); ?>"><?php esc_html_e('Font Awesome icon', 'spice-software-plus' ); ?></label>
        <input class="widefat" id="<?php echo esc_attr($this->get_field_id('spice_software_email_icon')); ?>" name="<?php echo esc_attr($this->get_field_name('spice_software_email_icon')); ?>" type="text" value="<?php
        if ($spice_software_email_icon)
            echo esc_attr($spice_software_email_icon);
        else
            echo esc_attr('fa fa-solid fa-phone','spice-software-plus' );
        ?>" />
        <span><?php esc_html_e('Link to get Font Awesome icons', 'spice-software-plus' ); ?><a href="<?php echo esc_url('https://fontawesome.com/v4/icons/'); ?>" target="_blank" ><?php esc_html_e('fa-icon', 'spice-software-plus' ); ?></a></span><br><br>

        <label for="<?php echo esc_attr($this->get_field_id('spice_software_email_id')); ?>"><?php esc_html_e('Email', 'spice-software-plus' ); ?></label>
        <input class="widefat" id="<?php echo esc_attr($this->get_field_id('spice_software_email_id')); ?>" name="<?php echo esc_attr($this->get_field_name('spice_software_email_id')); ?>" type="text" value="<?php
        if ($spice_software_email_id)
            echo esc_attr($spice_software_email_id);
        else
            esc_html_e('abc@example.com', 'spice-software-plus' );
        ?>" /><br><br>

        <label for="<?php echo esc_attr($this->get_field_id('spice_software_location_icon')); ?>"><?php esc_html_e('Font Awesome icon', 'spice-software-plus' ); ?></label>
        <input class="widefat" id="<?php echo esc_attr($this->get_field_id('spice_software_location_icon')); ?>" name="<?php echo esc_attr($this->get_field_name('spice_software_location_icon')); ?>" type="text" value="<?php
        if ($spice_software_location_icon)
            echo esc_attr($spice_software_location_icon);
        else
            echo esc_attr('fa fa-solid fa-location-dot','spice-software-plus' );
        ?>" />
        <span><?php esc_html_e('Link to get Font Awesome icons', 'spice-software-plus' ); ?><a href="<?php echo esc_url('https://fontawesome.com/v4/icons/'); ?>" target="_blank" ><?php esc_html_e('fa-icon', 'spice-software-plus' ); ?></a></span><br><br>

        <label for="<?php echo esc_attr($this->get_field_id('spice_software_location_text')); ?>"><?php esc_html_e('Address', 'spice-software-plus' ); ?></label>
        <input class="widefat" id="<?php echo esc_attr($this->get_field_id('spice_software_location_text')); ?>" name="<?php echo esc_attr($this->get_field_name('spice_software_location_text')); ?>" type="text" value="<?php
               if ($spice_software_location_text)
                   echo esc_attr($spice_software_location_text);
               else
                   esc_html_e('9999 Nemo Enim Ipsam, Voluptatem', 'spice-software-plus' );
               ?>" /><br><br>



        <?php
    }

    // Updating widget replacing old instances with new
    public function update($new_instance, $old_instance) {

        $instance = array();
        $instance['spice_software_phone_icon'] = (!empty($new_instance['spice_software_phone_icon']) ) ? spice_software_sanitize_text($new_instance['spice_software_phone_icon']) : '';
        $instance['phone_number'] = (!empty($new_instance['phone_number']) ) ? spice_software_sanitize_text($new_instance['phone_number']) : '';
        $instance['spice_software_email_icon'] = (!empty($new_instance['spice_software_email_icon']) ) ? spice_software_sanitize_text($new_instance['spice_software_email_icon']) : '';
        $instance['spice_software_email_id'] = (!empty($new_instance['spice_software_email_id']) ) ? $new_instance['spice_software_email_id'] : '';
        $instance['spice_software_location_icon'] = (!empty($new_instance['spice_software_location_icon']) ) ? spice_software_sanitize_text($new_instance['spice_software_location_icon']) : '';
        $instance['spice_software_location_text'] = (!empty($new_instance['spice_software_location_text']) ) ? spice_software_sanitize_text($new_instance['spice_software_location_text']) : '';

        return $instance;
    }

}
