<?php 
/**
 * Template Name: About Us
 */
get_header();?>
<!-- About Section -->
<?php if ( $post->post_content!=="" ) { ?>
<section class="page-section-space about-section">
	<div class="container<?php echo esc_html(spice_software_container());?>">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<?php 
				the_post();
				the_content(); ?>			
			</div>	
		</div>
	</div>
</section>
<?php } ?>
<div class="clearfix"></div>
<!-- /End of About Section -->

<?php
if(get_theme_mod('about_testimonial_enable',true) == true) {
include_once(SPICE_SOFTWAREP_PLUGIN_DIR.'/inc/inc/home-section/testimonial-content.php');?>
<div class="clearfix"></div>
<?php } ?>

<?php
if(get_theme_mod('about_funfact_enable',true) == true) {
include_once(SPICE_SOFTWAREP_PLUGIN_DIR.'/inc/inc/home-section/fun-content.php');?>
<div class="clearfix"></div>
<?php } ?>

<?php
if(get_theme_mod('about_team_enable',true) == true) {
include_once(SPICE_SOFTWAREP_PLUGIN_DIR.'/inc/inc/home-section/team-content'.get_theme_mod('about_team_design_layout',1).'.php');?>
<div class="clearfix"></div>
<?php } ?>

<?php
if(get_theme_mod('about_contact_detail_enable',true) == true) {
include_once(SPICE_SOFTWAREP_PLUGIN_DIR.'/inc/inc/home-section/contact-content.php');?>
<div class="clearfix"></div>
<?php } ?>

<?php get_footer();?>