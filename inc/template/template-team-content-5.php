<?php 
/**
 * Template Name: Team Grid Style One
*/
get_header();
if ( $post->post_content!=="" ) { ?>
<section class="page-section-space about-section">
	<div class="container<?php echo esc_html(spice_software_container());?>">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<?php 
				the_post();
				the_content(); ?>			
			</div>	
		</div>
	</div>
</section>
<?php } 

if(get_theme_mod('team_enable',true) == true) 
{ 
	include(SPICE_SOFTWAREP_PLUGIN_DIR.'/inc/inc/home-section/team-content1.php');

} 

if(get_theme_mod('team_funfact_enable',true) == true) 
{
	include(SPICE_SOFTWAREP_PLUGIN_DIR.'/inc/inc/home-section/fun-content.php');
} 

if(get_theme_mod('team_contact_detail_enable',true) == true) 
{
	include(SPICE_SOFTWAREP_PLUGIN_DIR.'/inc/inc/home-section/contact-content.php');
} 
get_footer();?>