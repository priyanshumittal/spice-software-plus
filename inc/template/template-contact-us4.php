<?php 
/**
 * Template Name: Contact Four
 */
get_header();
$contact_cf7_title = get_theme_mod('contact_cf7_title', __("Don't Hesitate To Contact Us", 'spice-software-plus'));?>

<!--Contact info Section-->
<section class="section-space contact-info contact3 bg-default">
    <div class="container<?php echo esc_html(spice_software_container());?>">     
        <div class="row">
           <div class="col-md-8">
            <!--Contact form section-->
            <?php if(get_theme_mod('contact_form_shortcode')):?>                            
                <div class="contact-form bg-default">
                    <div class="row"> 
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <?php if(!empty($contact_cf7_title)):?>
                            <div class="section-header">
                                <h2 class="section-title"><?php echo $contact_cf7_title; ?></h2>
                            </div>
                            <?php endif;?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div>
                                <?php
                                 echo do_shortcode(get_theme_mod('contact_form_shortcode')); 
                                ?>
                            </div>                            
                        </div>
                    </div>
                </div>
            <?php endif;?>
            <!--/Contact form section-->

                <!--Contact map section-->
                <div class="contact-form-map">
                    <div class="row">
                        <?php if(get_theme_mod('contact_google_map_shortcode')):?>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div id="google-map">
                                <?php echo do_shortcode(get_theme_mod('contact_google_map_shortcode')); ?>
                            </div>
                        </div>
                        <?php endif;?>
                    </div>
                </div>
                <!--/End of Contact map section-->

            </div>

            <!-- Contact Details Section -->
            <div class="col-md-4 pt-5">
                <?php $contact_data=content_contact_data('spice_software_plus_contact_content');
                $contact_data = json_decode($contact_data);
                if (!empty($contact_data))
                    { 
                        foreach($contact_data as $contact_item)
                            { 
                                $contact_title = ! empty( $contact_item->title ) ? apply_filters( 'spice_software_plus_translate_single_string', $contact_item->title, 'Contact section' ) : '';
                
                                $contact_text = ! empty( $contact_item->text ) ? apply_filters( 'spice_software_plus_translate_single_string', $contact_item->text, 'Contact section' ) : '';
                
                                $contact_icon = ! empty( $contact_item->icon_value ) ? apply_filters( 'spice_software_plus_translate_single_string', $contact_item->icon_value, 'Contact section' ) : '';?>
                                <div class="contact-detail-area">
                                       <?php if($contact_icon):?>
                                           <span><i class="fa <?php echo $contact_icon; ?>"></i></span>
                                        <?php endif;?>
                                        <?php if($contact_title):?>
                                           <h5><?php echo $contact_title; ?></h5>
                                        <?php endif;?>
                                        <?php if($contact_text):?>
                                           <address><?php echo $contact_text; ?></address>
                                        <?php endif;?>  
                                </div>
                    <?php } 
                    } ?>
            </div>
            <!-- /End of Contact Details Section -->

        </div>
    </div>
</section>
<!-- Contact form section-->
<?php get_footer();?>