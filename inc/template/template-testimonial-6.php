<?php 
/**
 * Template Name: Testimonial Grid Style Two
*/
get_header();
if ( $post->post_content!=="" ) { ?>
<section class="page-section-space about-section">
	<div class="container<?php echo esc_html(spice_software_container());?>">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<?php 
				the_post();
				the_content(); ?>			
			</div>	
		</div>
	</div>
</section>
<?php } 
if(get_theme_mod('testimonial_enable',true) == true) 
{ 
	include_once(SPICE_SOFTWAREP_PLUGIN_DIR.'/inc/inc/home-section/testimonial-content.php');
}
if(get_theme_mod('testimonial_funfact_enable',true) == true) 
{
	include_once(SPICE_SOFTWAREP_PLUGIN_DIR.'/inc/inc/home-section/fun-content.php');
} 

if(get_theme_mod('testimonial_contact_detail_enable',true) == true) 
{
	include_once(SPICE_SOFTWAREP_PLUGIN_DIR.'/inc/inc/home-section/contact-content.php');
} 
get_footer();?>