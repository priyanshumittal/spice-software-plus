<?php 
/**
 * Template Name: Contact Two
 */
get_header();
$contact_cf7_title = get_theme_mod('contact_cf7_title', __("Don't Hesitate To Contact Us", 'spice-software-plus'));?>

<!-- Contact Details Section -->
<section class="section-space contact-info bg-default">
    <div class="container<?php echo esc_html(spice_software_container());?>">
        <div class="row">
            <?php $contact_data=content_contact_data('spice_software_plus_contact_content');
            $contact_data = json_decode($contact_data);
                if (!empty($contact_data))
                    { 
                        foreach($contact_data as $contact_item)
                            { 
                                $contact_title = ! empty( $contact_item->title ) ? apply_filters( 'spice_software_translate_single_string', $contact_item->title, 'Contact section' ) : '';
                
                                $contact_text = ! empty( $contact_item->text ) ? apply_filters( 'spice_software_translate_single_string', $contact_item->text, 'Contact section' ) : '';
                
                                $contact_icon = ! empty( $contact_item->icon_value ) ? apply_filters( 'spice_software_translate_single_string', $contact_item->icon_value, 'Contact section' ) : '';?>
                                
                                <div class="col-md-4">
                                    <div class="contact-detail-area">
                                        <?php if($contact_icon):?>
                                           <span><i class="fa <?php echo $contact_icon; ?>"></i></span>
                                        <?php endif;?>
                                        <?php if($contact_title):?>
                                           <h5><?php echo $contact_title; ?></h5>
                                        <?php endif;?>
                                        <?php if($contact_text):?>
                                           <address><?php echo $contact_text; ?></address>
                                        <?php endif;?>  
                                    </div>               
                                </div>
                    <?php } 
                    } ?>
        </div>      
    </div>
</section>
<!-- /End of Contact Details Section -->

<!-- Contact form & Contact Form Section -->
<section class="section-space contact-form contact1">
    <div class="container<?php echo esc_html(spice_software_container());?>">
        <div class="row">
            <?php if(get_theme_mod('contact_google_map_shortcode')):?>
            <div class="col-md-6 col-sm-6 col-xs-6">
                <div class="contact-form-map">
                    <div id="google-map">
                        <?php echo do_shortcode(get_theme_mod('contact_google_map_shortcode')); ?>
                    </div>
                </div>
            </div>
            <?php endif;
            if(get_theme_mod('contact_form_shortcode')):?>
                <div class="col-md-6 col-sm-6 col-xs-6">
                    <?php if(!empty($contact_cf7_title)):?>
                    <div class="section-header">
                        <h2 class="section-title"><?php echo $contact_cf7_title; ?></h2>
                    </div>
                    <?php endif;?>
                    <div>
                        <?php
                        echo do_shortcode(get_theme_mod('contact_form_shortcode')); 
                        ?>  
                    </div>                
                </div>
            <?php endif;?>
        </div>
    </div>
</section>
   <!-- /End of Contact form & Contact Form section -->
<?php get_footer();?>