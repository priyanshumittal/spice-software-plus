<?php
/**
 *  Template Name: Blog Masonry 3 Column
 */
get_header();
$exc_length = get_theme_mod('spice_software_blog_content', 'excerpt'); ?>
<section class="section-space blog blog-masonry <?php
if ($exc_length != 'excerpt') {
    echo 'threemasonary-full-width';
}
?>">
    <div class="container<?php echo esc_html(spice_software_blog_post_container());?>">
        <?php
        if (get_theme_mod('post_nav_style_setting', 'pagination') == "pagination") {
            ?>
            <div class="grid">
                <?php
                if (get_query_var('paged')) {
                    $paged = get_query_var('paged');
                } elseif (get_query_var('page')) {
                    $paged = get_query_var('page');
                } else {
                    $paged = 1;
                }
                $args = array('post_type' => 'post', 'paged' => $paged);
                $loop = new WP_Query($args);
                if ($loop->have_posts()):
                    while ($loop->have_posts()): $loop->the_post();
                        ?>
                        <div class="grid-item col-md-4">
                            <?php include(SPICE_SOFTWAREP_PLUGIN_DIR.'/inc/template-parts/content-blog-template.php'); ?>
                        </div>
                        <?php
                    endwhile;
                endif;
                ?>
            </div>
            <!-- pagination function -->
            <div class="row justify-content-center">
                <?php
                echo '<div class="row justify-content-center">';
                $obj = new spice_software_plus_pagination();
                $obj->spice_software_plus_page($loop);
                echo '</div>';
                ?>
            </div>
            <?php
        }
        if (get_theme_mod('post_nav_style_setting', 'pagination') != "pagination") {
            echo do_shortcode('[ajax_posts]');
        }
        ?>
    </div>
</section>
<?php get_footer(); ?>