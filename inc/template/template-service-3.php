<?php 
/**
 * Template Name: Service Three
 */
get_header();
?>
<!-- Service Section -->
<?php if ( $post->post_content!=="" ) { ?>
	<section class="section-space services">
		<div class="container<?php echo esc_html(spice_software_container());?>">
			<div class="row">
				<div class="col-md-12 col-sm-12">
				<?php 
				the_post();
				the_content(); ?>			
				</div>	
			</div>
		</div>
	</section>
<?php } ?>
<div class="clearfix"></div>
<!-- /End of Service Section -->
<?php
if(get_theme_mod('services_enable',true) == true) {
   include_once(SPICE_SOFTWAREP_PLUGIN_DIR.'/inc/inc/home-section/service-content.php');?>
	<div class="clearfix"></div>
<?php } ?>

<?php if(get_theme_mod('service_funfact_enable',true) == true) { 
 include_once(SPICE_SOFTWAREP_PLUGIN_DIR.'/inc/inc/home-section/fun-content.php'); ?>
	<div class="clearfix"></div>
<?php } ?>

<?php if(get_theme_mod('service_testimonial_enable',true) == true) {
	include_once(SPICE_SOFTWAREP_PLUGIN_DIR.'/inc/inc/home-section/testimonial-content.php'); ?>
	<div class="clearfix"></div>
<?php } ?>

<?php if(get_theme_mod('service_contact_detail_enable',true) == true) {
	include_once(SPICE_SOFTWAREP_PLUGIN_DIR.'/inc/inc/home-section/contact-content.php');?>
	<div class="clearfix"></div>
<?php } 
get_footer();?>