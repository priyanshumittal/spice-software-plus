// OWL SLIDER CUSTOM JS

jQuery(document).ready(function () {

    /* Preloader */
    jQuery(window).on('load', function() {
        setTimeout(function(){
            jQuery('body').addClass('loaded');
        }, 1500);
    });  

    /* ---------------------------------------------- /*
     * Home section height
     /* ---------------------------------------------- */

      jQuery(".search-icon").click(function(e){
            e.preventDefault();
            //console.log();
            jQuery('.header-logo .search-box-outer ul.dropdown-menu').toggle('addSerchBox');
         });
    function buildHomeSection(homeSection) {
        if (homeSection.length > 0) {
            if (homeSection.hasClass('home-full-height')) {
                homeSection.height(jQuery(window).height());
            } else {
                homeSection.height(jQuery(window).height() * 0.85);
            }
        }
    }


    /* ---------------------------------------------- /*
     * Scroll top
     /* ---------------------------------------------- */

    jQuery(window).scroll(function () {
        if (jQuery(this).scrollTop() > 100) {
            jQuery('.scroll-up').fadeIn();
        } else {
            jQuery('.scroll-up').fadeOut();
        }
    });

    jQuery('a[href="#totop"]').click(function () {
        jQuery('html, body').animate({scrollTop: 0}, 'slow');
        return false;
    });

    // Tooltip Js
    jQuery(function () {
        jQuery('[data-toggle="tooltip"]').tooltip()
    });

    // Accodian Js
    function toggleIcon(e) {
        jQuery(e.target)
                .prev('.panel-heading')
                .find(".more-less")
                .toggleClass('fa-plus-square-o fa-minus-square-o');
    }
    jQuery('.panel-group').on('hidden.bs.collapse', toggleIcon);
    jQuery('.panel-group').on('shown.bs.collapse', toggleIcon);

    jQuery('.grid').masonry({
        itemSelector: '.grid-item',
        transitionDuration: '0.2s',
        horizontalOrder: true,
    });
});

		