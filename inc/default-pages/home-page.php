<?php
	//post status and options
	$spice_software_plus_post = array(
		  'comment_status' => 'closed',
		  'ping_status' =>  'closed' ,
		  'post_author' => 1,
		  'post_date' => date('Y-m-d H:i:s'),
		  'post_name' => 'Home',
		  'post_status' => 'publish' ,
		  'post_title' => 'Home',
		  'post_type' => 'page',
	);  
	//insert page and save the id
	$spice_software_plus_newvalue = wp_insert_post( $spice_software_plus_post, false );
	if ( $spice_software_plus_newvalue && ! is_wp_error( $spice_software_plus_newvalue ) ){
		update_post_meta( $spice_software_plus_newvalue, '_wp_page_template', 'template-business.php' );
		
		// Use a static front page
		$spice_software_plus_page = get_page_by_title('Home');
		update_option( 'show_on_front', 'page' );
		update_option( 'page_on_front', $spice_software_plus_page->ID );
		
	}
?>