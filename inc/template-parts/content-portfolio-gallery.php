<?php
$post_type = 'software_portfolio';
$tax = 'portfolio_categories';
$term_args = array('hide_empty' => true, 'orderby' => 'id');
$posts_per_page = get_theme_mod('portfolio_numbers_options', 4);
$tax_terms = get_terms($tax, $term_args);
$defualt_tex_id = get_option('spice_software_default_term_id');
$j = 1;$tab='';
if(isset($_GET['tab'])):
    $tab = $_GET['tab'];

endif;
if (isset($_GET['div'])) {
    $tab = $_GET['div'];
}
$porfolio_page_title = get_theme_mod('porfolio_page_title', __('Our Portfolio', 'spice-software-plus'));
$porfolio_page_subtitle = get_theme_mod('porfolio_page_subtitle', __('Our Recent Works', 'spice-software-plus'));
?>
<section class="section-space portfolio portfolio-page <?php if (is_page_template('template-portfolio-2-col-gallery.php') || is_page_template('template-portfolio-3-col-gallery.php') || is_page_template('template-portfolio-4-col-gallery.php')) { ?>portfolio-gallery<?php } ?>">
    <div class="container<?php echo esc_html(spice_software_container());?>">
        <?php if (!empty($porfolio_page_title) || !empty($porfolio_page_subtitle)): ?>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="section-header">
                        <?php if (!empty($porfolio_page_title)): ?><h2 class="section-title"><?php echo $porfolio_page_title; ?></h2>
                        <?php endif; ?>
                        <?php if (!empty($porfolio_page_subtitle)): ?><h5 class="section-subtitle"><?php echo $porfolio_page_subtitle; ?></h5><?php endif; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <!--Tab panels-->
        <div class="tab-content pt-0">

            <!--Panel 1-->
            <div class="tab-pane fade show in active" id="panel31" role="tabpanel">
                <?php
                global $paged;
                $curpage = $paged ? $paged : 1;

                $args = array(
                    'post_type' => $post_type,
                    'post_status' => 'publish',                                     
                    'posts_per_page' => $posts_per_page,
                    'paged' => $curpage,
                    'orderby' => 'DESC',
                );
                $portfolio_query = null;
                $portfolio_query = new WP_Query($args);
                if ($portfolio_query->have_posts()):
                    ?>
                    <div class="row">
                        <?php
                        
                        while ($portfolio_query->have_posts()) : $portfolio_query->the_post();
                            $portfolio_target = sanitize_text_field(get_post_meta(get_the_ID(), 'portfolio_target', true));
                            $portfolio_description = get_the_content();
                            if (get_post_meta(get_the_ID(), 'portfolio_link', true)) {
                                $portfolio_link = get_post_meta(get_the_ID(), 'portfolio_link', true);
                            } else {
                                $portfolio_link = '';
                            }
                            $class = '';
                            if (is_page_template('template-portfolio-2-col-gallery.php')) {
                                $class = 'col-md-6 col-sm-6 col-xs-12 portfolio-gallery';
                            }
                            if (is_page_template('template-portfolio-3-col-gallery.php')) {
                                $class = 'col-md-4 col-sm-4 col-xs-12 portfolio-gallery';
                            }
                            if (is_page_template('template-portfolio-4-col-gallery.php')) {
                                $class = 'col-md-3 col-sm-3 col-xs-12 portfolio-gallery';
                            }
                            if (is_page_template('template-portfolio-2-col-non-filter.php')) {
                                $class = 'col-md-6 col-sm-6 col-xs-12';
                            }
                            if (is_page_template('template-portfolio-3-col-non-filter.php')) {
                                $class = 'col-md-4 col-sm-4 col-xs-12';
                            }
                            if (is_page_template('template-portfolio-4-col-non-filter.php')) {
                                $class = 'col-md-3 col-sm-3 col-xs-12';
                            }
                            echo '<div class="' . $class . '">';
                            ?>
                            <figure class="portfolio-thumbnail">
                                <?php
                                the_post_thumbnail('full', array(
                                    'class' => 'card-img-top img-fluid',
                                    'alt' => get_the_title(),
                                ));
                                if (has_post_thumbnail()) {
                                    $post_thumbnail_id = get_post_thumbnail_id();
                                    $post_thumbnail_url = wp_get_attachment_url($post_thumbnail_id);
                                }

                                if (!empty($portfolio_link)) {
                                    $tagt = (!empty($portfolio_target)) ? 'target="_blank"' : '';

                                    $portlink = '<a href=' . "$portfolio_link" . ' title=' . get_the_title() . ' ' . $tagt . ' >' . get_the_title() . '</a>';
                                } else {
                                    $tagt = (!empty($portfolio_target)) ? 'target="_blank"' : '';
                                    $portlink ='<a href="#" title=' . get_the_title() . ' ' . $tagt . ' >' . get_the_title() . '</a>';
                                }
                                foreach ($tax_terms as $tax_term) {
                                $modelId = get_the_ID() . '_model' . rawurldecode($tax_term->slug);
                                }?>
                                <figcaption>
                                <?php if(!empty($portlink)):?>
                                    <div class="entry-header">
                                        <h4 class="entry-title">
                                            <?php echo $portlink; ?>
                                        </h4>
                                    </div>
                                <?php endif;
                                $tax_string=implode(" ",get_the_taxonomies());
                                $tax_cat=str_replace( array( 'Categories:'), ' ', $tax_string);?>
                                <p class="taxonomy-list"><?php  echo $tax_cat;?></p>
                                </figcaption>     
                                <a data-toggle="modal" data-target="#<?php echo $modelId; ?>"><i>+</i></a>

                            </figure>   
                            <?php echo '</div>'; ?>
                            <div class="modal fade" id="<?php echo $modelId; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-body p-0">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>

                                            <!-- Grid row -->
                                            <div class="row">
                                                <?php
                                                if ($post_thumbnail_url) {
                                                    if (!$portfolio_description) {
                                                        $ImgGridColumn = 'col-md-12';
                                                    } else {
                                                        $ImgGridColumn = 'col-md-6';
                                                    }
                                                    if ($portfolio_description) {
                                                        ?>
                                                        <!-- Grid column -->
                                                        <div class="col-md-6 py-5 pl-5">

                                                            <article class="post text-center">
                                                                <div class="entry-header">
                                                                    <h2 class="entry-title">                                                                
                                                                        <?php echo $portlink; ?>
                                                                    </h2>

                                                                </div>
                                                                <div class="entry-content">
                                                                    <p><?php the_content(); ?></p>
                                                                </div>
                                                            </article>

                                                        </div>
                                                        <!-- Grid column -->
                                                    <?php } ?>
                                                    <!-- Grid column -->
                                                    <div class="<?php echo $ImgGridColumn; ?>">

                                                        <div class="view rounded-right">
                                                            <img class="img-fluid" src="<?php echo $post_thumbnail_url; ?>" alt="Sample image">
                                                        </div>

                                                    </div>
                                                    <!-- Grid column -->
                                                <?php } ?>
                                            </div>
                                            <!-- Grid row -->

                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endwhile; ?>
                    </div>
                    <?php
                    $total = $portfolio_query->found_posts;
                    $Webriti_pagination = new Spicethemes_pagination();
                    $Webriti_pagination->Spicethemes_page($curpage, $portfolio_query, $total, $posts_per_page);
                    wp_reset_query();
                endif;
                ?>
            </div>
        </div>	
    </div>		
</section>
<?php
get_footer();
