<article id="post-<?php the_ID(); ?>" class="post">
	<div class="post-content">
		<header class="entry-header">
			<h3 class="entry-title"><?php esc_html_e('Nothing found', 'spice-software-plus'); ?></a></h3>
			<p><?php esc_html_e('Sorry, but nothing matched your search criteria. Please try again with some different keywords.','spice-software-plus'); ?></p>
		</header>
	</div>
</article>