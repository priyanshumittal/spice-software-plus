jQuery(document).ready(function() {

	/* Tabs in welcome page */
		function spice_software_plus_welcome_page_tabs(event) {
			jQuery(event).parent().addClass("active");
		   jQuery(event).parent().siblings().removeClass("active");
		   var tab = jQuery(event).attr("href");
		   jQuery(".spice-software-plus-tab-pane").not(tab).css("display", "none");
		   jQuery(tab).fadeIn();
		}
    
        jQuery(".spice-software-plus-nav-tabs li").slice(0,1).addClass("active");

	    jQuery(".spice-software-plus-nav-tabs a").click(function(event) {
		   event.preventDefault();
			spice_software_plus_welcome_page_tabs(this);
	    });
	   
		/* Tab Content height matches admin menu height for scrolling purpouses */
		$tab = jQuery('.spice-software-plus-tab-content > div');
		$admin_menu_height = jQuery('#adminmenu').height();
		if( (typeof $tab !== 'undefined') && (typeof $admin_menu_height !== 'undefined') )
		{
		    $newheight = $admin_menu_height - 180;
		    $tab.css('min-height',$newheight);
		}
	
		jQuery(".spice-software-plus-custom-class").click(function(event){
		   event.preventDefault();
		   jQuery('.spice-software-plus-nav-tabs li a[href="#changelog"]').click();
		});

		jQuery(".spice-software-plus-custom-class").click(function(event){
       event.preventDefault();
       jQuery('.spice-software-plus-nav-tabs li a[href="#one_click_demo"]').click();
    });

});