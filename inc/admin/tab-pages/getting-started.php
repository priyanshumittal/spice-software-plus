<?php
/**
 * Getting started template
 */
?>
<div id="getting_started" class="spice-software-plus-tab-pane active">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<h1 class="spice-software-plus-info-title text-center"><?php echo esc_html__('Spice Software Plus Configuration','spice-software-plus'); ?><?php if( !empty($spice_software['Version']) ): ?> <sup id="spice-software-plus-theme-version"><?php echo esc_html( $spice_software['Version'] ); ?> </sup><?php endif; ?></h1>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">			
			    <div class="spice-software-plus-page">
			    	<div class="mockup">
			    		<img src="<?php echo SPICE_SOFTWAREP_PLUGIN_URL.'/inc/admin/assets/img/mockup-pro.png';?>"  width="100%">
			    	</div>
				
				</div>	
			</div>
		</div>

		<div class="row" style="margin-top: 20px;">

			<div class="col-md-6">
				<div class="spice-software-plus-page">
					<div class="spice-software-plus-page-top"><?php esc_html_e( 'Links to Customizer Settings', 'spice-software-plus' ); ?></div>
					<div class="spice-software-plus-page-content">
						<ul class="spice-software-plus-page-list-flex">
							<li class="">
								<a class="spice-software-plus-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[section]=title_tagline' ) ); ?>" target="_blank"><?php esc_html_e('Site Logo','spice-software-plus'); ?></a>
							</li>
							<li class="">
								<a class="spice-software-plus-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[panel]=spice_software_theme_panel' ) ); ?>" target="_blank"><?php esc_html_e('Blog options','spice-software-plus'); ?></a>
							</li>
							 <li class="">
								<a class="spice-software-plus-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[panel]=nav_menus' ) ); ?>" target="_blank"><?php esc_html_e('Menus','spice-software-plus'); ?></a>
							</li>
							 <li class="">
								<a class="spice-software-plus-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[section]=theme_style' ) ); ?>" target="_blank"><?php esc_html_e('Layout & Color scheme','spice-software-plus'); ?></a>
							</li>
							 <li class="">
								<a class="spice-software-plus-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[panel]=widgets' ) ); ?>" target="_blank"><?php esc_html_e('Widgets','spice-software-plus'); ?></a>
							</li>
							 <li class="">
								<a class="spice-software-plus-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[panel]=spice_software_general_settings' ) ); ?>" target="_blank"><?php esc_html_e('General Settings','spice-software-plus'); ?></a><?php esc_html_e(" ( Preloader, After Menu, Header Presets, Sticky Header, Container settings, Post Navigation Styles, Scroll to Top settings )","spice-software-plus"); ?>
							</li>
                                                        <li class="">
								<a class="spice-software-plus-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[panel]=section_settings' ) ); ?>" target="_blank"><?php esc_html_e('Homepage sections','spice-software-plus'); ?></a>
							</li>
                                                        <li class="">
								<a class="spice-software-plus-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[panel]=spice_software_template_settings' ) ); ?>" target="_blank"><?php esc_html_e('Page template settings','spice-software-plus'); ?></a>
							</li>
			
							<li class="">
								<a class="spice-software-plus-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[panel]=spice_software_typography_setting' ) ); ?>" target="_blank"><?php esc_html_e('Typography','spice-software-plus'); ?></a>
							</li>
							<li class="">
								<a class="spice-software-plus-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'themes.php?page=Spice_Software_Plus_Hooks_Settings' ) ); ?>" target="_blank"><?php esc_html_e('Hooks','spice-software-plus'); ?></a>

							</li>
							
							<li class="">
								<a class="spice-software-plus-page-quick-setting-link" href="<?php echo esc_url( admin_url( 'customize.php?autofocus[section]=frontpage_layout' ) ); ?>" target="_blank"><?php esc_html_e('Sections order manager','spice-software-plus'); ?></a>
							</li>
						</ul>
					</div>
				</div>
			</div>
			
			<div class="col-md-6">				

				<div class="spice-software-plus-page">

					<div class="spice-software-plus-page-top spice-software-plus-demo-import"><?php esc_html_e( 'One Click Demo Import', 'spice-software-plus' ); ?></div>
					<p style="padding:10px 20px; font-size: 16px;"><?php _e( 'To import the demo data, you need to activate the <b>One Click Demo Import</b> and <b>Spice Software Demo Importer</b> plugins. <a class="spice-software-plus-custom-class" href="#one_click_demo" target="_self">Click Here</a>', 'spice-software-plus' ); ?></p>
				</div>
				<div class="spice-software-plus-page">
					<div class="spice-software-plus-page-top"><?php esc_html_e( 'Useful Links', 'spice-software-plus' ); ?></div>

					<div class="spice-software-plus-page-content">
						<ul class="spice-software-plus-page-list-flex">
							<li class="">
								<a class="spice-software-plus-page-quick-setting-link" href="<?php echo 'https://spice-software-pro.spicethemes.com/'; ?>" target="_blank"><?php echo esc_html__('Spice Software Plus Demo','spice-software-plus'); ?></a>
							</li>

							<li class="">
								<a class="spice-software-plus-page-quick-setting-link" href="<?php echo 'https://spicethemes.com/spice-software-plus'; ?>" target="_blank"><?php echo esc_html__('Spice Software Plus Details','spice-software-plus'); ?></a>
							</li>
							
							<li class="">
								<a class="spice-software-plus-page-quick-setting-link" href="<?php echo 'https://support.spicethemes.com/index.php?p=/categories/spice-software-plus'; ?>" target="_blank"><?php echo esc_html__('Spice Software Plus Support','spice-software-plus'); ?></a>
							</li>
							
						    <li class=""> 
								<a class="spice-software-plus-page-quick-setting-link" href="<?php echo 'https://wordpress.org/support/theme/spice-software/reviews/#new-post'; ?>" target="_blank"><?php echo esc_html__('Your feedback is valuable to us','spice-software-plus'); ?></a>
							</li>
							
							<li class=""> 
								<a class="spice-software-plus-page-quick-setting-link" href="<?php echo 'https://helpdoc.spicethemes.com/category/spice-software-plus/'; ?>" target="_blank"><?php echo esc_html__('Spice Software Plus Documentation','spice-software-plus'); ?></a>
							</li>
							
						    <li class=""> 
								<a class="spice-software-plus-page-quick-setting-link" href="<?php echo 'https://spicethemes.com/contact'; ?>" target="_blank"><?php echo esc_html__('Pre-sales enquiry','spice-software-plus'); ?></a>
							</li> 
							<li> 
								<a class="spice-software-plus-page-quick-setting-link" href="<?php echo esc_url('https://spicethemes.com/spice-software-free-vs-plus/'); ?>" target="_blank"><?php echo esc_html__('Free vs Plus','spice-software-plus'); ?></a>
							</li> 

							<li> 
								<a class="spice-software-plus-page-quick-setting-link" href="<?php echo esc_url('https://spicethemes.com/spice-software-plus-changelog/'); ?>" target="_blank"><?php echo esc_html__('Changelog','spice-software-plus'); ?></a>
							</li> 
						</ul>
					</div>
				</div>			    
			</div>	

		</div>
	</div>
</div>	