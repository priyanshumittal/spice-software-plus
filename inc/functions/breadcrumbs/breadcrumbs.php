<?php

// theme sub header breadcrumb functions
if (!function_exists('spice_software_breadcrumbs')):

    function spice_software_breadcrumbs() {
        $classes = get_body_class();
        global $post;
        $homeLink = home_url();
        $hide_show_banner = get_theme_mod('banner_enable',true);
        if($hide_show_banner == true)
        {
            if (is_404()){?>
                <section class="page-title-section" <?php if ( !empty(get_theme_mod('error_background_img')) ){ ?> style="background:#17212c url('<?php echo get_theme_mod('error_background_img');?>')"  <?php } ?>>   
            <?php
            }
            elseif (is_search()){?>
                <section class="page-title-section s" <?php if ( !empty(get_theme_mod('search_background_img')) ){ ?> style="background:#17212c url('<?php echo get_theme_mod('search_background_img');?>')"  <?php } ?>>           
                <?php
            }
            elseif (in_array('date',$classes)) {?>
                <section class="page-title-section" <?php if ( !empty(get_theme_mod('date_background_img')) ){ ?> style="background:#17212c url('<?php echo get_theme_mod('date_background_img');?>')"  <?php } ?>>     
            <?php 
            }
            elseif (in_array('author',$classes)) {?>
                <section class="page-title-section" <?php if ( !empty(get_theme_mod('author_background_img')) ){ ?> style="background:#17212c url('<?php echo get_theme_mod('author_background_img');?>')"  <?php } ?>>     
            <?php 
            }
            else{?>
                <section class="page-title-section" <?php if( get_header_image() ){ ?> style="background:#17212c url('<?php header_image(); ?>')"  <?php } ?>>      
                <?php
            }
            if(get_theme_mod('breadcrumb_image_overlay',true)==true){
                $breadcrumb_overlay=get_theme_mod('breadcrumb_overlay_section_color','rgba(0,0,0,0.6)');
            }else{
                $breadcrumb_overlay= 'none';
            }
            ?>
            <style type="text/css">
                .page-title-section .overlay
                {

                    background-color: <?php echo $breadcrumb_overlay;?>;
                }
            </style>
                <?php
                if(get_theme_mod('breadcrumb_image_overlay',true)==true):?>
                    <div class="overlay"></div>     
                <?php endif;?>  
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <?php
                                include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
                                if (is_home() || is_front_page()) {
                                    if (get_option('show_on_front') == 'posts') {
                                        ?>
                                        <div class="page-title">
                                            <h1><?php esc_html_e('Home', 'spice-software-plus') ?></h1>
                                        </div> 
                                        <?php
                                    } else {
                                        ?>
                                        <div class="page-title">
                                            <h1><?php echo get_the_title(get_option('page_for_posts', true)); ?> </h1>
                                        </div> 
                                        <?php
                                    }
                                } else {
                                    ?>                   
                                    <div class="page-title text-center text-white">
                                        <?php
                                        if (is_search()) {
                                            echo '<h1>' . get_search_query() . '</h1>';
                                        } else if (is_404()) {
                                            echo '<h1>' . esc_html__('Error 404', 'spice-software-plus') . '</h1>';
                                        } else if (is_category()) {
                                            echo '<h1>' . esc_html__('Category : ' . single_cat_title('', false), 'spice-software-plus') . '</h1>';
                                        } else if (!is_single() && !is_page() && get_post_type() != 'post' && !is_404()) {
                                            if (class_exists('WooCommerce')) {
                                                if (is_shop()) {
                                                    ?>
                                                    <h1><?php woocommerce_page_title(); ?></h1>
                                                    <?php
                                                }
                                            }
                                        } elseif (is_tag()) {
                                            echo '<h1>' . esc_html__('Tag : ' . single_tag_title('', false), 'spice-software-plus') . '</h1>';
                                        } else if (is_archive()) {
                                            the_archive_title('<h1>', '</h1>');
                                        } else {
                                            ?>
                                            <h1><?php the_title(''); ?></h1>
                                            <?php
                                        }
                                        global $template;
                                        if (basename($template) == "taxonomy-portfolio_categories.php") {
                                            the_archive_title('<h1 >', '</h1>');
                                        }
                                        ?>
                                    </div>  
                                    <?php
                                }
                                $breadcrumb_enable = get_theme_mod('breadcrumb_setting_enable', true);
								if ($breadcrumb_enable == true) {
								$spice_sofware_breadcrumb_type = get_theme_mod('spice_sofware_breadcrumb_type','yoast');
								if($spice_sofware_breadcrumb_type == 'yoast') {
								if ( function_exists('yoast_breadcrumb') ) {
								$wpseo_titles=get_option('wpseo_titles');
								if($wpseo_titles['breadcrumbs-enable']==true){

								echo '<ul class="page-breadcrumb text-center">';
								echo '<li>';
								echo '</li>';
								$breadcrumbs = yoast_breadcrumb("","",false);
								echo wp_kses_post($breadcrumbs);
								echo '</ul>';
								}	
								}
								}
								elseif($spice_sofware_breadcrumb_type == 'navxt')
								{
								if( function_exists( 'bcn_display' ) )
								{
								echo '<nav class="page-breadcrumb text-center navxt-breadcrumb">';
								bcn_display();
								echo '</nav>';
								}
								}
								elseif($spice_sofware_breadcrumb_type == 'rankmath')
								{
								if( function_exists( 'rank_math_the_breadcrumbs' ) )
								{
								rank_math_the_breadcrumbs();
								} 
								}
								}
                                ?>
                            </div>
                        </div>
                    </div>
            </section>
            <div class="page-seperate"></div>
        <?php } else { ?><div class="page-seperate"></div><?php
        }
    }

endif;
?>
