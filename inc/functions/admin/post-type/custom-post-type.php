<?php
/************* Home portfolio custom post type ************************/		
function spice_software_plus_project_type()
		{	register_post_type( 'software_portfolio',
				array(
				'labels' => array(
				'name' => __('Projects', 'spice-software-plus'),
				'add_new' => __('Add New', 'spice-software-plus'),
                'add_new_item' => __('Add New Project','spice-software-plus'),
				'edit_item' => __('Add New','spice-software-plus'),
				'new_item' => __('New Link','spice-software-plus'),
				'all_items' => __('All Projects','spice-software-plus'),
				'view_item' => __('View Link','spice-software-plus'),
				'search_items' => __('Search Links','spice-software-plus'),
				'not_found' =>  __('No Links found','spice-software-plus'),
				'not_found_in_trash' => __('No Links found in Trash','spice-software-plus'), 
				
			),
				'supports' => array('title','thumbnail','editor','excerpt'),
				'show_in_nav_menus' => false,
				'public' => true,
				'rewrite' => array('slug' => 'software_portfolio'),
				'menu_position' => 11,
				'public' => true,
				'menu_icon' => SPICE_SOFTWAREP_PLUGIN_URL . '/inc/images/portfolio.png',
			)
	);
}
add_action( 'init', 'spice_software_plus_project_type' );
?>