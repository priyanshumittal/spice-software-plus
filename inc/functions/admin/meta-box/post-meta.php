<?php
// define portfolio custom post type metabox
add_action('admin_init', 'spice_software_plus_init');

function spice_software_plus_init() {
    add_meta_box('home_project_meta', __('Project Details', 'spice-spice-software-plus'), 'spice_software_plus_meta_portfolio', 'software_portfolio', 'normal', 'high');

    add_action('save_post', 'spice_software_plus_meta_save');
}

// code for portfolio description
function spice_software_plus_meta_portfolio() {
    global $post;

    $portfolio_link = sanitize_text_field(get_post_meta(get_the_ID(), 'portfolio_link', true));
    //$portfolio_description = sanitize_text_field(get_post_meta(get_the_ID(), 'portfolio_description', true));
    $portfolio_target = sanitize_text_field(get_post_meta(get_the_ID(), 'portfolio_target', true));
    ?>	

    <p><h4 class="heading"><?php esc_attr_e('Link', 'spice-spice-software-plus'); ?></h4></p>
    <p><input style="width:600px;" name="portfolio_link" id="portfolio_link" placeholder="<?php esc_attr_e('Link', 'spice-spice-software-plus'); ?>" type="text" value="<?php if (!empty($portfolio_link)) echo esc_attr($portfolio_link); ?>"> </p>
    <p><input type="checkbox" id="portfolio_target" name="portfolio_target" <?php if ($portfolio_target) echo "checked"; ?> > <?php esc_attr_e('Open link in new tab', 'spice-spice-software-plus'); ?></p>
    <!-- <p><h4 class="heading"><?php //esc_attr_e('Description', 'spice-spice-software-plus'); ?></h4></p> -->
   <!--  <p><textarea class="widefat" rows="4" cols="4" name="portfolio_description"> <?php// if (!empty($portfolio_description)) //echo esc_html($portfolio_description); ?></textarea></p> -->
    <?php
}

function spice_software_plus_meta_save($post_id) {
    if ((defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) || (defined('DOING_AJAX') && DOING_AJAX) || isset($_REQUEST['bulk_edit']))
        return;

    if (!current_user_can('edit_page', $post_id)) {
        return;
    }

    if (isset($_POST['post_ID'])) {
        $post_ID = $_POST['post_ID'];
        $post_type = get_post_type($post_ID);

        if ($post_type == 'software_portfolio') {
            update_post_meta($post_ID, 'portfolio_link', sanitize_text_field($_POST['portfolio_link']));
            //update_post_meta($post_ID, 'portfolio_description', sanitize_text_field($_POST['portfolio_description']));
            update_post_meta($post_ID, 'portfolio_target', sanitize_text_field($_POST['portfolio_target']));
        }
    }
}
?>