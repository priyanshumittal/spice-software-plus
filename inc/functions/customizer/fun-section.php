<?php
/* funfact section */
$wp_customize->add_setting('funfact_section_enabled', array(
    'default' => true,
    'sanitize_callback' => 'spice_software_sanitize_checkbox'
));

$wp_customize->add_control(new Spice_Software_Toggle_Control($wp_customize, 'funfact_section_enabled',
                array(
            'label' => __('Enable Funfact on homepage', 'spice-software-plus'),
            'type' => 'toggle',
            'section' => 'funfact_section',
                )
));

$wp_customize->add_section('funfact_section', array(
    'title' => __('Funfact Settings', 'spice-software-plus'),
    'panel' => 'section_settings',
    'priority' => 19,
));

if (class_exists('Spice_Software_Plus_Repeater')) {
    $wp_customize->add_setting('spice_software_funfact_content', array());

    $wp_customize->add_control(new Spice_Software_Plus_Repeater($wp_customize, 'spice_software_funfact_content', array(
                'label' => esc_html__('Funfact content', 'spice-software-plus'),
                'section' => 'funfact_section',
                'priority' => 10,
                'add_field_label' => esc_html__('Add new Funfact', 'spice-software-plus'),
                'item_name' => esc_html__('Funfact', 'spice-software-plus'),
                'customizer_repeater_icon_control' => true,
                'customizer_repeater_title_control' => true,
                'customizer_repeater_text_control' => true,
                'active_callback' => 'spice_software_plus_funfact_callback'
    )));
}

/**
 * Add selective refresh for Front page funfact section controls.
 */
$wp_customize->selective_refresh->add_partial('home_fun_section_title', array(
    'selector' => '.funfact .title',
    'settings' => 'home_fun_section_title',
    'render_callback' => 'spice_software_plus_home_fun_section_title_render_callback'
));

function spice_software_plus_home_fun_section_title_render_callback() {
    return get_theme_mod('home_fun_section_title');
}?>