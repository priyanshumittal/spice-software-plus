<?php
$wp_customize->add_section('contact_section', array(
    'title' => esc_html__('Contact Settings', 'spice-software-plus'),
    'panel' => 'section_settings',
//    'priority' => 2,
));


//Contact Section
$wp_customize->add_setting('home_contact_section_enabled', array(
    'default' => true,
    'sanitize_callback' => 'spice_software_sanitize_checkbox'
));

$wp_customize->add_control(new Spice_Software_Toggle_Control($wp_customize, 'home_contact_section_enabled',
                array(
            'label' => esc_html__('Enable Contact on homepage', 'spice-software-plus'),
            'type' => 'toggle',
            'section' => 'contact_section',
                )
));

//Contact section title
$wp_customize->add_setting('home_contact_section_title', array(
    'capability' => 'edit_theme_options',
    'default' => esc_html__('Stay in touch with us', 'spice-software-plus'),
    'sanitize_callback' => 'spice_software_home_page_sanitize_text',
    'transport' => $selective_refresh,
));

$wp_customize->add_control('home_contact_section_title', array(
    'label' => esc_html__('Title', 'spice-software-plus'),
    'section' => 'contact_section',
    'type' => 'text',
    'active_callback' => 'spice_software_plus_contact_callback'
));
function spice_software_plus_contact_bg_callback($control) {
        if ( $control->manager->get_setting('spice_software_color_skin')->value() == 'light') {
            return false;
        } else {
            return true;
        }
    }
$wp_customize->add_setting(
        'contact_bg_color_enable',
        array('capability'  => 'edit_theme_options',
        'default' => false,
        ));

    $wp_customize->add_control(
        'contact_bg_color_enable',
        array(
            'type' => 'checkbox',
            'label' => __('Click here to apply this setting','spice-software-plus'),
            'section' => 'contact_section',
            'active_callback' => function($control) {
                return (
                        spice_software_plus_contact_bg_callback($control) 
                        );
            },
        )
    );

//Background Color
$theme = wp_get_theme();
if('Spice Software Dark' == $theme->name) {
    $ss_contact_bg_color='#000000';
}
else{
    $ss_contact_bg_color='#ffffff';
}
$wp_customize->add_setting('contact_bg_color', array(
    'sanitize_callback' => 'sanitize_text_field',
    'default' => $ss_contact_bg_color,
));

$wp_customize->add_control(new Spice_Software_Plus_Customize_Alpha_Color_Control($wp_customize, 'contact_bg_color', array(
            'label' => esc_html__('Background Color', 'spice-software-plus'),
            'palette' => true,
            'active_callback' => 'spice_software_plus_contact_callback',
            'section' => 'contact_section')
));


if (class_exists('Spice_Software_Plus_Repeater')) {
    $wp_customize->add_setting('spice_software_plus_contact_detail_content', array());
    $wp_customize->add_control(new Spice_Software_Plus_Repeater($wp_customize, 'spice_software_plus_contact_detail_content', array(
                'label' => esc_html__('Contact content', 'spice-software-plus'),
                'section' => 'contact_section',
                'priority' => 10,
                'add_field_label' => esc_html__('Add new Contact', 'spice-software-plus'),
                'item_name' => esc_html__('Contact', 'spice-software-plus'),
                'customizer_repeater_icon_control' => true,
                'customizer_repeater_title_control' => true,
                'customizer_repeater_text_control' => true,
                'customizer_repeater_image_control' => true,
                'active_callback' => 'spice_software_plus_contact_callback'
    )));
}

$wp_customize->selective_refresh->add_partial('home_contact_section_title', array(
    'selector' => '.section-space.contact-detail h2.section-title',
    'settings' => 'home_contact_section_title',
    'render_callback' => 'home_contact_section_title_render_callback'
));

function home_contact_section_title_render_callback() {
    return get_theme_mod('home_contact_section_title');
}