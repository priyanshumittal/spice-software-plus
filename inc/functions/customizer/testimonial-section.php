<?php
$theme = wp_get_theme();
if('Spice Software Dark' == $theme->name) {
    $ss_testimonial_design=4;
}
else{
    $ss_testimonial_design=1;
}
/* Testimonial Section */
$wp_customize->add_section('testimonial_section', array(
    'title' => __('Testimonials Settings', 'spice-software-plus'),
    'panel' => 'section_settings',
    'priority' => 17,
));

// Enable testimonial section
$wp_customize->add_setting('testimonial_section_enable', array(
    'default' => true,
    'sanitize_callback' => 'spice_software_sanitize_checkbox'
));

$wp_customize->add_control(new Spice_Software_Toggle_Control($wp_customize, 'testimonial_section_enable',
                array(
            'label' => __('Enable Home Testimonial section', 'spice-software-plus'),
            'type' => 'toggle',
            'section' => 'testimonial_section',
                )
));

// testimonial section title
$wp_customize->add_setting('home_testimonial_section_title', array(
    'capability' => 'edit_theme_options',
    'default' => __('Our Clients Says', 'spice-software-plus'),
    'sanitize_callback' => 'softwarep_home_page_sanitize_text',
    'transport' => $selective_refresh,
));
$wp_customize->add_control('home_testimonial_section_title', array(
    'label' => __('Title', 'spice-software-plus'),
    'section' => 'testimonial_section',
    'type' => 'text',
    'active_callback' => 'spice_software_plus_testimonial_callback'
));

//testimonials & partners section discription
$wp_customize->add_setting('home_testimonial_section_discription', array(
    'default' => __('industry is the same think. Lorem Ipsum is simply printing and typesetting industry is the same think.', 'spice-software-plus'),
    'transport' => $selective_refresh,
));
$wp_customize->add_control('home_testimonial_section_discription', array(
    'label' => __('Sub Title', 'spice-software-plus'),
    'section' => 'testimonial_section',
    'type' => 'text',
    'active_callback' => 'spice_software_plus_testimonial_callback'
));

//Style Design
$wp_customize->add_setting('home_testimonial_design_layout', array('default' => $ss_testimonial_design));
$wp_customize->add_control('home_testimonial_design_layout',
        array(
            'label' => __('Design Style', 'spice-software-plus'),
            'active_callback' => 'spice_software_plus_testimonial_callback',
            'section' => 'testimonial_section',
            'type' => 'select',
            'choices' => array(
                1 => __('Design 1', 'spice-software-plus'),
                2 => __('Design 2', 'spice-software-plus'),
                3 => __('Design 3', 'spice-software-plus'),
                4 => __('Design 4', 'spice-software-plus')
            )
));

//Slide Item
$wp_customize->add_setting('home_testimonial_slide_item', array('default' => 1));
$wp_customize->add_control('home_testimonial_slide_item',
        array(
            'label' => __('Slide Item', 'spice-software-plus'),
            'active_callback' => 'spice_software_plus_testimonial_callback',
            'section' => 'testimonial_section',
            'type' => 'select',
            'choices' => array(
                1 => __('One', 'spice-software-plus'),
                2 => __('Two', 'spice-software-plus'),
                3 => __('Three', 'spice-software-plus'),
            )
));

if (class_exists('Spice_Software_Plus_Repeater')) {
    $wp_customize->add_setting('spice_software_testimonial_content', array(
    ));

    $wp_customize->add_control(new Spice_Software_Plus_Repeater($wp_customize, 'spice_software_testimonial_content', array(
                'label' => esc_html__('Testimonial content', 'spice-software-plus'),
                'section' => 'testimonial_section',
                'add_field_label' => esc_html__('Add new Testimonial', 'spice-software-plus'),
                'item_name' => esc_html__('Testimonial', 'spice-software-plus'),
//                'customizer_repeater_title_control' => true,
                'customizer_repeater_text_control' => true,
                'customizer_repeater_link_control' => true,
                'customizer_repeater_checkbox_control' => true,
                'customizer_repeater_image_control' => true,
                'customizer_repeater_user_name_control' => true,
                'customizer_repeater_designation_control' => true,
                'customizer_repeater_star_rating_control' => true,
                'active_callback' => 'spice_software_plus_testimonial_callback'
    )));
}

//Testimonial Background Image
$wp_customize->add_setting('testimonial_callout_background', array(
    'default' => SPICE_SOFTWAREP_PLUGIN_URL.'/inc/images/bg/bg-img.jpg',
    'sanitize_callback' => 'esc_url_raw',
));

$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'testimonial_callout_background', array(
            'label' => __('Background Image', 'spice-software-plus'),
            'section' => 'testimonial_section',
            'settings' => 'testimonial_callout_background',
            'active_callback' => function($control) {
        return (
                spice_software_plus_testimonial_design_layout_callback($control) &&
                spice_software_plus_testimonial_callback($control)
                );
    },
        )));

// Image overlay
$wp_customize->add_setting('testimonial_image_overlay', array(
    'default' => true,
    'sanitize_callback' => 'sanitize_text_field',
));

$wp_customize->add_control('testimonial_image_overlay', array(
    'label' => __('Enable testimonial image overlay', 'spice-software-plus'),
    'section' => 'testimonial_section',
    'type' => 'checkbox',
    'active_callback' => function($control) {
        return (
                spice_software_plus_testimonial_design_layout_callback($control) &&
                spice_software_plus_testimonial_callback($control)
                );
    },
));


//Testimonial Background Overlay Color
$wp_customize->add_setting('testimonial_overlay_section_color', array(
    'sanitize_callback' => 'sanitize_text_field',
    'default' => 'rgba(1, 7, 12, 0.8)',
));

$wp_customize->add_control(new Spice_Software_Plus_Customize_Alpha_Color_Control($wp_customize, 'testimonial_overlay_section_color', array(
            'label' => __('Testimonial image overlay color', 'spice-software-plus'),
            'palette' => true,
//            'active_callback' => 'spice_software_testimonial_callback',
            'active_callback' => function($control) {
                return (
                        spice_software_plus_testimonial_design_layout_callback($control) &&
                        spice_software_plus_testimonial_callback($control)
                        );
            },
            'section' => 'testimonial_section')
));

//Navigation Type
$wp_customize->add_setting('testimonial_nav_style', array('default' => 'bullets'));
$wp_customize->add_control('testimonial_nav_style', array(
    'label' => __('Navigation Style', 'spice-software-plus'),
    'section' => 'testimonial_section',
    'type' => 'radio',
    'priority' => 17,
    'choices' => array(
        'bullets' => __('Bullets', 'spice-software-plus'),
        'navigation' => __('Navigation', 'spice-software-plus'),
        'both' => __('Both', 'spice-software-plus'),
    ),
    'active_callback' => 'spice_software_plus_testimonial_callback'
));

// animation speed
$wp_customize->add_setting('testimonial_animation_speed', array('default' => 3000));
$wp_customize->add_control('testimonial_animation_speed',
        array(
            'label' => __('Animation speed', 'spice-software-plus'),
            'section' => 'testimonial_section',
            'type' => 'select',
            'choices' => array(
                '2000' => '2.0',
                '3000' => '3.0',
                '4000' => '4.0',
                '5000' => '5.0',
                '6000' => '6.0',
            ),
            'active_callback' => 'spice_software_plus_testimonial_callback'
));

// smooth speed
$wp_customize->add_setting('testimonial_smooth_speed', array('default' => 1000));
$wp_customize->add_control('testimonial_smooth_speed',
        array(
            'label' => __('Smooth speed', 'spice-software-plus'),
            'section' => 'testimonial_section',
            'type' => 'select',
            'choices' => array('500' => '0.5',
                '1000' => '1.0',
                '1500' => '1.5',
                '2000' => '2.0',
                '2500' => '2.5',
                '3000' => '3.0'),
            'active_callback' => 'spice_software_plus_testimonial_callback'
));

/**
 * Add selective refresh for Front page testimonial section controls.
 */
$wp_customize->selective_refresh->add_partial('home_testimonial_section_title', array(
    'selector' => '.section-space.testimonial .section-title',
    'settings' => 'home_testimonial_section_title',
    'render_callback' => 'spice_software_plus_home_testimonial_section_title_render_callback',
));
$wp_customize->selective_refresh->add_partial('home_testimonial_section_discription', array(
    'selector' => '.section-space.testimonial .section-header > p',
    'settings' => 'home_testimonial_section_discription',
    'render_callback' => 'spice_software_plus_home_testimonial_section_discription_render_callback',
));

function spice_software_plus_home_testimonial_section_title_render_callback() {
    return get_theme_mod('home_testimonial_section_title');
}

function spice_software_plus_home_testimonial_section_discription_render_callback() {
    return get_theme_mod('home_testimonial_section_discription');
}