<?php
function spice_software_plus_column_callback($control) {
    $column_value=$control->manager->get_setting('home_news_design_layout')->value();
    if ( $column_value == 2 || $column_value == 4) {
        return false;
    }
    return true;
}
$theme = wp_get_theme();
if('Spice Software Dark' == $theme->name) {
    $ss_news_design=4;
}
else{
    $ss_news_design=1;
}
//Latest News Section
$wp_customize->add_section('spice_software_latest_news_section', array(
    'title' => __('Latest News Settings', 'spice-software-plus'),
    'panel' => 'section_settings',
    'priority' =>18,
));


// Enable news section
$wp_customize->add_setting('latest_news_section_enable', array(
    'default' => true,
    'sanitize_callback' => 'spice_software_plus_sanitize_checkbox'
));

$wp_customize->add_control(new Spice_Software_Toggle_Control($wp_customize, 'latest_news_section_enable',
                array(
            'label' => __('Enable Home News section', 'spice-software-plus'),
            'type' => 'toggle',
            'section' => 'spice_software_latest_news_section',
                )
));

// News section title
$wp_customize->add_setting('home_news_section_title', array(
    'capability' => 'edit_theme_options',
    'default' => __('Our Latest News', 'spice-software-plus'),
    'sanitize_callback' => 'spice_software_plus_home_page_sanitize_text',
    'transport' => $selective_refresh,
));
$wp_customize->add_control('home_news_section_title', array(
    'label' => __('Title', 'spice-software-plus'),
    'section' => 'spice_software_latest_news_section',
    'type' => 'text',
    'active_callback' => 'spice_software_plus_news_callback'
));

//News section subtitle
$wp_customize->add_setting('home_news_section_discription', array(
    'default' => __('From our blog', 'spice-software-plus'),
    'transport' => $selective_refresh,
));
$wp_customize->add_control('home_news_section_discription', array(
    'label' => __('Sub Title', 'spice-software-plus'),
    'section' => 'spice_software_latest_news_section',
    'type' => 'text',
    'active_callback' => 'spice_software_plus_news_callback'
));

$wp_customize->add_setting('home_news_design_layout', array('default' => $ss_news_design));
$wp_customize->add_control('home_news_design_layout',
        array(
            'label' => __('Design Style', 'spice-software-plus'),
            'section' => 'spice_software_latest_news_section',
            'active_callback' => 'spice_software_plus_news_callback',
            'type' => 'select',
            'choices' => array(
                1 => __('Grid Style', 'spice-software-plus'),
                2 => __('List Style', 'spice-software-plus'),
                3 => __('Masonry Style', 'spice-software-plus'),
                4 => __('Switcher Style', 'spice-software-plus'),
            )
));

/* * ****************** Blog Content ****************************** */

$wp_customize->add_setting('spice_software_homeblog_layout',
        array(
            'default' => 4,
            'sanitize_callback' => 'spice_software_sanitize_select'
        )
);

$wp_customize->add_control('spice_software_homeblog_layout',
        array(
            'label' => esc_html__('Column Layout', 'spice-software-plus'),
            'section' => 'spice_software_latest_news_section',
            'type' => 'select',
            'active_callback' => 'spice_software_column_callback',
            'active_callback' => function($control) {
                return (
                        spice_software_plus_news_callback($control) &&
                        spice_software_plus_column_callback($control)
                        );
            },
            'choices' => array(
                6 => esc_html__('2 Column', 'spice-software-plus'),
                4 => esc_html__('3 Column', 'spice-software-plus'),
                3 => esc_html__('4 Column', 'spice-software-plus'),
            )
        )
);

$wp_customize->add_setting('spice_software_homeblog_counts',
        array(
            'default' => 3,
            'capability' => 'edit_theme_options',
            'sanitize_callback' => 'spice_software_sanitize_number_range',
        )
);
$wp_customize->add_control('spice_software_homeblog_counts',
        array(
            'label' => esc_html__('No of Post', 'spice-software-plus'),
            'section' => 'spice_software_latest_news_section',
            'type' => 'number',
            'input_attrs' => array('min' => 2, 'max' => 20, 'step' => 1, 'style' => 'width: 100%;'),
            'active_callback' => 'spice_software_plus_news_callback'
        )
);

// Read More Button
$wp_customize->add_setting('home_news_button_title', array(
    'capability' => 'edit_theme_options',
    'default' => __('Read More', 'spice-software-plus'),
    'sanitize_callback' => 'softwarep_home_page_sanitize_text',
    'transport' => $selective_refresh,
));
$wp_customize->add_control('home_news_button_title', array(
    'label' => __('Read More Text', 'spice-software-plus'),
    'section' => 'spice_software_latest_news_section',
    'type' => 'text',
    'active_callback' => 'spice_software_plus_news_callback'
));

// enable/disable meta section 
$wp_customize->add_setting(
        'home_meta_section_settings',
        array('capability' => 'edit_theme_options',
            'default' => true,
));
$wp_customize->add_control(
        'home_meta_section_settings',
        array(
            'type' => 'checkbox',
            'label' => __('Enable post meta in blog section', 'spice-software-plus'),
            'section' => 'spice_software_latest_news_section',
            'active_callback' => 'spice_software_plus_news_callback'
        )
);

$wp_customize->add_setting(
        'home_blog_more_btn',
        array(
            'default' => __('View More', 'spice-software-plus'),
            'capability' => 'edit_theme_options',
            'sanitize_callback' => 'sanitize_text_field',
            'transport' => $selective_refresh,
        )
);

$wp_customize->add_control(
        'home_blog_more_btn',
        array(
            'label' => __('View More Button Text', 'spice-software-plus'),
            'section' => 'spice_software_latest_news_section',
            'type' => 'text',
            'active_callback' => 'spice_software_plus_news_callback'
));

$wp_customize->add_setting(
        'home_blog_more_btn_link',
        array(
            'default' => '#',
            'capability' => 'edit_theme_options',
            'sanitize_callback' => 'sanitize_text_field',
            'transport' => $selective_refresh,
));

$wp_customize->add_control(
        'home_blog_more_btn_link',
        array(
            'label' => __('View More Button Link', 'spice-software-plus'),
            'section' => 'spice_software_latest_news_section',
            'type' => 'text',
            'active_callback' => 'spice_software_plus_news_callback'
));

$wp_customize->add_setting(
        'home_blog_more_btn_link_target',
        array('sanitize_callback' => 'sanitize_text_field',
            'transport' => $selective_refresh,
));

$wp_customize->add_control(
        'home_blog_more_btn_link_target',
        array(
            'type' => 'checkbox',
            'label' => __('Open link in new tab', 'spice-software-plus'),
            'section' => 'spice_software_latest_news_section',
            'active_callback' => 'spice_software_plus_news_callback'
        )
);

/**
 * Add selective refresh for Front page news section controls.
 */
$wp_customize->selective_refresh->add_partial('home_news_section_title', array(
    'selector' => '.home-blog .section-header h2',
    'settings' => 'home_news_section_title',
    'render_callback' => 'spice_software_plus_home_news_section_title_render_callback',
));

$wp_customize->selective_refresh->add_partial('home_news_section_discription', array(
    'selector' => '.home-blog .section-header h5',
    'settings' => 'home_news_section_discription',
    'render_callback' => 'spice_software_plus_home_news_section_discription_render_callback',
));

$wp_customize->selective_refresh->add_partial('home_blog_more_btn', array(
    'selector' => '.home-blog .business-view-more-post',
    'settings' => 'home_blog_more_btn',
    'render_callback' => 'spice_software_plus_home_blog_more_btn_render_callback',
));

$wp_customize->selective_refresh->add_partial('home_news_button_title', array(
    'selector' => '.home-blog a.more-link',
    'settings' => 'home_news_button_title',
    'render_callback' => 'spice_software_plus_home_news_button_title_render_callback',
));

function spice_software_plus_home_news_section_title_render_callback() {
    return get_theme_mod('home_news_section_title');
}

function spice_software_plus_home_news_section_discription_render_callback() {
    return get_theme_mod('home_news_section_discription');
}

function spice_software_plus_home_blog_more_btn_render_callback() {
    return get_theme_mod('home_blog_more_btn');
}

function spice_software_plus_home_news_button_title_render_callback() {
    return get_theme_mod('home_news_button_title');
}