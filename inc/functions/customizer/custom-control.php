<?php
if (!function_exists('spice_software_plus_register_custom_controls')) :

    /**
     * Register Custom Controls
     */
    function spice_software_plus_register_custom_controls($wp_customize) {
        require_once SPICE_SOFTWAREP_PLUGIN_DIR . '/inc/inc/customizer/repeater/class-repeater-setting.php';
        require_once SPICE_SOFTWAREP_PLUGIN_DIR . '/inc/inc/customizer/repeater/class-control-repeater.php';
        require_once SPICE_SOFTWAREP_PLUGIN_DIR . '/inc/inc/customizer/sortable/class-sortable-control.php';
        require_once SPICE_SOFTWAREP_PLUGIN_DIR . '/inc/inc/customizer/slider/class-slider-control.php';
        require_once SPICE_SOFTWAREP_PLUGIN_DIR . '/inc/inc/customizer/slider/class-opacity-control.php';
        $wp_customize->register_control_type('Spice_Software_Plus_Control_Sortable');
        $wp_customize->register_control_type('Spice_Software_Plus_Slider_Control');
        $wp_customize->register_control_type( 'Spice_Software_Plus_Opacity_Control' );
    }

endif;
add_action('customize_register', 'spice_software_plus_register_custom_controls');