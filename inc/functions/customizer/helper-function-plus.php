<?php
/**
 * Helper functions.
 *
 * @package software
 */

/**
 * Get Footer widgets
 */
if (!function_exists('spice_software_plus_footer_widget_area')) {

    /**
     * Get Footer Default Sidebar
     *
     * @param  string $sidebar_id   Sidebar Id..
     * @return void
     */
    function spice_software_plus_footer_widget_area($sidebar_id) {

        if (is_active_sidebar($sidebar_id)) {
            dynamic_sidebar($sidebar_id);
        } elseif (current_user_can('edit_theme_options')) {

            global $wp_registered_sidebars;
            $sidebar_name = '';
            if (isset($wp_registered_sidebars[$sidebar_id])) {
                $sidebar_name = $wp_registered_sidebars[$sidebar_id]['name'];
            }
            ?>
            <div class="widget ast-no-widget-row">
                <h2 class='widget-title'><?php echo esc_html($sidebar_name); ?></h2>

                <p class='no-widget-text'>
                    <a href='<?php echo esc_url(admin_url('widgets.php')); ?>'>
                        <?php esc_html_e('Click here to assign a widget for this area.', 'spice-software-plus'); ?>
                    </a>
                </p>
            </div>
            <?php
        }
    }

}

/**
 * Function to get Footer Menu
 */
if (!function_exists('spice_software_plus_footer_bar_menu')) {

    /**
     * Function to get Footer Menu
     *
     */
    function spice_software_plus_footer_bar_menu() {

        ob_start();

        if (has_nav_menu('footer_menu')) {
            wp_nav_menu(
                    array(
                        'theme_location' => 'footer_menu',
                        'menu_class' => 'nav-menu',
                        'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                        'depth' => 1,
                    )
            );
        } else {
            if (is_user_logged_in() && current_user_can('edit_theme_options')) {
                ?>
                <a href="<?php echo esc_url(admin_url('/nav-menus.php?action=locations')); ?>"><?php esc_html_e('Assign Footer Menu', 'spice-software-plus'); ?></a>
                <?php
            }
        }

        return ob_get_clean();
    }

}

if (!function_exists('spice_software_plus_widget_layout')):

    function spice_software_plus_widget_layout() {

        $spice_software_plus_footer_widget = get_theme_mod('footer_widgets_section', 4);
        switch ($spice_software_plus_footer_widget) {
            case 1:
                include_once(SPICE_SOFTWAREP_PLUGIN_DIR.'/inc/inc/footer-widget/layout-1.php');
                break;

            case 2:
                include_once(SPICE_SOFTWAREP_PLUGIN_DIR.'/inc/inc/footer-widget/layout-2.php');
                break;

            case 3:
                include_once(SPICE_SOFTWAREP_PLUGIN_DIR.'/inc/inc/footer-widget/layout-3.php');
                break;

            case 4:
                include_once(SPICE_SOFTWAREP_PLUGIN_DIR.'/inc/inc/footer-widget/layout-4.php');
                break;

            case 5:
                include_once(SPICE_SOFTWAREP_PLUGIN_DIR.'/inc/inc/footer-widget/layout-5.php');
                break;

            case 6:
                include_once(SPICE_SOFTWAREP_PLUGIN_DIR.'/inc/inc/footer-widget/layout-6.php');
                break;

            case 7:
                include_once(SPICE_SOFTWAREP_PLUGIN_DIR.'/inc/inc/footer-widget/layout-7.php');
                break;

            case 8:
                include_once(SPICE_SOFTWAREP_PLUGIN_DIR.'/inc/inc/footer-widget/layout-8.php');
                break;

            case 9:
                include_once(SPICE_SOFTWAREP_PLUGIN_DIR.'/inc/inc/footer-widget/layout-9.php');
                break;
        }
    }

endif;

/* Footer Widget Layout section */
if (!function_exists('spice_software_plus_footer_widget_layout_section')) {

    function spice_software_plus_footer_widget_layout_section() {
            /**
             * Get Footer widgets
             */
            if (!function_exists('spice_software_plus_footer_widget_area')) {

                /**
                 * Get Footer Default Sidebar
                 *
                 * @param  string $sidebar_id   Sidebar Id..
                 * @return void
                 */
                function spice_software_plus_footer_widget_area($sidebar_id) {

                    if (is_active_sidebar($sidebar_id)) {
                        dynamic_sidebar($sidebar_id);
                    } elseif (current_user_can('edit_theme_options')) {

                        global $wp_registered_sidebars;
                        $sidebar_name = '';
                        if (isset($wp_registered_sidebars[$sidebar_id])) {
                            $sidebar_name = $wp_registered_sidebars[$sidebar_id]['name'];
                        }
                        ?>
                        <div class="widget ast-no-widget-row">
                            <h2 class='widget-title'><?php echo esc_html($sidebar_name); ?></h2>

                            <p class='no-widget-text'>
                                <a href='<?php echo esc_url(admin_url('widgets.php')); ?>'>
                                    <?php esc_html_e('Click here to assign a widget for this area.', 'spice-software-plus'); ?>
                                </a>
                            </p>
                        </div>
                        <?php
                    }
                }

            }
            /* Function for widget sectons */
            spice_software_plus_widget_layout();
        /* Function for widget sectons */

        
    }

}
/* Footer Widget Layout section */

/* Footer Bar layout section */
if (!function_exists('spice_software_plus_footer_bar_section')) {

    function spice_software_plus_footer_bar_section() {
        if (get_theme_mod('ftr_bar_enable', true) == true):
            $advance_footer_bar_section = get_theme_mod('advance_footer_bar_section', 1);
            switch ($advance_footer_bar_section) {
                case 1:
                    include_once(SPICE_SOFTWAREP_PLUGIN_DIR.'/inc/inc/footer-bar/layout-1.php');
                    break;

                case 2:
                    include_once(SPICE_SOFTWAREP_PLUGIN_DIR.'/inc/inc/footer-bar/layout-2.php');
                    break;
            }
        endif;
        }

}
/* Footer Bar layout section */

if (!function_exists('spice_software_plus_custom_navigation')) :

    function spice_software_plus_custom_navigation() {
        if(get_theme_mod('post_nav_style_setting','pagination')=='pagination'){
            echo '<div class="row justify-content-center">';
                    $obj = new spice_software_plus_pagination();
                    $obj->spice_software_plus_page();
                    echo '</div>';
        }else{
            echo do_shortcode('[ajax_posts]');
        }
    }
endif;
add_action('spice_software_plus_post_navigation', 'spice_software_plus_custom_navigation');

function spice_software_plus_comment($comment, $args, $depth) {
    $tag = 'div';
    $add_below = 'comment';
    ?>
    <div class="media comment-box">
        <span class="pull-left-comment">
    <?php echo get_avatar($comment, 100, null, 'comments user', array('class' => array('img-fluid comment-img'))); ?>
        </span>
        <div class="media-body">
            <div class="comment-detail">
                <h5 class="comment-detail-title"><?php esc_html(comment_author()); ?><time class="comment-date"><?php printf(esc_html__('%1$s  %2$s', 'spice-software-plus'), esc_html(get_comment_date()), esc_html(get_comment_time())); ?></time></h5>
    <?php comment_text(); ?>

                <div class="reply">
    <?php comment_reply_link(array_merge($args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
                </div>
            </div>


        </div>      

    </div>
    <?php
}

if (!function_exists('spice_software_plus_posted_content')) :

    /**
     * Content
     *
     */
    function spice_software_plus_posted_content() {
        $blog_content = get_theme_mod('spice_software_blog_content', 'excerpt');
        $excerpt_length = get_theme_mod('spice_software_blog_content_length', 30);

        if ('excerpt' == $blog_content) {
            $excerpt = spice_software_the_excerpt(absint($excerpt_length));
            if (!empty($excerpt)) :
                ?>


                <?php
                echo wp_kses_post(wpautop($excerpt));
                ?>


            <?php endif;
        } else {
            ?>

            <?php the_content(); ?>

        <?php }
       
    }

endif;



if (!function_exists('spice_software_plus_the_excerpt')) :

    /**
     * Generate excerpt.
     *
     */
    function spice_software_plus_the_excerpt($length = 0, $post_obj = null) {

        global $post;

        if (is_null($post_obj)) {
            $post_obj = $post;
        }

        $length = absint($length);

        if (0 === $length) {
            return;
        }

        $source_content = $post_obj->post_content;

        if (!empty($post_obj->post_excerpt)) {
            $source_content = $post_obj->post_excerpt;
        }

        $source_content = preg_replace('`\[[^\]]*\]`', '', $source_content);
        $trimmed_content = wp_trim_words($source_content, $length, '&hellip;');
        return $trimmed_content;
    }

endif;

if (!function_exists('spice_software_plus_button_title')) :

    /**
     * Display Button on Archive/Blog Page 
     */
    function spice_software_plus_button_title() {
        if (get_theme_mod('spice_software_enable_blog_read_button', true) == true):
            $blog_button = get_theme_mod('spice_software_blog_button_title', 'Read More');

            if (empty($blog_button)) {
                return;
            }
            echo '<p><a href = "' . esc_url(get_the_permalink()) . '" class="more-link">' . esc_html($blog_button) . ' <i class="fa fa-long-arrow-right"></i></a></p>';

        endif;
    }

endif;


/*  Related posts
  /* ------------------------------------ */
if (!function_exists('spice_software_plus_related_posts')) {

    function spice_software_plus_related_posts() {
        wp_reset_postdata();
        global $post;

        // Define shared post arguments
        $args = array(
            'no_found_rows' => true,
            'update_post_meta_cache' => false,
            'update_post_term_cache' => false,
            'ignore_sticky_posts' => 1,
            'orderby' => 'rand',
            'post__not_in' => array($post->ID),
            'posts_per_page' => 10
        );
        // Related by categories
        if (get_theme_mod('spice_software_related_post_option') == 'categories') {

            $cats = get_post_meta($post->ID, 'related-cat', true);

            if (!$cats) {
                $cats = wp_get_post_categories($post->ID, array('fields' => 'ids'));
                $args['category__in'] = $cats;
            } else {
                $args['cat'] = $cats;
            }
        }
        // Related by tags
        if (get_theme_mod('spice_software_related_post_option') == 'tags') {

            $tags = get_post_meta($post->ID, 'related-tag', true);

            if (!$tags) {
                $tags = wp_get_post_tags($post->ID, array('fields' => 'ids'));
                $args['tag__in'] = $tags;
            } else {
                $args['tag_slug__in'] = explode(',', $tags);
            }
            if (!$tags) {
                $break = true;
            }
        }

        $query = !isset($break) ? new WP_Query($args) : new WP_Query;
        return $query;
    }

}

/**
 * Displays the author name
 */
function spice_software_plus_get_author_name($post) {

    $user_id = $post->post_author;
    if (empty($user_id)) {
        return;
    }

    $user_info = get_userdata($user_id);
    echo esc_html($user_info->display_name);
}

function spice_software_plus_footer_section_fn(){
spice_software_plus_before_footer_section_trigger();?>
<footer class="site-footer" <?php if (!empty(get_theme_mod('ftr_wgt_background_img'))): ?> style="background-image: url(<?php echo get_theme_mod('ftr_wgt_background_img'); ?>);" <?php endif; ?>>  
    <?php $fwidgets_overlay_section_color = get_theme_mod('spice_software_plus_fwidgets_overlay_section_color', 'rgba(0, 0, 0, 0.7)'); ?>
    <div class="overlay" <?php
    if (!empty(get_theme_mod('ftr_wgt_background_img'))) {
        if (get_theme_mod('spice_software_plus_fwidgets_image_overlay', true) == true) {
            ?> style="background-color:<?php echo $fwidgets_overlay_section_color; ?>" <?php
             }
         }
         ?>>

        <?php if (get_theme_mod('ftr_widgets_enable', true) === true) { ?>
            <div class="container">
                <?php
//        Widget Layout
                spice_software_plus_footer_widget_layout_section();
                ?>
            </div>
        <?php } ?>

        <!-- Animation lines-->
        <div _ngcontent-kga-c2="" class="lines">
            <div _ngcontent-kga-c2="" class="line"></div>
            <div _ngcontent-kga-c2="" class="line"></div>
            <div _ngcontent-kga-c2="" class="line"></div>
        </div>
        <!--/ Animation lines-->

        <?php
//    Footer bar
        spice_software_plus_footer_bar_section();
        ?>
    </div>
</footer>
<?php spice_software_plus_after_footer_section_trigger();
$ribon_enable = get_theme_mod('scrolltotop_setting_enable', true);
$client_bg_clr=get_theme_mod('clt_bg_color','#fff');
if ($ribon_enable == true) {
    ?>
    <div class="scroll-up custom <?php echo get_theme_mod('scroll_position_setting', 'right'); ?>"><a href="#totop"><i class="<?php echo get_theme_mod('spice_software_plus_scroll_icon_class', 'fa fa-arrow-up'); ?>"></i></a></div>
<?php } ?>

<style type="text/css">
    .scroll-up {
        <?php echo get_theme_mod('scroll_position_setting', 'right'); ?>: 30px !important;
    }
    .scroll-up.custom a {
        border-radius: <?php echo get_theme_mod('spice_software_plus_scroll_border_radius', 3); ?>px;
    }  
    <?php if(get_theme_mod('apply_scrll_top_clr_enable',false)==true):?>
    .scroll-up.custom a {
    background: <?php echo get_theme_mod('spice_software__scroll_bg_color','#00BFFF');?>;
    color: <?php echo get_theme_mod('spice_software__scroll_icon_color','#fff');?>;
    }
    .scroll-up.custom a:hover,
    .scroll-up.custom a:active {
        background: <?php echo get_theme_mod('spice_software__scroll_bghover_color','#fff');?>;
        color: <?php echo get_theme_mod('spice_software__scroll_iconhover_color','#00BFFF');?>;
    }
<?php endif;?>  
</style>
<style type="text/css">

    .sponsors {
        background-color: <?php echo $client_bg_clr; ?>;
    }
    <?php
    if (get_theme_mod('testimonial_image_overlay', true) != false) {
        $testimonial_overlay_section_color = get_theme_mod('testimonial_overlay_section_color', 'rgba(1, 7, 12, 0.8)');
        ?>
        .testi-1:before {
            background-color:<?php echo $testimonial_overlay_section_color; ?>;
        }
        .testi-2:before {
            background-color:<?php echo $testimonial_overlay_section_color; ?>;
        }
        .testi-3:before{
            background-color: <?php echo $testimonial_overlay_section_color; ?>;
        }
        .testi-4:before {
            background-color: <?php echo $testimonial_overlay_section_color; ?>;
        <?php } ?>
        </style>

    <style type="text/css">
            .site-footer {
            background-repeat:  <?php echo get_theme_mod('footer_widget_reapeat', 'no-repeat'); ?>;
            background-position: <?php echo get_theme_mod('footer_widget_position', 'left top'); ?>;
            background-size: <?php echo get_theme_mod('footer_widget_bg_size', 'cover'); ?>;
            background-attachment: <?php echo get_theme_mod('footer_widget_bg_attachment', 'scroll'); ?>;
        }
    </style>
<?php
$slidelayout=get_theme_mod('home_testimonial_design_layout',1);
$slideitem=get_theme_mod('home_testimonial_slide_item',1);
if($slidelayout==2 && $slideitem==3){?>
    <style type="text/css">
        #testimonial-carousel2 .testmonial-block, 
        .page-template-template-testimonial-6 .testmonial-block {
            padding: 15px;
            margin: 0 15px 15px;
        }
        #testimonial-carousel2 .avatar, 
        .page-template-template-testimonial-6 .avatar {
            position: relative;
        }
    </style>
    <?php
}
//Stick Header
    if (get_theme_mod('sticky_header_enable', false) == true):
        include_once(SPICE_SOFTWAREP_PLUGIN_DIR.'/inc/inc/sticky-header/sticky-with' . get_theme_mod('sticky_header_animation', '') . '-animation.php');
    endif;
}

add_action('spice_software_plus_footer_section_hook', 'spice_software_plus_footer_section_fn');

function the_company_blog_meta() {
    if (get_theme_mod('spice_software_enable_blog_author') == false) {
        $string = ' In ';
    } else {
        $string = ' in ';
    }
    return $string;
}

function the_company_single_meta() {
    if (get_theme_mod('spice_software_plus_enable_single_post_admin') == false) {
        $string = ' In ';
    } else {
        $string = ' in ';
    }
    return $string;
}

add_action('spice_software_plus_sticky_header_logo', 'spice_software_plus_sticky_header_logo_fn');

function spice_software_plus_sticky_header_logo_fn() {
    $custom_logo_id = get_theme_mod('custom_logo');
    $image = wp_get_attachment_image_src($custom_logo_id, 'full');
    if (get_theme_mod('sticky_header_device_enable', 'desktop') == 'desktop') {
        $sticky_header_logo_desktop = get_theme_mod('sticky_header_logo_desktop', '');
        if (!empty($sticky_header_logo_desktop)):
            ?>
            <a href="<?php echo esc_url(home_url('/')); ?>" class="navbar-brand sticky-logo" style="display: none;">
                <img src="<?php echo esc_url($sticky_header_logo_desktop); ?>" class="custom-logo"></a>
            <?php
        else:
            ?>
            <a href="<?php echo esc_url(home_url('/')); ?>" class="navbar-brand sticky-logo" style="display: none;">
                <img src="<?php echo esc_url($image[0]); ?>" class="custom-logo"></a>
            <?php
            endif;
        } elseif (get_theme_mod('sticky_header_device_enable', 'desktop') == 'mobile') {
            $sticky_header_logo_mbl = get_theme_mod('sticky_header_logo_mbl', '');
            if (!empty($sticky_header_logo_mbl)):
                ?>
            <a href="<?php echo esc_url(home_url('/')); ?>" class="navbar-brand sticky-logo-mbl" style="display: none;">
                <img width="280" height="48" src="<?php echo esc_url($sticky_header_logo_mbl); ?>" class="custom-logo"></a>
            <?php else:
            ?><a href="<?php echo esc_url(home_url('/')); ?>" class="navbar-brand sticky-logo-mbl" style="display: none;">
                <img width="280" height="48" src="<?php echo esc_url($image[0]); ?>" class="custom-logo"></a>
        <?php
            endif;
        } else {
            $sticky_header_logo_desktop = get_theme_mod('sticky_header_logo_desktop', '');
            if (!empty($sticky_header_logo_desktop)):
                ?>
            <a href="<?php echo esc_url(home_url('/')); ?>" class="navbar-brand sticky-logo" style="display: none;">
                <img src="<?php echo esc_url($sticky_header_logo_desktop); ?>" class="custom-logo"></a>
            <?php else:
            ?><a href="<?php echo esc_url(home_url('/')); ?>" class="navbar-brand sticky-logo" style="display: none;">
                <img src="<?php echo esc_url($image[0]); ?>" class="custom-logo"></a>
            <?php
            endif;

            $sticky_header_logo_mbl = get_theme_mod('sticky_header_logo_mbl', '');
            if (!empty($sticky_header_logo_mbl)):
                ?>
            <a href="<?php echo esc_url(home_url('/')); ?>" class="navbar-brand sticky-logo-mbl" style="display: none;">
                <img width="280" height="48" src="<?php echo esc_url($sticky_header_logo_mbl); ?>" class="custom-logo"></a>
            <?php else:
            ?><a href="<?php echo esc_url(home_url('/')); ?>" class="navbar-brand sticky-logo-mbl" style="display: none;">
                <img width="280" height="48" src="<?php echo esc_url($image[0]); ?>" class="custom-logo"></a>
        <?php
        endif;
    }
}

/* software Contact template function * */

function content_contact_data($data) {
    $contact_data = get_theme_mod($data);
    if (empty($contact_data)) {
        $contact_data = json_encode(array(
            array(
                'icon_value' => 'fa fa-inbox',
                'title' => 'Email Your Problem',
                'text' => __('Official: example@politics.com <br/>Help line: example@politics.com', 'spice-software-plus'),
                'id' => 'customizer_repeater_56d7ea7f40b60',
            ),
            array(
                'icon_value' => 'fa fa-phone',
                'title' => 'Call Us Any Time',
                'text' => __('+(15) 718-999-3939, <br/>+(15) 781-254-8437', 'spice-software-plus'),
                'id' => 'customizer_repeater_56d7ea7f40b61',
            ),
            array(
                'icon_value' => 'fa fa-envelope-o',
                'title' => 'Visit Our Office',
                'text' => __('490 E North. Orlando, FL <br/>North America', 'spice-software-plus'),
                'id' => 'customizer_repeater_56d7ea7f40b62',
            ),
        ));
    }
    return $contact_data;
}

// Add heder feature
add_action('spice_software_plus_header_feaure_section_hook','spice_software_plus_header_feaure_section_hook');
function spice_software_plus_header_feaure_section_hook(){

    include_once(SPICE_SOFTWAREP_PLUGIN_DIR.'/inc/inc/topbar-header.php');

    spice_software_plus_before_header_section_trigger();
    //Header Preset
    if (get_theme_mod('header_logo_placing', 'left')):
        $header_logo_path= SPICE_SOFTWAREP_PLUGIN_DIR.'/inc/inc/header-preset/menu-with-'.get_theme_mod('header_logo_placing','left').'-logo.php';
        include_once($header_logo_path);
    endif;
    
    if (get_theme_mod('search_effect_style_setting', 'toggle') != 'toggle'):?>
        <div id="searchbar_fullscreen" <?php if (get_theme_mod('search_effect_style_setting', 'popup_light') == 'popup_light'): ?> class="bg-light" <?php endif; ?>>
            <button type="button" class="close">×</button>
            <form method="get" id="searchform" autocomplete="off" class="search-form" action="<?php echo esc_url(home_url('/')); ?>"><label><input autofocus type="search" class="search-field" placeholder="Search Keyword" value="" name="s" id="s" autofocus></label><input type="submit" class="search-submit btn" value="<?php echo esc_html__('Search', 'spice_software_plus'); ?>"></form>
        </div>
    <?php
    endif;
    spice_software_plus_after_header_section_trigger();
}

//Container Setting For Page
function spice_software_container()
{
  if(get_theme_mod('page_container_setting','default')=='default')
{
 $container_width= " container_default";
}
elseif(get_theme_mod('page_container_setting','default')=='full_width_fluid')
{
 $container_width= "-fluid";
}
else
{
  $container_width= "-fluid streached";
}
return $container_width;
}

//Container Setting For Blog Post
function spice_software_blog_post_container()
{
  if(get_theme_mod('post_container_setting','default')=='default')
{
 $container_width= " container_default";
}
elseif(get_theme_mod('post_container_setting','default')=='full_width_fluid')
{
 $container_width= "-fluid";
}
else
{
  $container_width= "-fluid streached";
}
return $container_width;
}

//Conainer Setting For Single Post

function spice_software_single_post_container()
{
  if(get_theme_mod('single_post_container_setting','default')=='default')
{
 $container_width= " container_default";
}
elseif(get_theme_mod('single_post_container_setting','default')=='full_width_fluid')
{
 $container_width= "-fluid";
}
else
{
  $container_width= "-fluid streached";
}
return $container_width;
}

//Preloader feature section function
function spice_software_plus_preloader_feaure_section_fn(){
global $template;
$col=explode("-",basename($template));
if (array_key_exists(1,$col)){
$column=$col[1];
}
else{
$column='';
}
//Preloader
if(get_theme_mod('preloader_enable',false)==true && ($column!='portfolio')):
 include_once(SPICE_SOFTWAREP_PLUGIN_DIR.'/inc/inc/preloader/preloader-'.get_theme_mod('preloader_style',1).'.php');
endif;
}
add_action('spice_software_plus_preloader_feaure_section_hook','spice_software_plus_preloader_feaure_section_fn');