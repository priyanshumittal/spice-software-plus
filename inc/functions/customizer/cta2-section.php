<?php
//Callout Section
$wp_customize->add_section('home_cta2_page_section', array(
    'title' => esc_html__('Callout 2 Settings', 'spice-software-plus'),
    'panel' => 'section_settings',
    'priority' => 15,
));

// Enable call to action section
$wp_customize->add_setting('cta2_section_enable', array(
    'default' => true,
    'sanitize_callback' => 'spice_software_sanitize_checkbox'
    ));

$wp_customize->add_control(new Spice_Software_Toggle_Control($wp_customize, 'cta2_section_enable',
                array(
            'label' => esc_html__('Enable Home Callout 2 section', 'spice-software-plus'),
            'type' => 'toggle',
            'section' => 'home_cta2_page_section',
                )
));

//cta2 Background Image
$wp_customize->add_setting('callout_cta2_background', array(
    'default'=> SPICE_SOFTWAREP_PLUGIN_URL.'/inc/images/bg/cta-3.jpg',
    'sanitize_callback' => 'esc_url_raw',
));

$wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'callout_cta2_background', array(
            'label' => esc_html__('Background Image', 'spice-software-plus'),
            'section' => 'home_cta2_page_section',
            'settings' => 'callout_cta2_background',
            'active_callback' => 'spice_software_plus_cta2_callback'
        )));

// Image overlay
$wp_customize->add_setting('cta2_image_overlay', array(
    'default' => true,
    'sanitize_callback' => 'sanitize_text_field',
));

$wp_customize->add_control('cta2_image_overlay', array(
    'label' => esc_html__('Enable callout image overlay', 'spice-software-plus'),
    'section' => 'home_cta2_page_section',
    'type' => 'checkbox',
    'active_callback' => 'spice_software_plus_cta2_callback'
));


//callout Background Overlay Color
$wp_customize->add_setting('cta2_overlay_section_color', array(
    'sanitize_callback' => 'sanitize_text_field',
    'default' => 'rgba(0, 11, 24, 0.8)',
));

$wp_customize->add_control(new Spice_Software_Plus_Customize_Alpha_Color_Control($wp_customize, 'cta2_overlay_section_color', array(
            'label' => esc_html__('Callout image overlay color', 'spice-software-plus'),
            'palette' => true,
            'active_callback' => 'spice_software_plus_cta2_callback',
            'section' => 'home_cta2_page_section')
));




$wp_customize->add_setting(
        'home_cta2_title',
        array(
            'default' => esc_html__('Easy & Simple - No Coding Required!', 'spice-software-plus'),
            'transport' => $selective_refresh,
        )
);
$wp_customize->add_control('home_cta2_title', array(
    'label' => esc_html__('Title', 'spice-software-plus'),
    'section' => 'home_cta2_page_section',
    'type' => 'text',
    'active_callback' => 'spice_software_plus_cta2_callback'
    ));

$wp_customize->add_setting(
        'home_cta2_desc',
        array(
            'default' => esc_html__('It is a long established fact that a reader will be distracted by the readable content of a page when looking <br> at its layout. The point of using Lorem ipsum dolor sit amet elit.', 'spice-software-plus'),
            'transport' => $selective_refresh,
        )
);
$wp_customize->add_control('home_cta2_desc', array(
    'label' => esc_html__('Description', 'spice-software-plus'),
    'section' => 'home_cta2_page_section',
    'type' => 'textarea',
    'active_callback' => 'spice_software_plus_cta2_callback'
    ));

//button text 1
$wp_customize->add_setting(
        'home_cta2_btn1_text',
        array(
            'default' => esc_html__('Purchase Now', 'spice-software-plus'),
            'capability' => 'edit_theme_options',
            'sanitize_callback' => 'sanitize_text_field',
            'transport' => $selective_refresh,
        )
);

$wp_customize->add_control(
        'home_cta2_btn1_text',
        array(
            'label' => esc_html__('Button Text 1', 'spice-software-plus'),
            'section' => 'home_cta2_page_section',
            'type' => 'text',
            'active_callback' => 'spice_software_plus_cta2_callback'
));

//Button link 1
$wp_customize->add_setting(
        'home_cta2_btn1_link',
        array(
            'default' => '#',
            'capability' => 'edit_theme_options',
            'sanitize_callback' => 'sanitize_text_field',
            'transport' => $selective_refresh,
));


$wp_customize->add_control(
        'home_cta2_btn1_link',
        array(
            'label' => esc_html__('Button Link 1', 'spice-software-plus'),
            'section' => 'home_cta2_page_section',
            'type' => 'text',
            'active_callback' => 'spice_software_plus_cta2_callback'
));

//button 1 target
$wp_customize->add_setting(
        'home_cta2_btn1_link_target',
        array('sanitize_callback' => 'sanitize_text_field',
));

$wp_customize->add_control(
        'home_cta2_btn1_link_target',
        array(
            'type' => 'checkbox',
            'label' => esc_html__('Open link in new tab', 'spice-software-plus'),
            'section' => 'home_cta2_page_section',
            'active_callback' => 'spice_software_plus_cta2_callback'
        )
);

//button text 2
$wp_customize->add_setting(
        'home_cta2_btn2_text',
        array(
            'default' => esc_html__('Get In Touch', 'spice-software-plus'),
            'capability' => 'edit_theme_options',
            'sanitize_callback' => 'sanitize_text_field',
            'transport' => $selective_refresh,
        )
);

$wp_customize->add_control(
        'home_cta2_btn2_text',
        array(
            'label' => esc_html__('Button Text 2', 'spice-software-plus'),
            'section' => 'home_cta2_page_section',
            'type' => 'text',
            'active_callback' => 'spice_software_plus_cta2_callback'
));

//Button link 2
$wp_customize->add_setting(
        'home_cta2_btn2_link',
        array(
            'default' => '#',
            'capability' => 'edit_theme_options',
            'sanitize_callback' => 'sanitize_text_field',
            'transport' => $selective_refresh,
));


$wp_customize->add_control(
        'home_cta2_btn2_link',
        array(
            'label' => esc_html__('Button Link 2', 'spice-software-plus'),
            'section' => 'home_cta2_page_section',
            'type' => 'text',
            'active_callback' => 'spice_software_plus_cta2_callback'
));

//button 2 target
$wp_customize->add_setting(
        'home_cta2_btn2_link_target',
        array('sanitize_callback' => 'sanitize_text_field',
));

$wp_customize->add_control(
        'home_cta2_btn2_link_target',
        array(
            'type' => 'checkbox',
            'label' => esc_html__('Open link in new tab', 'spice-software-plus'),
            'section' => 'home_cta2_page_section',
            'active_callback' => 'spice_software_plus_cta2_callback'
        )
);


/**
 * Add selective refresh for Front page pricing section controls.
 */
$wp_customize->selective_refresh->add_partial('home_cta2_title', array(
    'selector' => '.cta-2 .cta-block h2',
    'settings' => 'home_cta2_title',
    'render_callback' => 'home_cta2_title_render_callback',
));

$wp_customize->selective_refresh->add_partial('home_cta2_desc', array(
    'selector' => '.cta-2 .cta-block p',
    'settings' => 'home_cta2_desc',
    'render_callback' => 'home_cta2_desc_render_callback',
));

$wp_customize->selective_refresh->add_partial('home_cta2_btn1_text', array(
    'selector' => '.cta-2 .btn-default',
    'settings' => 'home_cta2_btn1_text',
    'render_callback' => 'home_cta2_btn1_text_render_callback',
));

$wp_customize->selective_refresh->add_partial('home_cta2_btn2_text', array(
    'selector' => '.cta-2 .btn-light',
    'settings' => 'home_cta2_btn2_text',
    'render_callback' => 'home_cta2_btn2_text_render_callback',
));

function home_cta2_title_render_callback() {
    return get_theme_mod('home_cta2_title');
}

function home_cta2_desc_render_callback() {
    return get_theme_mod('home_cta2_desc');
}

function home_cta2_btn1_text_render_callback() {
    return get_theme_mod('home_cta2_btn1_text');
}

function home_cta2_btn2_text_render_callback() {
    return get_theme_mod('home_cta2_btn2_text');
}?>