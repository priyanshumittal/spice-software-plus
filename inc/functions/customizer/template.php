<?php
/**
 * Template Options Customizer
 *
 * @package software
 */
function spice_software_plus_template_customizer($wp_customize) {
    $wp_customize->add_panel('spice_software_template_settings',
            array(
                'priority' => 920,
                'capability' => 'edit_theme_options',
                'title' => __('Template Settings', 'spice-software-plus')
            )
    );

 // add section to manage About us page settings
    $wp_customize->add_section(
            'about_page_section',
            array(
                'title' => __('About Us Page Settings', 'spice-software-plus'),
                'panel' => 'spice_software_template_settings',
                'priority' => 100,
            )
    );

    // enable/disable Funfact section from about page
    $wp_customize->add_setting('about_funfact_enable',
            array(
                'default' => true,
                'sanitize_callback' => 'spice_software_sanitize_checkbox'
    ));

    $wp_customize->add_control(new Spice_Software_Toggle_Control($wp_customize, 'about_funfact_enable',
                    array(
                'label' =>__('Enable Funfact Section', 'spice-software-plus'),
                'section' => 'about_page_section',
                'type' => 'toggle',
                    )
    ));

    // enable/disable Team section from about page
    $wp_customize->add_setting('about_team_enable',
        array(
            'default' => true,
            'sanitize_callback' => 'spice_software_sanitize_checkbox'
            ));

    $wp_customize->add_control(new Spice_Software_Toggle_Control( $wp_customize, 'about_team_enable',
        array(
            'label'    => __( 'Enable Team Section', 'spice-software-plus' ),
            'section'  => 'about_page_section',
            'type'     => 'toggle',
        )
    ));

    //Team Design for about template
        $wp_customize->add_setting( 'about_team_design_layout', array( 'default' => 1) );
        $wp_customize->add_control( 'about_team_design_layout', 
        array(
            'label'    => __( 'Team Design', 'spice-software-plus' ),
            'active_callback'=> 'spice_software_plus_abt_team_callback',
            'section'  => 'about_page_section',
            'type'     => 'select',
            'choices'=>array(
                1=>__('Design 1', 'spice-software-plus'),
                2=>__('Design 2', 'spice-software-plus'),
                3=>__('Design 3', 'spice-software-plus'),
                4=>__('Design 4', 'spice-software-plus')
                )
        ));

    // enable/disable Testimonial section from about page
    $wp_customize->add_setting('about_testimonial_enable',
        array(
            'default' => true,
            'sanitize_callback' => 'spice_software_sanitize_checkbox'
            ));

    $wp_customize->add_control(new Spice_Software_Toggle_Control( $wp_customize, 'about_testimonial_enable',
        array(
            'label'    => __( 'Enable Testimonial Section', 'spice-software-plus' ),
            'section'  => 'about_page_section',
            'type'     => 'toggle',
        )
    ));

    //Testimonial Design
        $wp_customize->add_setting( 'about_testimonial_design_layout', array( 'default' => 1) );
        $wp_customize->add_control( 'about_testimonial_design_layout', 
        array(
            'label'    => __( 'Testimonial Design', 'spice-software-plus' ),
            'active_callback'=> 'spice_software_plus_abt_testi_callback',
            'section'  => 'about_page_section',
            'type'     => 'select',
            'choices'=>array(
                1=>__('Design 1', 'spice-software-plus'),
                2=>__('Design 2', 'spice-software-plus'),
                3=>__('Design 3', 'spice-software-plus'),
                4=>__('Design 4', 'spice-software-plus')
                )
        ));
    
    // enable/disable Contact Detail Section from about page
    $wp_customize->add_setting('about_contact_detail_enable',
            array(
                'default' => true,
                'sanitize_callback' => 'spice_software_sanitize_checkbox'
    ));

    $wp_customize->add_control(new Spice_Software_Toggle_Control($wp_customize, 'about_contact_detail_enable',
                    array(
                'label' => __('Enable Contact Detail section', 'spice-software-plus'),
                'section' => 'about_page_section',
                'type' => 'toggle',
                    )
    ));

// Add section to manage Service page settings
    $wp_customize->add_section(
            'service_page_section',
            array(
                'title' => esc_html__('Service Page Settings', 'spice-software-plus'),
                'panel' => 'spice_software_template_settings',
                'priority' => 200,
            )
    );

    // enable/disable Service section from service page
    $wp_customize->add_setting('services_enable',
            array(
                'default' => true,
                'sanitize_callback' => 'spice_software_sanitize_checkbox'
    ));

    $wp_customize->add_control(new Spice_Software_Toggle_Control($wp_customize, 'services_enable',
                    array(
                'label' => __('Enable/Disable Service section', 'spice-software-plus'),
                'section' => 'service_page_section',
                'type' => 'toggle',
                    )
    ));

    // enable/disable Funfact section from service page
    $wp_customize->add_setting('service_funfact_enable',
            array(
                'default' => true,
                'sanitize_callback' => 'spice_software_sanitize_checkbox'
    ));

    $wp_customize->add_control(new Spice_Software_Toggle_Control($wp_customize, 'service_funfact_enable',
                    array(
                'label' => __('Enable/Disable FunFact section', 'spice-software-plus'),
                'section' => 'service_page_section',
                'type' => 'toggle',
                    )
    ));

    // enable/disable Testimonial section from service page
    $wp_customize->add_setting('service_testimonial_enable',
            array(
                'default' => true,
                'sanitize_callback' => 'spice_software_sanitize_checkbox'
    ));

    $wp_customize->add_control(new Spice_Software_Toggle_Control($wp_customize, 'service_testimonial_enable',
                    array(
                'label' => __('Enable/Disable Testimonial section', 'spice-software-plus'),
                'section' => 'service_page_section',
                'type' => 'toggle',
                    )
    ));


    // enable/disable Contact Detail section from service page
    $wp_customize->add_setting('service_contact_detail_enable',
            array(
                'default' => true,
                'sanitize_callback' => 'spice_software_sanitize_checkbox'
    ));

    $wp_customize->add_control(new Spice_Software_Toggle_Control($wp_customize, 'service_contact_detail_enable',
                    array(
                'label' => __('Enable/Disable Contact Detail section', 'spice-software-plus'),
                'section' => 'service_page_section',
                'type' => 'toggle',
                    )
    ));

// Add section to manage Team page settings
    $wp_customize->add_section(
            'testimonial_page_section',
            array(
                'title' => esc_html__('Testimonial Page Settings', 'spice-software-plus'),
                'panel' => 'spice_software_template_settings',
                'priority' => 300,
            )
    );



    // enable/disable Service section from service page
    $wp_customize->add_setting('testimonial_enable',
            array(
                'default' => true,
                'sanitize_callback' => 'spice_software_sanitize_checkbox'
    ));

    $wp_customize->add_control(new Spice_Software_Toggle_Control($wp_customize, 'testimonial_enable',
                    array(
                'label' => __('Enable/Disable Testimonial section', 'spice-software-plus'),
                'section' => 'testimonial_page_section',
                'type' => 'toggle',
                    )
    ));

    // enable/disable Funfact section from team page
    $wp_customize->add_setting('testimonial_funfact_enable',
            array(
                'default' => true,
                'sanitize_callback' => 'spice_software_sanitize_checkbox'
    ));

    $wp_customize->add_control(new Spice_Software_Toggle_Control($wp_customize, 'testimonial_funfact_enable',
                    array(
                'label' => __('Enable/Disable FunFact section', 'spice-software-plus'),
                'section' => 'testimonial_page_section',
                'type' => 'toggle',
                    )
    ));

    // enable/disable Contact detail section from team page
    $wp_customize->add_setting('testimonial_contact_detail_enable',
            array(
                'default' => true,
                'sanitize_callback' => 'spice_software_sanitize_checkbox'
    ));

    $wp_customize->add_control(new Spice_Software_Toggle_Control($wp_customize, 'testimonial_contact_detail_enable',
                    array(
                'label' => __('Enable/Disable Contact Detail section', 'spice-software-plus'),
                'section' => 'testimonial_page_section',
                'type' => 'toggle',
                    )
    ));

 //    add section to manage Team page settings
    $wp_customize->add_section(
            'team_page_section',
            array(
                'title' => __('Team Page Settings', 'spice-software-plus'),
                'panel' => 'spice_software_template_settings',
                'priority' => 400,
            )
    );

    // enable/disable Service section from service page
    $wp_customize->add_setting('team_enable',
            array(
                'default' => true,
                'sanitize_callback' => 'spice_software_sanitize_checkbox'
    ));

    $wp_customize->add_control(new Spice_Software_Toggle_Control($wp_customize, 'team_enable',
                    array(
                'label' => __('Enable/Disable Team section', 'spice-software-plus'),
                'section' => 'team_page_section',
                'type' => 'toggle',
                    )
    ));

    // enable/disable Funfact section from team page
    $wp_customize->add_setting('team_funfact_enable',
            array(
                'default' => true,
                'sanitize_callback' => 'spice_software_sanitize_checkbox'
    ));

    $wp_customize->add_control(new Spice_Software_Toggle_Control($wp_customize, 'team_funfact_enable',
                    array(
                'label' => __('Enable/Disable FunFact section', 'spice-software-plus'),
                'section' => 'team_page_section',
                'type' => 'toggle',
                    )
    ));

    // enable/disable Contact detail section from team page
    $wp_customize->add_setting('team_contact_detail_enable',
            array(
                'default' => true,
                'sanitize_callback' => 'spice_software_sanitize_checkbox'
    ));

    $wp_customize->add_control(new Spice_Software_Toggle_Control($wp_customize, 'team_contact_detail_enable',
                    array(
                'label' => __('Enable/Disable Contact Detail section', 'spice-software-plus'),
                'section' => 'team_page_section',
                'type' => 'toggle',
                    )
    ));

    //     add section to manage Error page settings
    $wp_customize->add_section(
            'error_page_section',
            array(
                'title' => __('Error Page Settings', 'spice-software-plus'),
                'panel' => 'spice_software_template_settings',
                'priority' => 500,
                )
    );

    // enable/disable Contact Detail Section from Error page
    $wp_customize->add_setting('error_contact_detail_enable',
            array(
                'default' => true,
                'sanitize_callback' => 'spice_software_sanitize_checkbox'
    ));

    $wp_customize->add_control(new Spice_Software_Toggle_Control($wp_customize, 'error_contact_detail_enable',
                    array(
                'label' => __('Enable Contact Detail section', 'spice-software-plus'),
                'section' => 'error_page_section',
                'type' => 'toggle',
                    )
    ));


    // Add section to manage Portfolio page settings
    $wp_customize->add_section(
            'porfolio_page_section',
            array(
                'title' => __('Portfolio Page Settings', 'spice-software-plus'),
                'panel' => 'spice_software_template_settings',
                'priority' => 600,
            )
    );

    // Portfolio page title
    $wp_customize->add_setting(
            'porfolio_page_title', array(
        'default' => __('Our Portfolio', 'spice-software-plus'),
        'sanitize_callback' => 'sanitize_text_field',
    ));

    $wp_customize->add_control(
            'porfolio_page_title',
            array(
                'type' => 'text',
                'label' => __('Title', 'spice-software-plus'),
                'section' => 'porfolio_page_section',
            )
    );

    // Portfolio page subtitle
    $wp_customize->add_setting(
            'porfolio_page_subtitle', array(
        'default' => __('Our Recent Works', 'spice-software-plus'),
        'sanitize_callback' => 'sanitize_text_field',
    ));

    $wp_customize->add_control(
            'porfolio_page_subtitle',
            array(
                'type' => 'text',
                'label' => __('Sub Title', 'spice-software-plus'),
                'section' => 'porfolio_page_section',
            )
    );

    // Number of Portfolio in Portfolio's template
    $wp_customize->add_setting(
            'portfolio_numbers_options',
            array(
                'default' => 4,
                'capability' => 'edit_theme_options',
                'sanitize_callback' => 'sanitize_text_field',
            )
    );
    $wp_customize->add_control('portfolio_numbers_options',
            array(
                'type' => 'number',
                'label' => __('Number of portfolio on portfolio templates', 'spice-software-plus'),
                'section' => 'porfolio_page_section',
                'input_attrs' => array(
                    'min' => '1', 'step' => '1', 'max' => '50',
                ),
            )
    );


    // Add section to manage Portfolio category page settings
    $wp_customize->add_section(
            'porfolio_category_page_section',
            array(
                'title' => __('Portfolio Category Page Settings', 'spice-software-plus'),
                'panel' => 'spice_software_template_settings',
                'priority' => 700,
            )
    );
    // Portfolio category page title
    $wp_customize->add_setting(
            'porfolio_category_page_title', array(
        'default' => __('Portfolio Category Title', 'spice-software-plus'),
        'sanitize_callback' => 'sanitize_text_field',
    ));
    $wp_customize->add_control(
            'porfolio_category_page_title',
            array(
                'type' => 'text',
                'label' => __('Title', 'spice-software-plus'),
                'section' => 'porfolio_category_page_section',
            )
    );
    // Portfolio category page subtitle
    $wp_customize->add_setting(
            'porfolio_category_page_desc', array(
        'default' => __('Morbi facilisis, ipsum ut ullamcorper venenatis, nisi diam placerat turpis, at auctor nisi magna cursus arcu.', 'spice-software-plus'),
        'sanitize_callback' => 'sanitize_text_field',
    ));
    $wp_customize->add_control(
            'porfolio_category_page_desc',
            array(
                'type' => 'text',
                'label' => __('Description', 'spice-software-plus'),
                'section' => 'porfolio_category_page_section',
            )
    );
    // Number of portfolio column layout
    $wp_customize->add_setting('portfolio_cat_column_layout', array(
        'default' => 3,
        'sanitize_callback' => 'sanitize_text_field',
    ));
    $wp_customize->add_control('portfolio_cat_column_layout', array(
        'type' => 'select',
        'label' => __('Select column layout', 'spice-software-plus'),
        'section' => 'porfolio_category_page_section',
        'choices' => array(2 => 2, 3 => 3, 4 => 4),
    ));

    // Add section to manage Contact page settings
    $wp_customize->add_section(
            'contact_page_section',
            array(
                'title' => __('Contact Page Setting', 'spice-software-plus'),
                'panel' => 'spice_software_template_settings',
                'priority' => 800,
            )
    );

    // Contact form title
    $wp_customize->add_setting(
            'contact_cf7_title', array(
        'default' => __("Don't Hesitate To Contact Us", 'spice-software-plus'),
        'sanitize_callback' => 'sanitize_text_field',
    ));

    $wp_customize->add_control(
            'contact_cf7_title',
            array(
                'type' => 'text',
                'label' => __('Contact Form Title', 'spice-software-plus'),
                'section' => 'contact_page_section',
            )
    );

    $wp_customize->add_control(
            'contact_info_title',
            array(
                'type' => 'text',
                'label' => __('Contact Title', 'spice-software-plus'),
                'section' => 'contact_page_section',
            )
    );

    if (class_exists('Spice_Software_Plus_Repeater')) {
        $wp_customize->add_setting('spice_software_plus_contact_content', array());

        $wp_customize->add_control(new Spice_Software_Plus_Repeater($wp_customize, 'spice_software_plus_contact_content', array(
                    'label' => esc_html__('Contact Details', 'spice-software-plus'),
                    'section' => 'contact_page_section',
                    'priority' => 10,
                    'add_field_label' => esc_html__('Add new Contact Info', 'spice-software-plus'),
                    'item_name' => esc_html__('Contact', 'spice-software-plus'),
                    'customizer_repeater_icon_control' => true,
                    'customizer_repeater_title_control' => true,
                    'customizer_repeater_text_control' => true,
        )));
    }

    // google map shortcode
    $wp_customize->add_setting('contact_google_map_shortcode', array(
        'capability' => 'edit_theme_options',
        'sanitize_callback' => 'sanitize_text_field',
    ));
    $wp_customize->add_control('contact_google_map_shortcode', array(
        'label' => __('Google Map Shortcode', 'spice-software-plus'),
        'section' => 'contact_page_section',
        'type' => 'textarea',
    ));

    // Contact form 7 shortcode
    $wp_customize->add_setting('contact_form_shortcode', array(
        'capability' => 'edit_theme_options',
        'sanitize_callback' => 'sanitize_text_field',
    ));
    $wp_customize->add_control('contact_form_shortcode', array(
        'label' => esc_html__('Contact Form shortcode', 'spice-software-plus'),
        'section' => 'contact_page_section',
        'type' => 'textarea',
    ));

    
    $wp_customize->selective_refresh->add_partial('contact_cf7_title', array(
        'selector' => '.section-space.contact-form h2',
        'settings' => 'contact_cf7_title',
        'render_callback' => 'contact_cf7_title_render_callback'
    ));

    $wp_customize->selective_refresh->add_partial('porfolio_category_page_title', array(
        'selector' => '.portfolio-cat-page h2.section-title',
        'settings' => 'porfolio_category_page_title',
        'render_callback' => 'porfolio_category_page_title_render_callback'
    ));

    $wp_customize->selective_refresh->add_partial('porfolio_category_page_desc', array(
        'selector' => '.portfolio-cat-page h5.section-subtitle ',
        'settings' => 'porfolio_category_page_desc',
        'render_callback' => 'porfolio_category_page_desc_render_callback'
    ));

    $wp_customize->selective_refresh->add_partial('porfolio_page_title', array(
        'selector' => '.portfolio-page h2.section-title',
        'settings' => 'porfolio_page_title',
        'render_callback' => 'porfolio_page_title_render_callback'
    ));

    $wp_customize->selective_refresh->add_partial('porfolio_page_subtitle', array(
        'selector' => '.portfolio-page h5.section-subtitle',
        'settings' => 'porfolio_page_subtitle',
        'render_callback' => 'porfolio_page_subtitle_render_callback'
    ));

    function contact_cf7_title_render_callback() {
        return get_theme_mod('contact_cf7_title');
    }

    function contact_info_title_render_callback() {
        return get_theme_mod('contact_info_title');
    }

    function porfolio_category_page_title_render_callback() {
        return get_theme_mod('porfolio_category_page_title');
    }

    function porfolio_category_page_desc_render_callback() {
        return get_theme_mod('porfolio_category_page_desc');
    }

    function porfolio_page_title_render_callback() {
        return get_theme_mod('porfolio_page_title');
    }

    function porfolio_page_subtitle_render_callback() {
        return get_theme_mod('porfolio_page_subtitle');
    }

}
add_action('customize_register', 'spice_software_plus_template_customizer');
?>