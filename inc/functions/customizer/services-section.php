<?php
$theme = wp_get_theme();
if('Spice Software Dark' == $theme->name) {
    $ss_service_design=3;
}
else{
    $ss_service_design=1;
}
$wp_customize->add_section('services_section', array(
    'title' => __('Services Settings', 'spice-software-plus'),
    'panel' => 'section_settings',
    'priority' => 14,
));


//Service Section

$wp_customize->add_setting('home_service_section_enabled', array(
    'default' => true,
    'sanitize_callback' => 'spice_software_sanitize_checkbox'
));

$wp_customize->add_control(new Spice_Software_Toggle_Control($wp_customize, 'home_service_section_enabled',
                array(
            'label' => __('Enable Services on homepage', 'spice-software-plus'),
            'type' => 'toggle',
            'section' => 'services_section',
                )
));

//Service section title
$wp_customize->add_setting('home_service_section_title', array(
    'capability' => 'edit_theme_options',
    'default' => __('Why Choose Us?', 'spice-software-plus'),
    'sanitize_callback' => 'softwarep_home_page_sanitize_text',
    'transport' => $selective_refresh,
));

$wp_customize->add_control('home_service_section_title', array(
    'label' => __('Title', 'spice-software-plus'),
    'section' => 'services_section',
    'type' => 'text',
    'active_callback' => 'spice_software_plus_service_callback'
));

// Service section description
$wp_customize->add_setting('home_service_section_discription', array(
    'capability' => 'edit_theme_options',
    'default' => __('Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam.', 'spice-software-plus'),
    'transport' => $selective_refresh,
));


$wp_customize->add_control('home_service_section_discription', array(
    'label' => __('Sub Title', 'spice-software-plus'),
    'section' => 'services_section',
    'type' => 'text',
    'active_callback' => 'spice_software_plus_service_callback'
));

//Style Design
$wp_customize->add_setting('home_serive_design_layout', array('default' => $ss_service_design));
$wp_customize->add_control('home_serive_design_layout',
        array(
            'label' => __('Design Style', 'spice-software-plus'),
            'section' => 'services_section',
            'type' => 'select',
            'choices' => array(
                1 => __('Design 1', 'spice-software-plus'),
                2 => __('Design 2', 'spice-software-plus'),
                3 => __('Design 3', 'spice-software-plus'),
                4 => __('Design 4', 'spice-software-plus')
            ),
            'active_callback' => 'spice_software_plus_service_callback'
));

if (class_exists('Spice_Software_Plus_Repeater')) {
    $wp_customize->add_setting('spice_software_service_content', array());

    $wp_customize->add_control(new Spice_Software_Plus_Repeater($wp_customize, 'spice_software_service_content', array(
                'label' => esc_html__('Services content', 'spice-software-plus'),
                'section' => 'services_section',
                'priority' => 10,
                'add_field_label' => esc_html__('Add new Service', 'spice-software-plus'),
                'item_name' => esc_html__('Service', 'spice-software-plus'),
                'customizer_repeater_icon_control' => true,
                'customizer_repeater_title_control' => true,
                'customizer_repeater_text_control' => true,
                'customizer_repeater_link_control' => true,
                'customizer_repeater_checkbox_control' => true,
                'customizer_repeater_image_control' => true,
                'active_callback' => 'spice_software_plus_service_callback'
    )));
}

$wp_customize->selective_refresh->add_partial('home_service_section_title', array(
    'selector' => '.services .section-title, .services2 .section-title, .services3 .section-title, .services4 .section-title',
    'settings' => 'home_service_section_title',
    'render_callback' => 'spice_software_plus_home_service_section_title_render_callback'
));

$wp_customize->selective_refresh->add_partial('home_service_section_discription', array(
    'selector' => '.services .section-subtitle, .services2 .section-subtitle, .services3 .section-subtitle, .services4 .section-subtitle',
    'settings' => 'home_service_section_discription',
    'render_callback' => 'spice_software_plus_home_service_section_discription_render_callback'
));

$wp_customize->selective_refresh->add_partial('service_viewmore_btn_text', array(
    'selector' => '.services .view-more-services',
    'settings' => 'service_viewmore_btn_text',
    'render_callback' => 'spice_software_plus_service_viewmore_btn_text_render_callback'
));

function spice_software_plus_home_service_section_title_render_callback() {
    return get_theme_mod('home_service_section_title');
}

function spice_software_plus_home_service_section_discription_render_callback() {
    return get_theme_mod('home_service_section_discription');
}

function spice_software_plus_service_viewmore_btn_text_render_callback() {
    return get_theme_mod('service_viewmore_btn_text');
}