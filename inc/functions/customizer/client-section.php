<?php
//Client Section
$wp_customize->add_section('home_client_section', array(
    'title' => esc_html__('Clients Settings', 'spice-software-plus'),
    'panel' => 'section_settings',
    'priority' => 22,
));

// Enable client section
$wp_customize->add_setting('client_section_enable', array(
    'default' => true,
    'sanitize_callback' => 'spice_software_sanitize_checkbox'
    ));

$wp_customize->add_control(new Spice_Software_Toggle_Control($wp_customize, 'client_section_enable',
                array(
            'label' => esc_html__('Enable Home Client section', 'spice-software-plus'),
            'type' => 'toggle',
            'section' => 'home_client_section',
                )
));

// clients & partners section title
$wp_customize->add_setting('home_client_section_title', array(
    'default' => esc_html__('We Work With The Best Clients', 'spice-software-plus'),
    'sanitize_callback' => 'spice_software_home_page_sanitize_text',
    'transport' => $selective_refresh,
));
$wp_customize->add_control('home_client_section_title', array(
    'label' => esc_html__('Title', 'spice-software-plus'),
    'section' => 'home_client_section',
    'type' => 'text',
    'active_callback' => 'spice_software_plus_sponsors_callback'
));

//clients & partners section discription
$wp_customize->add_setting('home_client_section_discription', array(
    'default' => esc_html__('It is a long established fact that a reader will be distracted by the readable content.', 'spice-software-plus'),
    'transport' => $selective_refresh,
));
$wp_customize->add_control('home_client_section_discription', array(
    'label' => esc_html__('Sub Title', 'spice-software-plus'),
    'section' => 'home_client_section',
    'type' => 'textarea',
    'active_callback' => 'spice_software_plus_sponsors_callback'
));

//Client Background Overlay Color
$theme = wp_get_theme();
if('Spice Software Dark' == $theme->name) {
    $ss_clt_bg_color='#181a1f';
}
else{
    $ss_clt_bg_color='#f8f8f8';
}
$wp_customize->add_setting('clt_bg_color', array(
    'sanitize_callback' => 'sanitize_text_field',
    'default' => $ss_clt_bg_color,
));

$wp_customize->add_control(new Spice_Software_Plus_Customize_Alpha_Color_Control($wp_customize, 'clt_bg_color', array(
            'label' => esc_html__('Client Section Background Color', 'spice-software-plus'),
            'palette' => true,
            'active_callback' => 'spice_software_plus_sponsors_callback',
            'section' => 'home_client_section')
));

if (class_exists('Spice_Software_Plus_Repeater')) {
    $wp_customize->add_setting(
            'spice_software_clients_content', array(
            )
    );

    $wp_customize->add_control(
            new Spice_Software_Plus_Repeater(
                    $wp_customize, 'spice_software_clients_content', array(
                'label' => esc_html__('Clients content', 'spice-software-plus'),
                'section' => 'home_client_section',
                'add_field_label' => esc_html__('Add new client', 'spice-software-plus'),
                'item_name' => esc_html__('Client', 'spice-software-plus'),
                'customizer_repeater_image_control' => true,
                'customizer_repeater_link_control' => true,
                'customizer_repeater_checkbox_control' => true,
                'active_callback' => 'spice_software_plus_sponsors_callback'
                    )
            )
    );
}

$wp_customize->selective_refresh->add_partial('home_client_section_title', array(
    'selector' => '.sponsors .section-header h2',
    'settings' => 'home_client_section_title',
    'render_callback' => 'home_client_section_title_render_callback'
));

$wp_customize->selective_refresh->add_partial('home_client_section_discription', array(
    'selector' => '.sponsors .section-subtitle',
    'settings' => 'home_client_section_discription',
    'render_callback' => 'home_client_section_discription_render_callback'
));

function home_client_section_title_render_callback() {
    return get_theme_mod('home_client_section_title');
}

function home_client_section_discription_render_callback() {
    return get_theme_mod('home_client_section_discription');
}?>