<?php
function spice_software_plus_footer_customizer($wp_customize) {
// Only For Footer Widgets	
    class Spice_Software_Plus_Footer_Widgets_Customize_Control_Radio_Image extends WP_Customize_Control {

        /**
         * The type of customize control being rendered.
         *
         * @since 1.1.24
         * @var   string
         */
        public $type = 'radio-image';

        /**
         * Displays the control content.
         *
         * @since  1.1.24
         * @access public
         * @return void
         */
        public function render_content() {
            /* If no choices are provided, bail. */
            if (empty($this->choices)) {
                return;
            }
            ?>

            <?php if (!empty($this->label)) : ?>
                <span class="customize-control-title"><?php echo esc_html($this->label); ?></span>
            <?php endif; ?>

            <?php if (!empty($this->description)) : ?>
                <span class="description customize-control-description"><?php echo $this->description; ?></span>
            <?php endif; ?>

            <div id="<?php echo esc_attr("input_{$this->id}"); ?>">

                <?php foreach ($this->choices as $value => $args) : ?>

                    <input type="radio" value="<?php echo esc_attr($value); ?>" name="<?php echo esc_attr("_customize-radio-{$this->id}"); ?>" id="<?php echo esc_attr("{$this->id}-{$value}"); ?>" <?php $this->link(); ?> <?php checked($this->value(), $value); ?> />

                    <label for="<?php echo esc_attr("{$this->id}-{$value}"); ?>" class="<?php echo esc_attr("{$this->id}-{$value}"); ?>">
                        <?php if (!empty($args['label'])) { ?>
                            <span class="screen-reader-text"><?php echo esc_html($args['label']); ?></span>
                            <?php }?>
                        <img class="wp-ui-highlight" src="<?php echo esc_url(sprintf($args['url'], SPICE_SOFTWAREP_PLUGIN_URL, get_stylesheet_directory_uri())); ?>" 
                        <?php
                        if (!empty($args['label'])) {
                            echo 'alt="' . esc_attr($args['label']) . '"';
                        } ?>
                             />
                    </label>

                <?php endforeach; ?>

            </div><!-- .image -->
            <script type="text/javascript">
                jQuery(document).ready(function () {
                    jQuery('#<?php echo esc_attr("input_{$this->id}"); ?>').buttonset();
                });
            </script>

            <?php
        }

        /**
         * Loads the jQuery UI Button script and hooks our custom styles in.
         *
         * @since  1.1.24
         * @access public
         * @return void
         */
        public function enqueue() {
            wp_enqueue_script('jquery-ui-button');
            add_action('customize_controls_print_styles', array($this, 'print_styles'));
        }

        /**
         * Outputs custom styles to give the selected image a visible border.
         *
         * @since  1.1.24
         * @access public
         * @return void
         */
        public function print_styles() {
            ?>

            <style type="text/css" id="hybrid-customize-radio-image-css">
                #customize-control-footer_widgets_section label {
                    display: inline-block;
                    max-width: 20% !important;
                    padding: 3px;
                    font-size: inherit;
                    line-height: inherit;
                    height: auto;
                    cursor: pointer;
                    border-width: 0;
                    -webkit-appearance: none;
                    -webkit-border-radius: 0;
                    border-radius: 0;
                    white-space: nowrap;
                    -webkit-box-sizing: border-box;
                    -moz-box-sizing: border-box;
                    box-sizing: border-box;
                    color: inherit;
                    background: none;
                    -webkit-box-shadow: none;
                    box-shadow: none;
                    vertical-align: inherit;
                }


                #customize-control-footer_widgets_section .ui-buttonset
                {
                    text-align: left !important;
                }


            </style>
            <?php
        }

    }

//Only For Footer Bar
    class Spice_Software_Plus_Footer_Bar_Customize_Control_Radio_Image extends WP_Customize_Control {

        /**
         * The type of customize control being rendered.
         *
         * @since 1.1.24
         * @var   string
         */
        public $type = 'radio-image';

        /**
         * Displays the control content.
         *
         * @since  1.1.24
         * @access public
         * @return void
         */
        public function render_content() {
            /* If no choices are provided, bail. */
            if (empty($this->choices)) {
                return;
            }
            ?>

            <?php if (!empty($this->label)) : ?>
                <span class="customize-control-title"><?php echo esc_html($this->label); ?></span>
            <?php endif; ?>

            <?php if (!empty($this->description)) : ?>
                <span class="description customize-control-description"><?php echo $this->description; ?></span>
            <?php endif; ?>

            <div id="<?php echo esc_attr("input_{$this->id}"); ?>">

                <?php foreach ($this->choices as $value => $args) : ?>

                    <input type="radio" value="<?php echo esc_attr($value); ?>" name="<?php echo esc_attr("_customize-radio-{$this->id}"); ?>" id="<?php echo esc_attr("{$this->id}-{$value}"); ?>" <?php $this->link(); ?> <?php checked($this->value(), $value); ?> />

                    <label for="<?php echo esc_attr("{$this->id}-{$value}"); ?>" class="<?php echo esc_attr("{$this->id}-{$value}"); ?>">
                        <?php if (!empty($args['label'])) { ?>
                            <span class="screen-reader-text"><?php echo esc_html($args['label']); ?></span>
                            <?php } ?>
                        <img class="wp-ui-highlight" src="<?php echo esc_url(sprintf($args['url'], SPICE_SOFTWAREP_PLUGIN_URL, get_stylesheet_directory_uri())); ?>" 
                        <?php
                        if (!empty($args['label'])) {
                            echo 'alt="' . esc_attr($args['label']) . '"';
                        }
                        ?>
                             />
                    </label>

                <?php endforeach; ?>

            </div><!-- .image -->
            <script type="text/javascript">
                jQuery(document).ready(function () {
                    jQuery('#<?php echo esc_attr("input_{$this->id}"); ?>').buttonset();

                    //This Script for Home
                    if (jQuery('#_customize-input-home_portfolio_design_layout').val() == 1)
                    {
                        jQuery('#customize-control-portfolio_nav_style').show();
                    } else
                    {
                        jQuery('#customize-control-portfolio_nav_style').hide();
                    }
                    wp.customize('home_portfolio_design_layout', function (value) {
                        value.bind(
                                function (newval) {
                                    if (newval == 1)
                                    {
                                        jQuery('#customize-control-portfolio_nav_style').show();
                                    } else
                                    {
                                        jQuery('#customize-control-portfolio_nav_style').hide();
                                    }
                                }
                        );
                    }
                    );

                    //Home page news section
                    if (jQuery('#_customize-input-home_news_design_layout').val() == 2 || jQuery('#_customize-input-home_news_design_layout').val() == 4)
                    {
                        jQuery('#customize-control-spice_software_homeblog_layout').hide();
                    } else
                    {
                        jQuery('#customize-control-spice_software_homeblog_layout').show();
                    }
                    wp.customize('home_news_design_layout', function (value) {
                        value.bind(
                                function (newval) {
                                    if (newval == 2)
                                    {
                                        jQuery('#customize-control-spice_software_homeblog_layout').hide();
                                    } else
                                    {
                                        jQuery('#customize-control-spice_software_homeblog_layout').show();
                                    }
                                }
                        );
                    }
                    );


                    if ((jQuery('#_customize-input-footer_bar_sec1').val() == "custom_text"))
                    {
                        jQuery('#customize-control-footer_copyright').show();
                    } else
                    {
                        jQuery('#customize-control-footer_copyright').hide();
                    }
                    if ((jQuery('#_customize-input-footer_bar_sec2').val() == "custom_text"))
                    {
                        jQuery('#customize-control-footer_copyright_2').show();
                    } else
                    {
                        jQuery('#customize-control-footer_copyright_2').hide();
                    }


                    wp.customize('footer_bar_sec1', function (value) {
                        value.bind(
                                function (newval) {
                                    if (newval == "custom_text")
                                    {
                                        jQuery('#customize-control-footer_copyright').show();
                                    } else
                                    {
                                        jQuery('#customize-control-footer_copyright').hide();
                                    }
                                }
                        );
                    }
                    );
                    wp.customize('footer_bar_sec2', function (value) {
                        value.bind(
                                function (newval) {
                                    if (newval == "custom_text")
                                    {
                                        jQuery('#customize-control-footer_copyright_2').show();
                                    } else
                                    {
                                        jQuery('#customize-control-footer_copyright_2').hide();
                                    }
                                }
                        );
                    }
                    );
                });
            </script>

            <?php
        }

        /**
         * Loads the jQuery UI Button script and hooks our custom styles in.
         *
         * @since  1.1.24
         * @access public
         * @return void
         */
        public function enqueue() {
            wp_enqueue_script('jquery-ui-button');
            add_action('customize_controls_print_styles', array($this, 'print_styles'));
        }

        /**
         * Outputs custom styles to give the selected image a visible border.
         *
         * @since  1.1.24
         * @access public
         * @return void
         */
        public function print_styles() {
            ?>

            <style type="text/css" id="hybrid-customize-radio-image-css">
                #customize-control-footer_widgets_section label {
                    display: inline-block;
                    max-width: 20% !important;
                    padding: 3px;
                    font-size: inherit;
                    line-height: inherit;
                    height: auto;
                    cursor: pointer;
                    border-width: 0;
                    -webkit-appearance: none;
                    -webkit-border-radius: 0;
                    border-radius: 0;
                    white-space: nowrap;
                    -webkit-box-sizing: border-box;
                    -moz-box-sizing: border-box;
                    box-sizing: border-box;
                    color: inherit;
                    background: none;
                    -webkit-box-shadow: none;
                    box-shadow: none;
                    vertical-align: inherit;
                }


                #customize-control-footer_widgets_section .ui-buttonset
                {
                    text-align: left !important;
                }


            </style>
            <?php
        }

    }

    // Footer Widgets Section
    $wp_customize->add_section(
            'spice_software_fwidgets_setting_section',
            array(
                'title' => __('Footer Widgets', 'spice-software-plus'),
                'panel' => 'spice_software_general_settings',
                'priority' => 200,
            )
    );

    //Enable/Disable Footer Widgets
    $wp_customize->add_setting('ftr_widgets_enable',
            array(
                'default' => true,
                'sanitize_callback' => 'spice_software_sanitize_checkbox'
            )
    );

    $wp_customize->add_control(new Spice_Software_Toggle_Control($wp_customize, 'ftr_widgets_enable',
                    array(
                'label' => __('Enable/Disable Footer Widgets', 'spice-software-plus'),
                'section' => 'spice_software_fwidgets_setting_section',
                'type' => 'toggle',
                'priority' => 1,
                    )
    ));

    if (class_exists('Spice_Software_Plus_Footer_Widgets_Customize_Control_Radio_Image')) {
        $wp_customize->add_setting(
                'footer_widgets_section', array(
            'default' => '4',
                )
        );

        $wp_customize->add_control(
                new Spice_Software_Plus_Footer_Widgets_Customize_Control_Radio_Image(
                        $wp_customize, 'footer_widgets_section', array(
                    'label' => esc_html__('Footer widget layout', 'spice-software-plus'),
                    'priority' => 199,
                    'section' => 'spice_software_fwidgets_setting_section',
                    'active_callback' => 'spice_software_plus_ftr_widgets_hide_show_callback',
                    'choices' => array(
                        '1' => array(
                            'url' => trailingslashit(SPICE_SOFTWAREP_PLUGIN_URL) . 'inc/images/footer-widgets/1.png',
                        ),
                        '2' => array(
                            'url' => trailingslashit(SPICE_SOFTWAREP_PLUGIN_URL) . 'inc/images/footer-widgets/2.png',
                        ),
                        '3' => array(
                            'url' => trailingslashit(SPICE_SOFTWAREP_PLUGIN_URL) . 'inc/images/footer-widgets/3.png',
                        ),
                        '4' => array(
                            'url' => trailingslashit(SPICE_SOFTWAREP_PLUGIN_URL) . 'inc/images/footer-widgets/4.png',
                        ),
                        '5' => array(
                            'url' => trailingslashit(SPICE_SOFTWAREP_PLUGIN_URL) . 'inc/images/footer-widgets/3-3-6.png',
                        ),
                        '6' => array(
                            'url' => trailingslashit(SPICE_SOFTWAREP_PLUGIN_URL) . 'inc/images/footer-widgets/3-6-3.png',
                        ),
                        '7' => array(
                            'url' => trailingslashit(SPICE_SOFTWAREP_PLUGIN_URL) . 'inc/images/footer-widgets/6-3-3.png',
                        ),
                        '8' => array(
                            'url' => trailingslashit(SPICE_SOFTWAREP_PLUGIN_URL) . 'inc/images/footer-widgets/8-4.png',
                        ),
                        '9' => array(
                            'url' => trailingslashit(SPICE_SOFTWAREP_PLUGIN_URL) . 'inc/images/footer-widgets/4-8.png',
                        ),
                    ),
                        )
                )
        );
    }

    //Footer Background Image
    $wp_customize->add_setting('ftr_wgt_background_img', array(
        'sanitize_callback' => 'esc_url_raw',
    ));

    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'ftr_wgt_background_img', array(
                'label' => __('Footer Widgets Background Image', 'spice-software-plus'),
                'priority' => 200,
                'section' => 'spice_software_fwidgets_setting_section',
                'settings' => 'ftr_wgt_background_img',
                'active_callback' => 'spice_software_plus_ftr_widgets_hide_show_callback',
    )));


    //Footer Widget Repeat
    $wp_customize->add_setting('footer_widget_reapeat', array('default' => 'no-repeat'));
    $wp_customize->add_control('footer_widget_reapeat',
            array(
                'label' => __('Background Image Repeat', 'spice-software-plus'),
                'priority' => 201,
                'active_callback' => 'spice_software_plus_ftr_widgets_hide_show_callback',
                'section' => 'spice_software_fwidgets_setting_section',
                'type' => 'select',
                'choices' => array(
                    'no-repeat' => __('No Repeat', 'spice-software-plus'),
                    'repeat' => __('Repeat All', 'spice-software-plus'),
                    'repeat-x' => __('Repeat Horizontally', 'spice-software-plus'),
                    'repeat-y' => __('Repeat Vertically', 'spice-software-plus'),
                )
    ));

    //Footer Widget position
    $wp_customize->add_setting('footer_widget_position', array('default' => 'left top'));
    $wp_customize->add_control('footer_widget_position',
            array(
                'label' => __('Background Image Position', 'spice-software-plus'),
                'priority' => 201,
                'active_callback' => 'spice_software_plus_ftr_widgets_hide_show_callback',
                'section' => 'spice_software_fwidgets_setting_section',
                'type' => 'select',
                'choices' => array(
                    'left top' => __('Left Top', 'spice-software-plus'),
                    'left center' => __('Left Center', 'spice-software-plus'),
                    'left bottom' => __('left bottom', 'spice-software-plus'),
                    'right top' => __('Right Top', 'spice-software-plus'),
                    'right center' => __('Right Center', 'spice-software-plus'),
                    'right bottom' => __('Right Bottom', 'spice-software-plus'),
                    'center top' => __('Center Top', 'spice-software-plus'),
                    'center center' => __('Center Center', 'spice-software-plus'),
                    'center bottom' => __('Center Bottom', 'spice-software-plus'),
                )
    ));

    //Footer Widget Size
    $wp_customize->add_setting('footer_widget_bg_size', array('default' => 'cover'));
    $wp_customize->add_control('footer_widget_bg_size',
            array(
                'label' => __('Background Size', 'spice-software-plus'),
                'priority' => 201,
                'active_callback' => 'spice_software_plus_ftr_widgets_hide_show_callback',
                'section' => 'spice_software_fwidgets_setting_section',
                'type' => 'select',
                'choices' => array(
                    'cover' => __('Cover', 'spice-software-plus'),
                    'contain' => __('Contain', 'spice-software-plus'),
                    'auto' => __('Auto', 'spice-software-plus'),
                )
    ));

    //Footer Widget Background Attachment
    $wp_customize->add_setting('footer_widget_bg_attachment', array('default' => 'scroll'));
    $wp_customize->add_control('footer_widget_bg_attachment',
            array(
                'label' => __('Background Attachment', 'spice-software-plus'),
                'description' => __('Note: Background Image Repeat and Background Image Position will not work with Background Attachment Fixed property', 'spice-software-plus'),
                'priority' => 201,
                'active_callback' => 'spice_software_plus_ftr_widgets_hide_show_callback',
                'section' => 'spice_software_fwidgets_setting_section',
                'type' => 'select',
                'choices' => array(
                    'scroll' => __('Scroll', 'spice-software-plus'),
                    'fixed' => __('Fixed', 'spice-software-plus'),
                )
    ));

    // Image overlay
    $wp_customize->add_setting('spice_software_plus_fwidgets_image_overlay', array(
        'default' => true,
        'sanitize_callback' => 'sanitize_text_field',
    ));

    $wp_customize->add_control('spice_software_plus_fwidgets_image_overlay', array(
        'label' => __('Enable Footer Widgets image overlay', 'spice-software-plus'),
        'priority' => 201,
        'active_callback' => 'spice_software_plus_ftr_widgets_hide_show_callback',
        'section' => 'spice_software_fwidgets_setting_section',
        'type' => 'checkbox',
    ));


    //Testimonial Background Overlay Color
    $wp_customize->add_setting('spice_software_plus_fwidgets_overlay_section_color', array(
        'sanitize_callback' => 'sanitize_text_field',
        'default' => 'rgba(0, 0, 0, 0.7)',
    ));

    $wp_customize->add_control(new Spice_Software_Plus_Customize_Alpha_Color_Control($wp_customize, 'spice_software_plus_fwidgets_overlay_section_color', array(
                'label' => __('Footer Widgets image overlay color', 'spice-software-plus'),
                'priority' => 202,
                'active_callback' => 'spice_software_plus_ftr_widgets_hide_show_callback',
                'palette' => true,
                'section' => 'spice_software_fwidgets_setting_section')
    ));


    $wp_customize->add_section('footer_section',
            array(
                'title' => esc_html__('Footer Bar', 'spice-software-plus'),
                'priority' => 200,
                'panel' => 'spice_software_general_settings',
            )
    );

    /*     * *********************** Eanble Footer ******************************** */


    //Enable/Disable Foot bar
    $wp_customize->add_setting('ftr_bar_enable',
            array(
                'default' => true,
                'sanitize_callback' => 'spice_software_sanitize_checkbox'
            )
    );

    $wp_customize->add_control(new Spice_Software_Toggle_Control($wp_customize, 'ftr_bar_enable',
                    array(
                'label' => __('Enable/Disable Footer Bar', 'spice-software-plus'),
                'section' => 'footer_section',
                'type' => 'toggle',
                'priority' => 1,
                    )
    ));


    if (class_exists('Spice_Software_Plus_Footer_Bar_Customize_Control_Radio_Image')) {
        $wp_customize->add_setting(
                'advance_footer_bar_section', array(
            'default' => '1',
                )
        );

        $wp_customize->add_control(
                new Spice_Software_Plus_Footer_Bar_Customize_Control_Radio_Image(
                        $wp_customize, 'advance_footer_bar_section', array(
                    'label' => esc_html__('Footer Bar layout', 'spice-software-plus'),
                    'priority' => 2,
                    'active_callback' => 'spice_software_plus_footer_callback',
                    'section' => 'footer_section',
                    'choices' => array(
                        '1' => array(
                            'url' => trailingslashit(SPICE_SOFTWAREP_PLUGIN_URL) . 'inc/images/footer-bar/footer-layout-1-76x48.png',
                        ),
                        '2' => array(
                            'url' => trailingslashit(SPICE_SOFTWAREP_PLUGIN_URL) . 'inc/images/footer-bar/footer-layout-2-76x48.png',
                        ),
                    ),
                        )
                )
        );
    }

    //Footer bar section 1
    $wp_customize->add_setting('footer_bar_sec1', array('default' => 'custom_text'));
    $wp_customize->add_control('footer_bar_sec1',
            array(
                'label' => __('Section 1', 'spice-software-plus'),
                'priority' => 3,
                'active_callback' => 'spice_software_plus_footer_callback',
                'section' => 'footer_section',
                'type' => 'select',
                'choices' => array(
                    'none' => __('None', 'spice-software-plus'),
                    'footer_menu' => __('Footer Menu', 'spice-software-plus'),
                    'custom_text' => __('Copyright Text', 'spice-software-plus'),
                    'widget' => __('Widget', 'spice-software-plus')
                )
    ));


    /*     * *********************** Copyright Section 1******************************** */
    $wp_customize->add_setting('footer_copyright',
            array(
                'default' => '<span class="copyright">'.__( 'Proudly powered by <a href="https://wordpress.org"> WordPress</a> | Theme: <a href="https://spicethemes.com/spice-software-wordpress-theme" rel="nofollow">Spice Software</a> by <a href="https://spicethemes.com" rel="nofollow">Spicethemes</a>', 'spice-software-plus').'</span>',
                'capability' => 'edit_theme_options',
                'sanitize_callback' => 'spice_software_copyright_sanitize_text',
            )
    );

    $wp_customize->add_control('footer_copyright',
            array(
                'label' => __('Copyright Section 1', 'spice-software-plus'),
                'section' => 'footer_section',
                'type' => 'textarea',
                'priority' => 4,
                'active_callback' => 'spice_software_plus_footer_callback'
            )
    );

    //Footer bar section 2
    $wp_customize->add_setting('footer_bar_sec2', array('default' => 'none'));
    $wp_customize->add_control('footer_bar_sec2',
            array(
                'label' => __('Section 2', 'spice-software-plus'),
                'priority' => 5,
//			'active_callback'=> 'spice_software_plus_footer_callback',
                'active_callback' => function($control) {
                    return (
                            spice_software_plus_footer_callback($control) &&
                            spice_software_plus_footer_column_callback($control)
                            );
                },
                'section' => 'footer_section',
                'type' => 'select',
                'choices' => array(
                    'none' => __('None', 'spice-software-plus'),
                    'footer_menu' => __('Footer Menu', 'spice-software-plus'),
                    'custom_text' => __('Copyright Text', 'spice-software-plus'),
                    'widget' => __('Widget', 'spice-software-plus')
                )
    ));

    /*     * *********************** Copyright Section 2******************************** */
    $wp_customize->add_setting('footer_copyright_2',
            array(
               'default' => '<span class="copyright">'.__( 'Proudly powered by <a href="https://wordpress.org"> WordPress</a> | Theme: <a href="https://spicethemes.com/spice-software-wordpress-theme" rel="nofollow">Spice Software</a> by <a href="https://spicethemes.com" rel="nofollow">Spicethemes</a>', 'spice-software-plus').'</span>',
                'capability' => 'edit_theme_options',
                'sanitize_callback' => 'spice_software_copyright_sanitize_text',
            )
    );

    $wp_customize->add_control('footer_copyright_2',
            array(
                'label' => __('Copyright Section 2', 'spice-software-plus'),
                'section' => 'footer_section',
                'type' => 'textarea',
                // 'active_callback' => function($control) {
                //     return (
                //             spice_software_plus_footer_callback($control) &&
                //             spice_software_plus_footer_column_callback($control)
                //             );
                // },
                'priority' => 6,
            )
    );

    //Footer Bar Border
    $wp_customize->add_setting('footer_bar_border',
            array(
                'default' => 0,
                'capability' => 'edit_theme_options',
            )
    );

    $wp_customize->add_control(new Spice_Software_Plus_Slider_Control($wp_customize, 'footer_bar_border',
                    array(
                'label' => __('Footer Bar Border', 'spice-software-plus'),
                'active_callback' => 'spice_software_plus_footer_callback',
                'section' => 'footer_section',
                'type' => 'slider',
                'min' => 0,
                'max' => 100,
                    )
    ));

    //Footer Bar Border Color
    $wp_customize->add_setting(
            'spice_software_footer_border_clr', array(
        'capability' => 'edit_theme_options',
        'default' => '#fff'
    ));

    $wp_customize->add_control(
            new WP_Customize_Color_Control(
                    $wp_customize,
                    'spice_software_footer_border_clr',
                    array(
                'label' => __('Border Color', 'spice-software-plus'),
                'section' => 'footer_section',
                'settings' => 'spice_software_footer_border_clr',
                'active_callback' => 'spice_software_plus_footer_callback',
    )));

    //Footer Bar border Style
    $wp_customize->add_setting('footer_border_style', array('default' => 'solid'));
    $wp_customize->add_control('footer_border_style',
            array(
                'label' => __('Border Style', 'spice-software-plus'),
                'active_callback' => 'spice_software_plus_footer_callback',
                'section' => 'footer_section',
                'type' => 'select',
                'choices' => array(
                    'solid' => __('Solid', 'spice-software-plus'),
                    'dotted' => __('Dotted', 'spice-software-plus'),
                    'dashed' => __('Dashed', 'spice-software-plus'),
                    'double' => __('Double', 'spice-software-plus'),
                    'groove' => __('Groove', 'spice-software-plus'),
                    'ridge' => __('Ridge', 'spice-software-plus'),
                    'inset' => __('Inset', 'spice-software-plus'),
                    'outset' => __('Outset', 'spice-software-plus')
                )
    ));

    $wp_customize->selective_refresh->add_partial('footer_copyright', array(
    'selector' => 'footer.site-footer .site-info span.copyright',
    'settings' => 'footer_copyright',
    'render_callback' => 'spice_software_plus_footer_copyright_render_callback',
    ));
    function spice_software_plus_footer_copyright_render_callback() {
        return get_theme_mod('footer_copyright');
    }
}
add_action('customize_register', 'spice_software_plus_footer_customizer');