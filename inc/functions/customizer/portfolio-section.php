<?php
$wp_customize->add_section('portfolio_section', array(
    'title' => __('Portfolio Settings', 'spice-software-plus'),
    'panel' => 'section_settings',
    'priority' => 16,
));

// Enable portfolio more btn
$wp_customize->add_setting('portfolio_section_enable', array(
    'default' => true,
    'sanitize_callback' => 'spice_software_sanitize_checkbox'
));

$wp_customize->add_control(new Spice_Software_Toggle_Control($wp_customize, 'portfolio_section_enable',
                array(
            'label' => __('Enable Home Portfolio section', 'spice-software-plus'),
            'type' => 'toggle',
            'section' => 'portfolio_section',
                )
));

// room section title
$wp_customize->add_setting('home_portfolio_section_title', array(
    'capability' => 'edit_theme_options',
    'sanitize_callback' => 'softwarep_home_page_sanitize_text',
    'default' => __('Look at our projects', 'spice-software-plus'),
));

$wp_customize->add_control('home_portfolio_section_title', array(
    'label' => __('Title', 'spice-software-plus'),
    'section' => 'portfolio_section',
    'type' => 'text',
    'active_callback' => 'spice_software_plus_portfolio_callback'
));

$wp_customize->add_setting('home_portfolio_section_subtitle', array(
    'capability' => 'edit_theme_options',
    'sanitize_callback' => 'softwarep_home_page_sanitize_text',
    'default' => __('Sedut perspiciatis unde o natus error sitlupttem accusantium doloremque laudantium totam rema ipsa quae ab illo inventore veritatis.', 'spice-software-plus'),
));

$wp_customize->add_control('home_portfolio_section_subtitle', array(
    'label' => __('Sub Title', 'spice-software-plus'),
    'section' => 'portfolio_section',
    'type' => 'text',
    'active_callback' => 'spice_software_plus_portfolio_callback'
));

//link
class WP_client_Customize_Control extends WP_Customize_Control {

    public $type = 'new_menu';

    /**
     * Render the control's content.
     */
    public function render_content() {
        ?>
        <a href="<?php bloginfo('url'); ?>/wp-admin/edit.php?post_type=software_portfolio" class="button"  target="_blank"><?php _e('Click here to add project', 'spice-software-plus'); ?></a>
        <?php
    }

}

$wp_customize->add_setting(
        'pro_project',
        array(
            'capability' => 'edit_theme_options',
            'sanitize_callback' => 'sanitize_text_field',
        )
);

$wp_customize->add_control(new WP_client_Customize_Control($wp_customize, 'pro_project', array(
            'section' => 'portfolio_section',
            'active_callback' => 'spice_software_plus_portfolio_callback'
                ))
);

// Number of Portfolio in Portfolio's Section
$wp_customize->add_setting(
        'home_portfolio_numbers_options',
        array(
            'default' => 3,
            'capability' => 'edit_theme_options',
            'sanitize_callback' => 'softwarep_home_page_sanitize_text',
        )
);
$wp_customize->add_control('home_portfolio_numbers_options',
        array(
            'type' => 'number',
            'label' => __('Number of projects on portfolio section', 'spice-software-plus'),
            'section' => 'portfolio_section',
            'input_attrs' => array(
                'min' => '1', 'step' => '1', 'max' => '50',
            ),
            'active_callback' => 'spice_software_plus_portfolio_callback'
        )
);

//Column Layout
$wp_customize->add_setting(
        'home_portfolio_column_laouts',
        array(
            'default' => 4,
            'capability' => 'edit_theme_options',
            'sanitize_callback' => 'softwarep_home_page_sanitize_text',
        )
);
$wp_customize->add_control('home_portfolio_column_laouts', array(
    'type' => 'radio',
    'label' => __('Portfolio Column layout', 'wallstreet'),
    'section' => 'portfolio_section',
    'choices' => array(3 => '4 Column Layout', 4 => '3 Column Layout', 6 => '2 Column Layout'),
    'active_callback' => 'spice_software_plus_portfolio_callback'
        )
);

/**
 * Add selective refresh for Front page funfact section controls.
 */
$wp_customize->selective_refresh->add_partial('home_portfolio_section_title', array(
    'selector' => '#portfolio .section-title',
    'settings' => 'home_portfolio_section_title',
    'render_callback' => 'spice_software_plus_home_portfolio_section_title_render_callback'
));

$wp_customize->selective_refresh->add_partial('home_portfolio_section_subtitle', array(
    'selector' => '#portfolio .section-subtitle',
    'settings' => 'home_portfolio_section_subtitle',
    'render_callback' => 'spice_software_plus_home_portfolio_section_subtitle_render_callback'
));

function spice_software_plus_home_portfolio_section_title_render_callback() {
    return get_theme_mod('home_portfolio_section_title');
}

function spice_software_plus_home_portfolio_section_subtitle_render_callback() {
    return get_theme_mod('home_portfolio_section_subtitle');
}