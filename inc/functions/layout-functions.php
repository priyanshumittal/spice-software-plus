<?php
if (!function_exists('starter_team_json')) {

    function starter_team_json() {
        return json_encode(array(
                array(
                    'image_url' => SPICE_SOFTWAREP_PLUGIN_URL . '/inc/images/team/item05.jpg',
                    'image_url2' => SPICE_SOFTWAREP_PLUGIN_URL . '/inc/images/portfolio/project-20.jpg',
                    'membername' => 'Danial Wilson',
                    'designation' => esc_html__('Senior Manager', 'spice-software-plus'),
                    'text' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime quae, dolores dicta. Blanditiis rem amet repellat, dolores nihil quae in mollitia asperiores ut rerum repellendus, voluptatum eum, officia laudantium quaerat?',
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_26d7ea7f40c56',
                    'social_repeater' => json_encode(
                            array(
                                array(
                                    'id' => 'customizer-repeater-social-repeater-37fb908374e06',
                                    'link' => 'facebook.com',
                                    'icon' => 'fa-brands fa-facebook-f',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-47fb9144530fc',
                                    'link' => 'twitter.com',
                                    'icon' => 'fa-brands fa-x-twitter',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb9750e1e09',
                                    'link' => 'linkedin.com',
                                    'icon' => 'fa-brands fa-linkedin',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-67fb0150e1e256',
                                    'link' => 'behance.com',
                                    'icon' => 'fa-brands fa-behance',
                                ),
                            )
                    ),
                ),
                array(
                    'image_url' => SPICE_SOFTWAREP_PLUGIN_URL . '/inc/images/team/item06.jpg',
                    'image_url2' => SPICE_SOFTWAREP_PLUGIN_URL . '/inc/images/about/about-8.jpg',
                    'membername' => 'Amanda Smith',
                    'designation' => esc_html__('Founder & CEO', 'spice-software-plus'),
                    'text' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime quae, dolores dicta. Blanditiis rem amet repellat, dolores nihil quae in mollitia asperiores ut rerum repellendus, voluptatum eum, officia laudantium quaerat?',
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_56d1ea2f40c66',
                    'social_repeater' => json_encode(
                            array(
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb9133a7772',
                                    'link' => 'facebook.com',
                                    'icon' => 'fa-brands fa-facebook-f',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb9160rt683',
                                    'link' => 'twitter.com',
                                    'icon' => 'fa-brands fa-x-twitter',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb916zzooc9',
                                    'link' => 'linkedin.com',
                                    'icon' => 'fa-brands fa-linkedin',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb916qqwwc784',
                                    'link' => 'behance.com',
                                    'icon' => 'fa-brands fa-behance',
                                ),
                            )
                    ),
                ),
                array(
                    'image_url' => SPICE_SOFTWAREP_PLUGIN_URL . '/inc/images/team/item07.jpg',
                    'image_url2' => SPICE_SOFTWAREP_PLUGIN_URL . '/inc/images/portfolio/project-11.jpg',
                    'membername' => 'Victoria Wills',
                    'designation' => esc_html__('Web Master', 'spice-software-plus'),
                    'text' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime quae, dolores dicta. Blanditiis rem amet repellat, dolores nihil quae in mollitia asperiores ut rerum repellendus, voluptatum eum, officia laudantium quaerat?',
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_56d7ea7f40c76',
                    'social_repeater' => json_encode(
                            array(
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb917e4c69e',
                                    'link' => 'facebook.com',
                                    'icon' => 'fa-brands fa-facebook-f',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb91830825c',
                                    'link' => 'twitter.com',
                                    'icon' => 'fa-brands fa-x-twitter',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb918d65f2e',
                                    'link' => 'linkedin.com',
                                    'icon' => 'fa-brands fa-linkedin',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb918d65f2e8',
                                    'link' => 'behance.com',
                                    'icon' => 'fa-brands fa-behance',
                                ),
                            )
                    ),
                ),
                array(
                    'image_url' => SPICE_SOFTWAREP_PLUGIN_URL . '/inc/images/team/item08.jpg',
                    'image_url2' => SPICE_SOFTWAREP_PLUGIN_URL . '/inc/images/about/about-8.jpg',
                    'membername' => 'Travis Marcus',
                    'designation' => esc_html__('UI Developer', 'spice-software-plus'),
                    'text' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime quae, dolores dicta. Blanditiis rem amet repellat, dolores nihil quae in mollitia asperiores ut rerum repellendus, voluptatum eum, officia laudantium quaerat?',
                    'open_new_tab' => 'no',
                    'id' => 'customizer_repeater_56d7ea7f40c86',
                    'social_repeater' => json_encode(
                            array(
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb925cedcb2',
                                    'link' => 'facebook.com',
                                    'icon' => 'fa-brands fa-facebook-f',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb92615f030',
                                    'link' => 'twitter.com',
                                    'icon' => 'fa-brands fa-x-twitter',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb9266c223a',
                                    'link' => 'linkedin.com',
                                    'icon' => 'fa-brands fa-linkedin',
                                ),
                                array(
                                    'id' => 'customizer-repeater-social-repeater-57fb9266c223a',
                                    'link' => 'behance.com',
                                    'icon' => 'fa-brands fa-behance',
                                ),
                            )
                    ),
                ),
            ));
    }

}
if (!function_exists('starter_service_json')) {

    function starter_service_json() {
        return json_encode(array(
            array(
                'icon_value' => 'fa-headphones',
                'title' => esc_html__('Unlimited Support', 'spice-software-plus'),
                'text' => esc_html__('Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore.', 'spice-software-plus'),
                'choice' => 'customizer_repeater_icon',
                'link' => '#',
                'open_new_tab' => 'yes',
                'id' => 'customizer_repeater_56d7ea7f40b56',
            ),
            array(
                'icon_value' => 'fa-solid fa-mobile-screen',
                'title' => esc_html__('Pixel Perfect Design', 'spice-software-plus'),
                'text' => esc_html__('Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore.', 'spice-software-plus'),
                'choice' => 'customizer_repeater_icon',
                'link' => '#',
                'open_new_tab' => 'yes',
                'id' => 'customizer_repeater_56d7ea7f40b66',
            ),
            array(
                'icon_value' => 'fa fa-cogs',
                'title' => esc_html__('Powerful Options', 'spice-software-plus'),
                'text' => esc_html__('Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore.', 'spice-software-plus'),
                'choice' => 'customizer_repeater_icon',
                'link' => '#',
                'open_new_tab' => 'yes',
                'id' => 'customizer_repeater_56d7ea7f40b86',
            ),
            array(
                'icon_value' => 'fa-desktop',
                'title' => esc_html__('Powerful Options', 'spice-software-plus'),
                'text' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore.',
                'choice' => 'customizer_repeater_icon',
                'link' => '#',
                'open_new_tab' => 'yes',
                'id' => 'customizer_repeater_52d7ea8f40b86',
            ),
            array(
                'icon_value' => 'fa-headphones',
                'title' => esc_html__('Powerful Options', 'spice-software-plus'),
                'text' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore.',
                'choice' => 'customizer_repeater_icon',
                'link' => '#',
                'open_new_tab' => 'yes',
                'id' => 'customizer_repeater_53d7ea9f40b86',
            ),
            array(
                'icon_value' => 'fa-desktop',
                'title' => esc_html__('Powerful Options', 'spice-software-plus'),
                'text' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore.',
                'choice' => 'customizer_repeater_icon',
                'link' => '#',
                'open_new_tab' => 'yes',
                'id' => 'customizer_repeater_59d7ea4f40b86',
            ),
        ));
    }

}
if (!function_exists('starter_funfact_json')) {

    function starter_funfact_json() {
        return json_encode(array(
            array(
                'icon_value' => 'fa-pie-chart',
                'title' => '4050',
                'text' => esc_html__('Satisfied Clients', 'spice-software-plus'),
                'id' => 'customizer_repeater_56d7ea7f40b56',
            ),
            array(
                'icon_value' => 'fa-regular fa-face-smile',
                'title' => '150',
                'text' => esc_html__('Finish Projects', 'spice-software-plus'),
                'id' => 'customizer_repeater_56d7ea7f40b66',
            ),
            array(
                'icon_value' => 'fa-users',
                'title' => '90%',
                'text' => esc_html__('Business Growth', 'spice-software-plus'),
                'id' => 'customizer_repeater_56d7ea7f40b86',
            ),
            array(
                'icon_value' => 'fa-coffee',
                'title' => '27',
                'text' => esc_html__('Consultants', 'spice-software-plus'),
                'id' => 'customizer_repeater_56d7ea7f40b87',
            ),
        ));
    }

}

if (!function_exists('spice_software_plus_dummy_portfolio_fn')) {

    function spice_software_plus_dummy_portfolio_fn($portfolio_col) {
        ?>
        <div id="content" class="tab-content" role="tablist">

            <div id="all" class="tab-pane fade show in active" role="tabpanel" aria-labelledby="tab-A">
                <div class="row">

                    <?php
                    for ($i = 1; $i <= 6; $i++) {
                        ?>
                        <div class="col-lg-<?php echo $portfolio_col; ?> col-md-6 col-sm-12">	
                            <figure class="portfolio-thumbnail">
                                <img src="<?php echo SPICE_SOFTWAREP_PLUGIN_URL; ?>/inc/images/portfolio/project-<?php echo $i; ?>.jpg" class="card-img-top" alt="Business Consultant">
                                <figcaption>
                                    <div class="entry-header"><h4 class="entry-title"><a href="#">Finance Planing</a></h4></div>
                                    <p>business, finance </p>
                                </figcaption>

                                <a data-toggle="modal" data-target="#basicExampleModal<?php echo $i; ?>"><i>+</i></a></figure>
                        </div>
                        <?php
                    }
                    ?>


                </div>
            </div>


            <div id="bussiness" class="tab-pane fade" role="tabpanel" aria-labelledby="tab-B">
                <div class="row">
                    <?php
                    for ($i = 7; $i <= 12; $i++) {
                        ?>
                        <div class="col-lg-<?php echo $portfolio_col; ?> col-md-6 col-sm-12">	
                            <figure class="portfolio-thumbnail">
                                <img src="<?php echo SPICE_SOFTWAREP_PLUGIN_URL; ?>/inc/images/portfolio/project-<?php echo $i; ?>.jpg" class="card-img-top" alt="Business Consultant">
                                <figcaption>
                                    <div class="entry-header"><h4 class="entry-title"><a href="#">Finance Planing</a></h4></div>
                                    <p>business, finance </p>
                                </figcaption>

                                <a data-toggle="modal" data-target="#basicExampleModal<?php echo $i; ?>"><i>+</i></a></figure>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>


            <div id="branding" class="tab-pane fade" role="tabpanel" aria-labelledby="tab-C">
                <div class="row">
                    <?php
                    for ($i = 13; $i <= 18; $i++) {
                        ?>
                        <div class="col-lg-<?php echo $portfolio_col; ?> col-md-6 col-sm-12">	
                            <figure class="portfolio-thumbnail">
                                <img src="<?php echo SPICE_SOFTWAREP_PLUGIN_URL; ?>/inc/images/portfolio/project-<?php echo $i; ?>.jpg" class="card-img-top" alt="Business Consultant">
                                <figcaption>
                                    <div class="entry-header"><h4 class="entry-title"><a href="#">Finance Planing</a></h4></div>
                                    <p>business, finance </p>
                                </figcaption>

                                <a data-toggle="modal" data-target="#basicExampleModal<?php echo $i; ?>"><i>+</i></a></figure>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>

        <?php
        for ($i = 1; $i <= 18; $i++) {
            ?>
            <!-- Modal -->
            <div class="modal fade" id="basicExampleModal<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-body p-0">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>

                            <!-- Grid row -->
                            <div class="row">

                                <!-- Grid column -->
                                <div class="col-md-6 py-5 pl-5">

                                    <article class="post text-center">
                                        <div class="entry-header">
                                            <h2 class="entry-title"><a href="#" alt="Multi-purpose">Finance Planing</a></h2>
                                        </div>
                                        <div class="entry-content">
                                            <p>sellus facilisis, nunc in lacinia auctor, eeros lacus aliquet velit, quis lobortis risus nunc nec nisi maecans et turpis vitae velit.volutpat.</p>
                                            <p>Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Duis aute irure dolor</p>
                                        </div>
                                    </article>

                                </div>
                                <!-- Grid column -->

                                <!-- Grid column -->
                                <div class="col-md-6">

                                    <div class="view rounded-right">
                                        <img class="img-fluid" src="<?php echo esc_url(SPICE_SOFTWAREP_PLUGIN_URL); ?>/inc/images/portfolio/project-<?php echo $i; ?>.jpg" alt="Sample image">
                                    </div>

                                </div>
                                <!-- Grid column -->

                            </div>
                            <!-- Grid row -->

                        </div>
                    </div>
                </div>
            </div>
            <?php
        }
    }

}

add_action('spice_software_plus_dummy_portfolio_layout', 'spice_software_plus_dummy_portfolio_fn');

if (!function_exists('testimonial_design')) {

    function testimonial_design($designId,$type) {
        switch ($designId) {
            case 'testimonial-carousel1':
                $sectionClass = 'testi-1';
                $textColor = 'text-black';
                $textAlign = 'text-center';
                break;
            case 'testimonial-carousel2':
                $sectionClass = 'testi-2';
                $textColor = 'text-white';
                $textAlign = '';
                break;
            case 'testimonial-carousel3':
                $sectionClass = 'testi-3';
                $textColor = 'text-white';
                $textAlign = '';
                break;
            case 'testimonial-carousel4':
                $sectionClass = 'testi-4';
                $textColor = 'text-white';
                $textAlign = 'text-center';
                break;

            default:
                break;
        }

        $testimonial_options = get_theme_mod('spice_software_testimonial_content');
        if (empty($testimonial_options))
        {
            if (get_theme_mod('home_testimonial_title') != '' || get_theme_mod('home_testimonial_desc') != '' || get_theme_mod('home_testimonial_name') != '' || get_theme_mod('home_testimonial_thumb') != '')
            {
            $home_testimonial_title = get_theme_mod('home_testimonial_title');
            $home_testimonial_discription = get_theme_mod('home_testimonial_desc');
            $home_testimonial_client_name = get_theme_mod('home_testimonial_name');
            $home_testimonial_designation = get_theme_mod('home_testimonial_designation');
            $home_testimonial_star=get_theme_mod('home_testimonial_star');
            $home_testimonial_link = get_theme_mod('home_testimonial_link');
            $home_testimonial_image = get_theme_mod('home_testimonial_thumb');
            $testimonial_callout_background=get_theme_mod('testimonial_callout_background');
            $testimonial_options = json_encode( array(
                                        array(
                                        'title' => !empty($home_testimonial_title) ? $home_testimonial_title : 'Exellent Theme & Very Fast Support',
                                        'text' => !empty($home_testimonial_discription) ? $home_testimonial_discription :'It is a long established fact that a reader will be distracted by the readable content of a page when<br> looking at its layout. The point of using Lorem ipsum dolor sit amet,<br> temp consectetur adipisicing elit.',
                                        'clientname' => !empty($home_testimonial_client_name) ? $home_testimonial_client_name : __('Amanda Smith', 'thecompany-plus'),
                                        'designation' => !empty($home_testimonial_designation) ? $home_testimonial_designation : __('Developer', 'thecompany-plus'),
                                        'home_testimonial_star' => !empty($home_testimonial_star) ? $home_testimonial_star : '4.5',
                                        'link' => !empty($home_testimonial_link) ? $home_testimonial_link : '#',
                                        'image_url' =>  !empty($home_testimonial_image) ? $home_testimonial_image : SPICE_SOFTWAREP_PLUGIN_URL . '/inc/images/user/user1.jpg',
                                        'open_new_tab' => 'no',
                                        'id' => 'customizer_repeater_56d7ea7f40b96',
                                        'home_slider_caption' => 'customizer_repeater_star_',
                                        ),
                                    ));
                }
                else
                {
                $home_testimonial_section_title = get_theme_mod('home_testimonial_section_title');
                $home_testimonial_section_discription = get_theme_mod('home_testimonial_section_discription');
                $designation = get_theme_mod('designation');
                $home_testimonial_thumb = get_theme_mod('home_testimonial_thumb');
                $testimonial_options = json_encode(array(
                    array(
                        'title' => 'Exellent Theme & Very Fast Support',
                        'text' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem ipsum dolor sit amet,<br> temp consectetur adipisicing elit.',
                        'clientname' => __('Amanda Smith', 'spice-software-plus'),
                        'designation' => __('Developer', 'spice-software-plus'),
                        'home_testimonial_star' => '4.5',
                        'link' => '#',
                        'image_url' => SPICE_SOFTWAREP_PLUGIN_URL . '/inc/images/user/user1.jpg',
                        'open_new_tab' => 'no',
                        'id' => 'customizer_repeater_77d7ea7f40b96',
                        'home_slider_caption' => 'customizer_repeater_star_4.5',
                    ),
                    array(
                        'title' => 'Exellent Theme & Very Fast Support',
                        'text' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem ipsum dolor sit amet,<br> temp consectetur adipisicing elit.',
                        'clientname' => __('Travis Cullan', 'spice-software-plus'),
                        'designation' => __('Team Leader', 'spice-software-plus'),
                        'home_testimonial_star' => '5',
                        'link' => '#',
                        'image_url' => SPICE_SOFTWAREP_PLUGIN_URL . '/inc/images/user/user2.jpg',
                        'open_new_tab' => 'no',
                        'id' => 'customizer_repeater_88d7ea7f40b97',
                        'home_slider_caption' => 'customizer_repeater_star_5',
                    ),
                    array(
                        'title' => 'Exellent Theme & Very Fast Support',
                        'text' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem ipsum dolor sit amet,<br> temp consectetur adipisicing elit.',
                        'clientname' => __('Victoria Wills', 'spice-software-plus'),
                        'designation' => __('Volunteer', 'spice-software-plus'),
                        'home_testimonial_star' => '3.5',
                        'link' => '#',
                        'image_url' => SPICE_SOFTWAREP_PLUGIN_URL . '/inc/images/user/user3.jpg',
                        'id' => 'customizer_repeater_11d7ea7f40b98',
                        'open_new_tab' => 'no',
                        'home_slider_caption' => 'customizer_repeater_star_3.5',
                    ),
                ));
            }
        }
        $testimonial_animation_speed = get_theme_mod('testimonial_animation_speed', 3000);
        $testimonial_smooth_speed = get_theme_mod('testimonial_smooth_speed', 1000);
        $isRTL = (is_rtl()) ? (bool) true : (bool) false;

        $slide_items = get_theme_mod('home_testimonial_slide_item', 1);
        $testimonial_nav_style = get_theme_mod('testimonial_nav_style', 'bullets');

        $testimonial_settings = array('design_id' => '#' . $designId, 'slide_items' => $slide_items, 'animationSpeed' => $testimonial_animation_speed, 'smoothSpeed' => $testimonial_smooth_speed, 'testimonial_nav_style' => $testimonial_nav_style, 'rtl' => $isRTL);

        wp_register_script('spice-software-testimonial', SPICE_SOFTWAREP_PLUGIN_URL . '/inc/js/front-page/testi.js', array('jquery'));
        wp_localize_script('spice-software-testimonial', 'testimonial_settings', $testimonial_settings);
        wp_enqueue_script('spice-software-testimonial');
        $home_testimonial_section_title = get_theme_mod('home_testimonial_section_title', __('Our Clients Says', 'spice-software-plus'));
        $home_testimonial_section_discription = get_theme_mod('home_testimonial_section_discription', __('industry is the same think. Lorem Ipsum is simply printing and typesetting industry is the same think.', 'spice-software-plus'));

        $testimonial_callout_background = get_theme_mod('testimonial_callout_background',SPICE_SOFTWAREP_PLUGIN_URL.'/inc/images/bg/bg-img.jpg');
        $theme = wp_get_theme();
        if('Spice Software Dark' == $theme->name) {
            $ss_testimonial_design=4;
        }
        else{
            $ss_testimonial_design=1;
        }
        $testi_layout=get_theme_mod('home_testimonial_design_layout',$ss_testimonial_design);
        if (get_page_template_slug()=='template-testimonial-1.php' || get_page_template_slug()=='template-testimonial-5.php' || get_page_template_slug()=='template-aboutus.php'|| get_page_template_slug()=='template-service.php') {
            ?>
            <section class="section-space testimonial <?php echo $sectionClass; ?>" >
            
                <?php
            } elseif(get_page_template_slug()=='template-business.php' && $testi_layout==1 ){
            ?>
            <section class="section-space testimonial <?php echo $sectionClass; ?>" >
            
                <?php
            } else {
                ?>
                <section class="section-space testimonial <?php echo $sectionClass; ?>"  style="background:url('<?php echo esc_url($testimonial_callout_background); ?>') 100% 100% no-repeat; -webkit-background-size: cover;
                  -moz-background-size: cover;
                  -o-background-size: cover;
                  background-size: cover;">
                    <?php
                }
                ?>
                <div class="owl-carousel owl-theme">
                    <div class="spice-software-tesi-container container">
                        <?php if ($home_testimonial_section_title != '' || $home_testimonial_section_discription != '') { ?>
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-xs-12">
                                    <div class="section-header">
                                        <?php if($home_testimonial_section_title):?>
                                        <h2 class="section-title <?php echo $textColor; ?>"><?php echo esc_attr($home_testimonial_section_title); ?></h2>
                                        <?php endif;?>
                                        <?php if ($home_testimonial_section_discription != ''):?>
                                        <p class="<?php echo $textColor; ?>"><?php echo esc_attr($home_testimonial_section_discription); ?></p>
                                        <?php endif;?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="row">
                            <?php if($type=='carousel'):?>
                            <div class="col-md-12" id="<?php echo $designId; ?>">
                                <?php endif;
                                $testimonial_options = json_decode($testimonial_options);
                                if ($testimonial_options != '') {
                                    $allowed_html = array(
                                        'br' => array(),
                                        'em' => array(),
                                        'strong' => array(),
                                        'b' => array(),
                                        'i' => array(),
                                    );
                                    foreach ($testimonial_options as $testimonial_iteam) {
                                        $title = !empty($testimonial_iteam->title) ? apply_filters('spice_software_translate_single_string', $testimonial_iteam->title, 'Testimonial section') : '';
                                        $test_desc = !empty($testimonial_iteam->text) ? apply_filters('spice_software_translate_single_string', $testimonial_iteam->text, 'Testimonial section') : '';
                                        $test_link = $testimonial_iteam->link;
                                        $open_new_tab = $testimonial_iteam->open_new_tab;
                                        $clientname = !empty($testimonial_iteam->clientname) ? apply_filters('spice_software_translate_single_string', $testimonial_iteam->clientname, 'Testimonial section') : '';
                                        $designation = !empty($testimonial_iteam->designation) ? apply_filters('spice_software_translate_single_string', $testimonial_iteam->designation, 'Testimonial section') : '';
                                        $stars = !empty($testimonial_iteam->home_testimonial_star) ? apply_filters('spice_software_translate_single_string', $testimonial_iteam->home_testimonial_star, 'Testimonial section') : '';
                                        ?>
                                        <div <?php if($type=='grid') { ?>class="col-lg-6 col-md-6 col-sm-12 testi-grid"<?php } else { ?> class="item" <?php } ?>>
                                            <blockquote class="testmonial-block <?php echo $textAlign; ?>">
                                                <?php $default_arg = array('class' => "img-circle"); ?>
                                                <?php if ($testimonial_iteam->image_url != ''): ?>
                                                    <figure class="avatar">
                                                        <img src="<?php echo $testimonial_iteam->image_url; ?>" class="img-fluid rounded-circle" alt="img" >
                                                    </figure>
                                                <?php endif; ?>

                                                <?php if (($clientname != '' || $designation != '' ) && $designId == 'testimonial-carousel4') { ?>								
                                                    <figcaption <?php echo $test_link . "-" . $open_new_tab; ?>>
                                                        <?php if (!empty($clientname)): ?>
                                                            <a href="<?php
                                                            if (empty($test_link)) {
                                                                echo '#';
                                                            } else {
                                                                echo esc_url($test_link);
                                                            }
                                                            ?>" <?php if ($open_new_tab == "yes") { ?> target="_blank"<?php } ?>>
                                                                <cite class="name <?php echo $textColor; ?>"><?php echo $clientname; ?></cite></a><?php endif; ?>
                                                        <?php if (!empty($designation)): ?><span class="designation <?php echo $textColor; ?>"><?php echo $designation; ?></span><?php endif; ?>
                                                    </figcaption>
                                                <?php } ?>

                                                <?php if (!empty($test_desc)): ?>
                                                    <div class="entry-content">
                                                        <?php if ($test_desc != '') { ?><p class="<?php echo $textColor; ?>" ><?php echo wp_kses(html_entity_decode($test_desc), $allowed_html); ?></p> <?php } ?>
                                                    </div>	
                                                    <?php
                                                endif;
                                                if (($clientname != '' || $designation != '') && $designId != 'testimonial-carousel4') {
                                                    ?>								
                                                    <figcaption <?php echo $test_link . "-" . $open_new_tab; ?>>
                                                        <?php if (!empty($clientname)): ?>
                                                            <a href="<?php
                                                            if (empty($test_link)) {
                                                                echo '#';
                                                            } else {
                                                                echo esc_url($test_link);
                                                            }
                                                            ?>" <?php if ($open_new_tab == "yes") { ?> target="_blank"<?php } ?>>
                                                                <cite class="name <?php echo $textColor; ?>"><?php echo $clientname; ?></cite></a><?php endif; ?>
                                                        <?php if (!empty($designation)): ?><span class="designation <?php echo $textColor; ?>"><?php echo $designation; ?></span><?php endif; ?>
                                                    </figcaption>
                                                <?php } ?>
                                                <?php if ((!empty($stars)) && in_array($designId, array('testimonial-carousel3', 'testimonial-carousel4'))) { ?>
                                                    <div class="rating">   
                                                    <?php
                                                    $star_arry=explode('_', $stars);
                                                    $stars_no_arr = end($star_arry);
                                                    $star_no = explode('_', $stars_no_arr);
                                                    $star_end = explode('.', end($star_no));
                                                    for ($i = 1; $i <= $star_end[0]; $i++) {
                                                        ?>
                                                        <span class="fa fa-star"></span>
                                                    <?php } ?>
                                                    <?php if(array_keys($star_end)==range(0,1)) { ?>
                                                        <span class="fa fa-star-half-o"></span>
                                                    <?php } ?>
                                                </div>
                                                <?php } ?>
                                            </blockquote>

                                            <?php if ((!empty($stars)) && !in_array($designId, array('testimonial-carousel3', 'testimonial-carousel4'))) { ?>
                                                <div class="rating">   
                                                    <?php
                                                    $star_arry=explode('_', $stars);
                                                    $stars_no_arr = end($star_arry);
                                                    $star_no = explode('_', $stars_no_arr);
                                                    $star_end = explode('.', end($star_no));
                                                    for ($i = 1; $i <= $star_end[0]; $i++) {
                                                        ?>
                                                        <span class="fa fa-star"></span>
                                                    <?php } ?>
                                                    <?php if(array_keys($star_end)==range(0,1)) { ?>
                                                        <span class="fa fa-star-half-o"></span>
                                                    <?php } ?>
                                                </div>
                                            <?php } ?>




                                        </div>		
                                        <?php
                                    }
                                } else {
                                    $image = array('user1', 'user2', 'user3', 'user4', 'user5', 'user6', 'user7', 'user8', 'user9', 'user10');
                                    $name = array(__('Martin Wills', 'spice-software-plus'),
                                        __('Amanda Smith', 'spice-software-plus'),
                                        __('Sue Thomas ', 'spice-software-plus'),
                                    );
                                    $desc = array(__('Developer', 'spice-software-plus'),
                                        __('Team Leader', 'spice-software-plus'),
                                        __('Volunteer', 'spice-software-plus'),
                                    );
                                    for ($i = 0; $i <= 2; $i++) {
                                        ?>	
                                        <div  class="item" >
                                            <blockquote class="testmonial-block <?php echo $textAlign; ?>">
                                                <figure class="avatar">
                                                    <img src="<?php echo SPICE_SOFTWAREP_PLUGIN_URL; ?>/inc/images/user/<?php echo $image[$i]; ?>.jpg" class="img-fluid rounded-circle" alt="Adriel Harlyn" >
                                                </figure>
                                                <?php if ($designId == 'testimonial-carousel4') { ?>
                                                    <figcaption>
                                                        <cite class="name <?php echo $textColor; ?>"><?php echo $name[$i]; ?></cite>
                                                        <span class="designation <?php echo $textColor; ?>"><?php echo $desc[$i]; ?></span>
                                                    </figcaption>
                                                <?php } ?>
                                                <div class="entry-content">
                                                    <p class="<?php echo $textColor; ?>"><?php echo esc_html__('Lorem Ipsum is simply dummy text of the printing and typesetting industry  is the same think. Lorem Ipsum is simply printing and typesetting industry is the same think. Lorem Ipsum is simply dummy text of the printing and typesetting industry  is the same think. Lorem Ipsum is simply printing and typesetting industry is the same think.', 'spice-software-plus'); ?></p>
                                                </div>	
                                                <?php if ($designId != 'testimonial-carousel4') { ?>
                                                    <figcaption>
                                                        <cite class="name <?php echo $textColor; ?>"><?php echo $name[$i]; ?></cite>
                                                        <span class="designation <?php echo $textColor; ?>"><?php echo $desc[$i]; ?></span>
                                                    </figcaption>
                                                <?php } ?>
                                                <?php if (in_array($designId, array('testimonial-carousel3', 'testimonial-carousel4'))) { ?>
                                                    <div class="rating">
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star"></span>
                                                        <span class="fa fa-star"></span>
                                                    </div>
                                                <?php } ?>
                                            </blockquote>
                                            <?php if (!in_array($designId, array('testimonial-carousel3', 'testimonial-carousel4'))) { ?>
                                                <div class="rating">
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>
                                                </div>
                                            <?php } ?>
                                        </div>
                                        <?php
                                    }
                                }
                                if($type=='carousel'):?>	
                            </div>
                        <?php endif;?>
                        </div>
                    </div>
                </div>
            </section>	
            <?php
        }

    }