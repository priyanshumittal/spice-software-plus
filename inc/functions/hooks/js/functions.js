/**
 *  Exported spice_software_filter_hooks
 *
 */

/**
 * Filter hooks
 */
function spice_software_filter_hooks() {

	var search_val = '';

	if ( typeof jQuery( '#spice_software_search_hooks' ) !== 'undefined' ) {

		if ( typeof jQuery( '#spice_software_search_hooks' ).val() !== 'undefined' ) {

			search_val = jQuery( '#spice_software_search_hooks' ).val().toUpperCase();

			if ( typeof search_val !== 'undefined' ) {

				jQuery( '#Spice_Software_Plus_Hooks_Settings th' ).each(
					function () {

						if ( jQuery( this ).text().toUpperCase().indexOf( search_val ) > -1 ) {
							jQuery( this ).parent().removeClass( 'hooks-none' );
						} else {
							jQuery( this ).parent().addClass( 'hooks-none' );
						}
					}
				);

			}

		}

	}
}