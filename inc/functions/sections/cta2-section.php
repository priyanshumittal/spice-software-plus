<?php
//call the action for the cta2 section
add_action('spice_software_plus_cta2_action','spice_software_plus_cta2_section');
//function for the cta2 section
function spice_software_plus_cta2_section()
{
$cta2_section_enable  = get_theme_mod('cta2_section_enable', true);
if($cta2_section_enable != false) 
{ 
	include_once(SPICE_SOFTWAREP_PLUGIN_DIR.'/inc/inc/home-section/cta2-content.php');
}
}