<?php
//call the action for the team section
add_action('spice_software_plus_team_action','spice_software_plus_team_section');
//function for the services section
function spice_software_plus_team_section()
{
$team_section_enable = get_theme_mod('team_section_enable', true);
$theme = wp_get_theme();
if('Spice Software Dark' == $theme->name) {
    $ss_team_design=4;
}
else{
    $ss_team_design=1;
}
if ($team_section_enable != false) {
	$team_layout=get_theme_mod('home_team_design_layout', $ss_team_design);
	include_once(SPICE_SOFTWAREP_PLUGIN_DIR.'/inc/inc/home-section/team-content'.$team_layout.'.php');
}
}