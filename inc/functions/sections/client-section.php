<?php
//call the action for the client section
add_action('spice_software_plus_client_action','spice_software_plus_client_section');
//function for the client section
function spice_software_plus_client_section()
{
$client_section_enable = get_theme_mod('client_section_enable', true);
if($client_section_enable != false)
{ 
include_once(SPICE_SOFTWAREP_PLUGIN_DIR.'/inc/inc/home-section/client-content.php');
}
}