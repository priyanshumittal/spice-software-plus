<?php
//call the action for the news section
add_action('spice_software_plus_news_action','spice_software_plus_news_section');
//function for the news section
function spice_software_plus_news_section()
{
$latest_news_section_enable = get_theme_mod('latest_news_section_enable', true);
$theme = wp_get_theme();
if('Spice Software Dark' == $theme->name) {
    $ss_news_design=4;
}
else{
    $ss_news_design=1;
}
if ($latest_news_section_enable != false) {
    $news_layout=get_theme_mod('home_news_design_layout', $ss_news_design);
	include_once(SPICE_SOFTWAREP_PLUGIN_DIR.'/inc/inc/home-section/news-content'.$news_layout.'.php');
}
}