<?php
//call the action for the fun-acts section
add_action('spice_software_plus_fun_action','spice_software_plus_fun_section');
//function for the fun-acts section
function spice_software_plus_fun_section()
{
$funfact_section_enabled = get_theme_mod('funfact_section_enabled', true);
if($funfact_section_enabled != false)
{ 
include_once(SPICE_SOFTWAREP_PLUGIN_DIR.'/inc/inc/home-section/fun-content.php');
}
}