<?php
//call the action for the services section
add_action('spice_software_plus_services_action','spice_software_plus_services_section');
//function for the services section
function spice_software_plus_services_section()
{
$spice_software_home_service_enabled = get_theme_mod('home_service_section_enabled', true);
if($spice_software_home_service_enabled != false)
{
include_once(SPICE_SOFTWAREP_PLUGIN_DIR.'/inc/inc/home-section/service-content.php');   
}
}