<?php
//call the action for the cta1 section
add_action('spice_software_plus_cta1_action','spice_software_plus_cta1_section');
//function for the cta1 section
function spice_software_plus_cta1_section()
{
$cta1_section_enable  = get_theme_mod('cta1_section_enable', true);
if($cta1_section_enable != false){ 
	include_once(SPICE_SOFTWAREP_PLUGIN_DIR.'/inc/inc/home-section/cta1-content.php');	
} 
}