<?php

class spice_software_plus_pagination {

    function spice_software_plus_page() {
       global $post;
        global $wp_query, $wp_rewrite, $loop, $template;
        if (get_query_var('paged')) {
            $paged = get_query_var('paged');
        } elseif (get_query_var('page')) {
            $paged = get_query_var('page');
        } else {
            $paged = 1;
        }
        if(basename($template)!='search.php') {                    
            if ($wp_query->max_num_pages == 0) {
                $wp_query = $loop;
            }
        }

        if (!is_rtl()) {
            the_posts_pagination(array(
                'prev_text' => __('Previous', 'spice-software-plus'),
                'next_text' => __('Next', 'spice-software-plus'),
            ));
        } else {
            the_posts_pagination(array(
                'prev_text' => __('Previous', 'spice-software-plus'),
                'next_text' => __('Next', 'spice-software-plus'),
            ));
        }
    }
}

class Spicethemes_pagination {

    function Spicethemes_page($curpage, $post_type_data, $total, $posts_per_page) {
        $count = $total - $posts_per_page;
        if ($count <= 0) {
            return;
        } else {
            ?>
            <nav aria-label="..." class="nav-pagination">
                <ul class="pagination justify-content-center">
                    <?php if ($curpage != 1) {
                        echo '<li class="page-item"><a class="page-link" href="' . get_pagenum_link(($curpage - 1 > 0 ? $curpage - 1 : 1)) . '" >Previous</a></li>';
                    }
                    ?>
                    <?php
                    for ($i = 1; $i <= $post_type_data->max_num_pages; $i++) {
                        echo '<li class="page-item"><a class="page-link ' . ($i == $curpage ? 'active ' : '') . '" href="' . get_pagenum_link($i) . '">' . $i . '</a></li>';
                    }
                    if ($i - 1 != $curpage) {
                        echo '<li class="page-item"><a class="page-link" href="' . get_pagenum_link(($curpage + 1 <= $post_type_data->max_num_pages ? $curpage + 1 : $post_type_data->max_num_pages)) . '">Next</a></li>';
                    }
                    ?>
                </ul>
            </nav>
        <?php
        }
    }

}
