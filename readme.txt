=== Spice Software Plus ===

Contributors: spicethemes
Requires at least: 4.5
Tested up to: 6.7.1
Stable tag: 1.3.3
Requires PHP: 5.4
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Enhance Spice Software WordPress Themes functionality.

== Description ==

Spice Software WordPress Theme is lightweight, elegant, fully responsive and translation-ready theme that allows you to create stunning blogs and websites. The theme is well suited for companies, law firms,ecommerce, finance, agency, travel, photography, recipes, design, arts, personal and any other creative websites and blogs. Theme is developed using Bootstrap 4 framework. It comes with a predesigned home page, good looking header designs and number of content sections that you can easily customize. It also has lots of customization options (banner, services, testimonial, etc) that will help you create a beautiful, unique website in no time. Spice Software is  compatible with popular plugins like WPML, Polylang, Woo-commerce  and Conact Form 7. Spice Software theme comes with various popular locales.(DEMO: https://spice-software-pro.spicethemes.com/) 


== Changelog ==

@Version 1.3.3
* Updated freemius directory.

@Version 1.3.2
* Updated font-awesome library and freemius directory.

@Version 1.3.1
* Added Rank Math,Seo Yoast and NavXT Plugin Breadcrumbs Feature.

@Version 1.3
* Updated freemius directory.

@Version 1.2.3
* Added toggle button for Loading Google Fonts Locally.

@Version 1.2.2
* Fixed customizer add project link issue.

@Version 1.2.1
* Corrected freemius product slug.

@Version 1.2
* Added freemius directory.

@Version 1.1.2
* Fixed Preloader and some style issues.
* Removed unnecessary code & updated some links.

@Version 1.1.1
* Fixed One Click Demo Import feature issues.
* Fixed pagination, sticky header, container setting and some style issues.
* Removed Google Plus social icon.
* Updated option's page link. 

@Version 1.1
* Fixed the issues with PHP 8.

@Version 1.0
* Added One Click Demo Import feature.
* Updated Option's page link, style and strings.
* Added blog switcher section and templates.
* Added a new soft orange predefined colors.
* Fixed some style issues and removed unnecessary code.
* Fixed data migration issues dark child to plus. 

@Version 0.1
* Initial release

== External resources ==

Owl Carousel: 
Copyright: (c) David Deutsch
License: MIT license
Source: https://cdnjs.com/libraries/OwlCarousel2/2.2.1

Alpha color picker Control:
Copyright: (c) 2016 Codeinwp cristian-ungureanu
License: MIT License
Source: https://github.com/Codeinwp/customizer-controls/tree/master/customizer-alpha-color-picker

Repeater Control:
Copyright: (c) 2016 Codeinwp cristian-ungureanu
License: MIT license
Source: https://github.com/Codeinwp/customizer-controls/tree/master/customizer-repeater

Custom control - Image Radio Button Custom Control:
Copyright: Anthony Hortin
License: GNU General Public License v2 or later
Source: https://github.com/maddisondesigns/customizer-custom-controls

== Images ==

* Image used in Slider, License CC0 Public Domain
1. https://pxhere.com/en/photo/1044386
2. https://pxhere.com/en/photo/102242
3. https://pxhere.com/en/photo/8873

* Image used in CTA 2, License CC0 Public Domain
https://pxhere.com/en/photo/938790

* Image used in Project, License CC0 Public Domain
1. https://pxhere.com/en/photo/1177788
2. https://pxhere.com/en/photo/1397984
3. https://pxhere.com/en/photo/229
4. https://pxhere.com/en/photo/1372631
5. https://pxhere.com/en/photo/1450143
6. https://pxhere.com/en/photo/1087339
7. https://pxhere.com/en/photo/892699
8. https://pxhere.com/en/photo/910919
9. https://pxhere.com/en/photo/1575121
10. https://pxhere.com/en/photo/1457957
11. https://pxhere.com/en/photo/1576793
12. https://pxhere.com/en/photo/939748
13. https://pxhere.com/en/photo/1559329
14. https://pxhere.com/en/photo/1206989
15. https://pxhere.com/en/photo/136941
16. https://pxhere.com/en/photo/98984
17. https://pxhere.com/en/photo/42491
18. https://pxhere.com/en/photo/892285

* Image used in Funfact, License CC0 Public Domain
https://pxhere.com/en/photo/1548825

* Image used in Testimonial, License CC0 Public Domain
1. https://pxhere.com/en/photo/52971
2. https://pxhere.com/en/photo/1587727
3. https://pxhere.com/en/photo/1432871

* Image used in Team, License CC0 Public Domain
1. https://pxhere.com/en/photo/1421179
2. https://pxhere.com/en/photo/1432871
3. https://pxhere.com/en/photo/642877
4. https://pxhere.com/en/photo/1596737
5. https://pxhere.com/en/photo/99184
6. https://pxhere.com/en/photo/1576793
7. https://pxhere.com/en/photo/816420

* Images on /images folder
Copyright (C) 2021, spicethemes and available as [GPLv2](https://www.gnu.org/licenses/gpl-2.0.html)